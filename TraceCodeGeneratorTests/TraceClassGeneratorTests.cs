﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TraceCodeGenerator;
using TestReferenceFunctional.BaseClasses;
using Xunit;
using TestStrings = TestReferenceTrace.Classes.TestStrings;

namespace TraceCodeGeneratorTests
{
	public class TraceClassGeneratorTests
	{
		[Theory]
		[InlineData(typeof(EmptyClass), TestStrings.TraceEmptyClassExpected)]
		[InlineData(typeof(GenericType<>), TestStrings.TraceGenericTypeExpected)]
		[InlineData(typeof(GenericType<int>), TestStrings.TraceGenericType_intExpected)]
		[InlineData(typeof(GenericInt), TestStrings.TraceGenericIntExpected)]
		[InlineData(typeof(ClassWithNonTraceableConstructor), TestStrings.TraceClassWithNonTraceableConstructorExpected)]
		[InlineData(typeof(PropertyAndMethodClass), TestStrings.TracePropertyAndMethodClassExpected)]
		public void Build_ByType_ReturnsExpectedCode(Type type, string expected)
		{
			var codeGen = new TraceClassGenerator(type, "TestReferenceTrace.Classes");

			string actual = codeGen.Build();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_GenericClassIntNamed_ReturnsExpectedCode()
		{
			var codeGen = new TraceClassGenerator(typeof(GenericType<int>), "TestReferenceTrace.Classes", "TraceGenericInt2");

			string expected = TestStrings.TraceGenericInt2Expected;
			string actual = codeGen.Build();

			Assert.Equal(expected, actual);
		}
	}
}
