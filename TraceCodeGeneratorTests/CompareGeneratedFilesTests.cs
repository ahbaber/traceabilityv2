﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.IO;
using TraceCodeFileTests.Functional;
using TraceCodeFileTests.Traceable;
using TraceCodeGenerator;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests
{
	public class CompareGeneratedFilesTests
	{
		static private readonly string _traceDirectory = GetDirectories();
		static private string _factoryDirectory;
		static private string _expectedDirectory;
		static private string _traceFactoryDirectory;

		private static string GetDirectories()
		{
			string dir = Directory.GetCurrentDirectory();
			dir = dir.Remove(dir.IndexOf("TraceCodeGeneratorTests", System.StringComparison.InvariantCulture));
			dir = Path.Combine(dir, "TraceCodeFileTests");
			_factoryDirectory = Path.Combine(dir, "Factories");
			_expectedDirectory = Path.Combine(dir, "Expected");
			_traceFactoryDirectory = Path.Combine(dir, "TraceFactories");
			return Path.Combine(dir, "Traceable");
		}

		public enum PathCodes
		{
			Factory,
			Traced,
			TraceFactory,
		}

		private static string GetPath(PathCodes code)
		{
			switch (code)
			{
				case PathCodes.Factory: return _factoryDirectory;
				case PathCodes.Traced: return _traceDirectory;
				case PathCodes.TraceFactory: return _traceFactoryDirectory;
			}
			throw new Exception("that should not have happened...");
		}

		[Theory]
		[InlineData("TraceEmptyClass", PathCodes.Traced)]
		[InlineData("TracePartial", PathCodes.Traced)]
		[InlineData("TraceAddUsing", PathCodes.Traced)]
		[InlineData("TraceInternal", PathCodes.Traced)]
		[InlineData("TraceSkipProp", PathCodes.Traced)]
		[InlineData("TraceSkipBothMethods", PathCodes.Traced)]
		[InlineData("TraceSkipSingleMethod", PathCodes.Traced)]
		[InlineData("TraceSkipByClass", PathCodes.Traced)]
		[InlineData("TraceClassWithNonTracedMembers", PathCodes.Traced)]
		[InlineData("TraceGenericClass", PathCodes.Traced)]
		[InlineData("TraceGenericInt", PathCodes.Traced)]
		[InlineData("EmptyClassFactory", PathCodes.Factory)]
		[InlineData("GenericClassFactory", PathCodes.Factory)]
		[InlineData("GenericIntFactory", PathCodes.Factory)]
		[InlineData("GenericNamedFactory", PathCodes.Factory)]
		[InlineData("TraceEmptyClassFactory", PathCodes.TraceFactory)]
		[InlineData("TraceGenericClassFactory", PathCodes.TraceFactory)]
		[InlineData("TraceGenericIntFactory", PathCodes.TraceFactory)]
		[InlineData("TraceGenericNamedFactory", PathCodes.TraceFactory)]
		public void CheckTraceFiles_FilesAreSame(string filename, PathCodes code)
		{
			string dir = GetPath(code);
			string testFile = Path.Combine(dir, filename + ".cs");
			string expectedFile = Path.Combine(_expectedDirectory, filename + ".txt");

			using (var testReader = new StreamReader(testFile))
			using (var expectedReader = new StreamReader(expectedFile))
			{
				string testLine = "";
				string expectedLine = "";
				while ((testLine = testReader.ReadLine()) != null
				&& (expectedLine = expectedReader.ReadLine()) != null)
				{
					Assert.Equal(expectedLine, testLine);
				}
				int lineCount = 0;

				while ((testLine = testReader.ReadLine()) != null)
				{ ++lineCount; }
				Assert.Equal(0, lineCount);

				while ((expectedLine = expectedReader.ReadLine()) != null)
				{ ++lineCount; }
				Assert.Equal(0, lineCount);
			}
		}

		[Theory]
		[InlineData(typeof(EmptyClass), typeof(TraceEmptyClass))]
		[InlineData(typeof(EmptyClass), typeof(TraceAddUsing))]
		[InlineData(typeof(GenericClass<>), typeof(TraceGenericClass<>))]
		[InlineData(typeof(GenericClass<int>), typeof(TraceGenericInt))]
		public void VerifyTraceInheritence_TraceClassesMatch(Type baseClass, Type traceClass)
		{
			Verify.VerifyTraceInheritence(baseClass, traceClass);
		}

		[Fact]
		public void VerifyTraceInheritence_TraceSkipProp_TraceClassMatches()
		{
			var ignore = new SkipMap().Add("IntProp", false);

			var actual = Verify.VerifyTraceInheritence(typeof(SkippingTestClass), typeof(TraceSkipProp), ignore);

			Assert.Empty(actual.Errors);
		}

		[Fact]
		public void VerifyTraceInheritence_TraceSkipBothMethods_TraceClassMatches()
		{
			var ignore = new SkipMap()
				.Add("Method1_System.Void", true)
				.Add("Method1_System.Void_System.Int32", true);

			var actual = Verify.VerifyTraceInheritence(typeof(SkippingTestClass), typeof(TraceSkipBothMethods), ignore);

			Assert.Empty(actual.Errors);
		}

		[Fact]
		public void VerifyTraceInheritence_TraceSkipSingleMethod_TraceClassMatches()
		{
			var ignore = new SkipMap().Add("Method1_System.Void_System.Int32", true);

			var actual = Verify.VerifyTraceInheritence(typeof(SkippingTestClass), typeof(TraceSkipSingleMethod), ignore);

			Assert.Empty(actual.Errors);
		}

		[Fact]
		public void VerifyTraceInheritence_TraceSkipByClass_TraceClassMatches()
		{
			var ignore = new SkipMap()
				.Add("IntProp", false)
				.Add("Method1_System.Void_System.Int32", true);

			var actual = Verify.VerifyTraceInheritence(typeof(SkippingTestClass), typeof(TraceSkipSingleMethod), ignore);

			Assert.Empty(actual.Errors);
		}
	}
}
