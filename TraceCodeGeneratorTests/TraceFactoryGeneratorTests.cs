﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TraceCodeGenerator;
using TestReferenceFunctional.Factories;
using TestReferenceTrace.Classes;
using Xunit;
using TestStrings = TestReferenceTrace.Factories.TestStrings;

namespace TraceCodeGeneratorTests
{
	public class TraceFactoryGeneratorTests
	{
		[Theory]
		[InlineData(typeof(EmptyClassFactory), typeof(TraceEmptyClass), TestStrings.TraceEmptyClassFactoryExpected)]
		[InlineData(typeof(GenericTypeFactory<>), typeof(TraceGenericType<>), TestStrings.TraceGenericTypeFactoryExpected)]
		[InlineData(typeof(GenericTypeFactory_int), typeof(TraceGenericType_int), TestStrings.TraceGenericTypeFactory_intExpected)]
		[InlineData(typeof(GenericIntFactory), typeof(TraceGenericInt), TestStrings.TraceGenericIntFactoryExpected)]
		[InlineData(typeof(GenericWithParamsOnConstructFactory), typeof(TraceGenericType<>), TestStrings.TraceGenericWithParamsOnConstructFactoryExpected)]
		[InlineData(typeof(ClassWithNonTraceableConstructorFactory), typeof(TraceClassWithNonTraceableConstructor), TestStrings.TraceClassWithNonTraceableConstructorFactoryExpected)]
		public void Build_Type_ReturnsExpectedCode(Type factoryType, Type traceClass, string expected)
		{
			var codeGen = new TraceFactoryGenerator(factoryType, traceClass, "TestReferenceTrace.Factories");

			string actual = codeGen.Build();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("EmptyClassFactory", "TraceEmptyClass", TestStrings.TraceEmptyClassFactoryExpected)]
		[InlineData("GenericTypeFactory_int", "TraceGenericType_int", TestStrings.TraceGenericTypeFactory_intExpected)]
		[InlineData("GenericIntFactory", "TraceGenericInt", TestStrings.TraceGenericIntFactoryExpected)]
		[InlineData("ClassWithNonTraceableConstructorFactory", "TraceClassWithNonTraceableConstructor", TestStrings.TraceClassWithNonTraceableConstructorFactoryExpected)]
		public void Build_ByTypeName_ReturnsExpectedCode(string factoryName, string traceName, string expected)
		{
			factoryName = $"TestReferenceFunctional.Factories.{factoryName}";
			traceName = $"TestReferenceTrace.Classes.{traceName}";

			var codeGen = new TraceFactoryGenerator(factoryName, traceName, "TestReferenceTrace.Factories");

			string actual = codeGen.Build();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("GenericTypeFactory", "TraceGenericType", "Could not locate valid type with: 'TestReferenceFunctional.Factories.GenericTypeFactory,TestReferenceFunctional'.")]
		[InlineData("GenericWithParamsOnConstructFactory", "TraceGenericType", "Could not locate valid type with 'TestReferenceTrace.Classes.TraceGenericType,TestReferenceTrace'")]
		public void Build_ByTypeName_ReturnsError(string factoryName, string traceName, string expected)
		{
			factoryName = $"TestReferenceFunctional.Factories.{factoryName}";
			traceName = $"TestReferenceTrace.Classes.{traceName}";

			var codeGen = new TraceFactoryGenerator(factoryName, traceName, "TestReferenceTrace.Factories");

			Assert.True(codeGen.HasError);
			Assert.Equal(expected, codeGen.Error);
		}

		[Fact]
		public void Construct_UsingGenerator_ReturnsExpectedCode()
		{
			var baseFactory = new FactoryGenerator(typeof(TestReferenceFunctional.BaseClasses.EmptyClass), "TestReferenceFunctional.Factories");
			var traceClass = new TraceClassGenerator(typeof(TestReferenceFunctional.BaseClasses.EmptyClass), "TestReferenceTrace.Classes");
			var codeGen = new TraceFactoryGenerator(baseFactory, traceClass, "TestReferenceTrace.Factories");

			string expected = TestStrings.TraceEmptyClassFactoryExpected;

			Assert.Equal(expected, codeGen.Build());
		}
	}
}
