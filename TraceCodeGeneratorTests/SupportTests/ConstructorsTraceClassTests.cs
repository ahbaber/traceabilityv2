﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class ConstructorsTraceClassTests
	{
		private class DefaultConstructor { }

		[Fact]
		public void Build_DefaultConstuctor_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(DefaultConstructor), Visibility.Flags);

			string expected = @"
		public TraceDefaultConstructor(ITracer tracer)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceDefaultConstructor").ToString();

			Assert.Equal(expected, actual);
		}

		private class ProtectedConstructor { protected ProtectedConstructor() { } }

		[Fact]
		public void Build_ProtectedConstuctor_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(ProtectedConstructor), Visibility.Flags);

			string expected = @"
		protected TraceProtectedConstructor(ITracer tracer)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceProtectedConstructor").ToString();

			Assert.Equal(expected, actual);
		}

		private class InternalConstructor { protected InternalConstructor() { } }

		[Fact]
		public void Build_InternalConstuctor_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(InternalConstructor), Visibility.Flags);

			string expected = @"
		protected TraceInternalConstructor(ITracer tracer)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceInternalConstructor").ToString();

			Assert.Equal(expected, actual);
		}

		private class SingleParam { public SingleParam(int param) { } }

		[Fact]
		public void Build_SingleParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(SingleParam), Visibility.Flags);

			string expected = @"
		public TraceSingleParam(ITracer tracer, int param)
		:base(param)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceSingleParam").ToString();

			Assert.Equal(expected, actual);
		}

		private class DoubleParam { public DoubleParam(string a, long b) { } }

		[Fact]
		public void Build_DoubleParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(DoubleParam), Visibility.Flags);

			string expected = @"
		public TraceDoubleParam(ITracer tracer, string a, long b)
		:base(a, b)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceDoubleParam").ToString();

			Assert.Equal(expected, actual);
		}

		private class DoubleParamWithDefault { public DoubleParamWithDefault(int c, bool d = true) { } }

		[Fact]
		public void Build_DoubleParamWithDefault_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(DoubleParamWithDefault), Visibility.Flags);

			string expected = @"
		public TraceDoubleParamWithDefault(ITracer tracer, int c, bool d = true)
		:base(c, d)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceDoubleParamWithDefault").ToString();

			Assert.Equal(expected, actual);
		}

		private class ConstructorWithOutParam { public ConstructorWithOutParam(out int i) { i = 0; } }

		[Fact]
		public void Build_ConstructorWithOutParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(ConstructorWithOutParam), Visibility.Flags);

			string expected = @"
		public TraceConstructorWithOutParam(ITracer tracer, out int i)
		:base(out i)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceConstructorWithOutParam").ToString();

			Assert.Equal(expected, actual);
		}

		private class ConstructorWithRefParam { public ConstructorWithRefParam(ref int i) { } }

		[Fact]
		public void Build_ConstructorWithRefParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(ConstructorWithRefParam), Visibility.Flags);

			string expected = @"
		public TraceConstructorWithRefParam(ITracer tracer, ref int i)
		:base(ref i)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceConstructorWithRefParam").ToString();

			Assert.Equal(expected, actual);
		}

		private class PrivateConstructor { private PrivateConstructor(int i) { } }

		[Fact]
		public void Build_PrivateConstructor_ReturnsComment()
		{
			var classInfo = new ClassInfo(typeof(PrivateConstructor), Visibility.Flags);

			string expected = @"
		// skipped private PrivateConstructor(int i)
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TracePrivateConstructor").ToString();

			Assert.Equal(expected, actual);
		}

		private class Generic<T>
		{
			public Generic() { }
			public Generic(T arg) { }
		}

		[Fact]
		public void Build_GenericClassNoType_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(Generic<>), Visibility.Flags);

			string expected = @"
		public TraceGeneric(ITracer tracer)
		{
			_tracer = tracer;
		}

		public TraceGeneric(ITracer tracer, T arg)
		:base(arg)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceGeneric").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_GenericClassTypeInt_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(Generic<int>), Visibility.Flags);

			string expected = @"
		public TraceGeneric(ITracer tracer)
		{
			_tracer = tracer;
		}

		public TraceGeneric(ITracer tracer, int arg)
		:base(arg)
		{
			_tracer = tracer;
		}
";
			string actual = ConstructorsTraceClass.Singleton.Build(new StringBuilder(), classInfo, "TraceGeneric").ToString();

			Assert.Equal(expected, actual);
		}
	}
}
