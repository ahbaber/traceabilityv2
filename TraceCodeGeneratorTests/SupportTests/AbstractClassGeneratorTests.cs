﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TestReferenceFunctional.BaseClasses;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class AbstractClassGeneratorTests
	{
		private class ConcreteGenerator : AbstractClassGenerator
		{
			public ConcreteGenerator(Type baseType, string name_space, string className = null, bool inherits = false)
			:base(baseType, Visibility.PublicFlags, name_space, null)
			{
				_inherits = inherits;
				SetNames(className, "Concrete");
			}

			public ConcreteGenerator(string typeName, string name_space, string className = null)
			:base(typeName, Visibility.PublicFlags, name_space, null)
			{
				SetNames(className, "Concrete");
			}

			public override string Build()
			{
				AddOpeningBoilerplate(_inherits);
				_builder.Append("\n\t\t// Concrete Generator");
				AddClosingBoilerPlate();
				return _builder.ToString();
			}

			private readonly bool _inherits;

			public override string FileType => ".cs";
		}

		private static ConcreteGenerator[] generators =
		{
			new ConcreteGenerator(typeof(EmptyClass), "UnitTestingAbstract"),
			new ConcreteGenerator("TestReferenceFunctional.BaseClasses.EmptyClass", "UnitTestingAbstract"),
			new ConcreteGenerator(typeof(EmptyClass), "UnitTestingAbstract", "GivenName"),
			new ConcreteGenerator("TestReferenceFunctional.BaseClasses.EmptyClass", "UnitTestingAbstract", "GivenName"),
			new ConcreteGenerator(typeof(GenericType<>), "UnitTestingAbstract"),
		};

		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		public void Construct_ReturnsExpectedNamespace(int index)
		{
			var generator = generators[index];

			Assert.Equal("UnitTestingAbstract", generator.Namespace);
		}

		[Theory]
		[InlineData(0, "ConcreteEmptyClass")]
		[InlineData(1, "ConcreteEmptyClass")]
		[InlineData(2, "GivenName")]
		[InlineData(3, "GivenName")]
		[InlineData(4, "ConcreteGenericType<T>")]
		public void Construct_ReturnsExpectedDeclarationName(int index, string expected)
		{
			var generator = generators[index];

			Assert.Equal(expected, generator.DeclarationName);
		}

		[Theory]
		[InlineData(0, "UnitTestingAbstract.ConcreteEmptyClass,UnitTestingAbstract")]
		[InlineData(1, "UnitTestingAbstract.ConcreteEmptyClass,UnitTestingAbstract")]
		[InlineData(2, "UnitTestingAbstract.GivenName,UnitTestingAbstract")]
		[InlineData(3, "UnitTestingAbstract.GivenName,UnitTestingAbstract")]
		[InlineData(4, "")]
		public void Construct_ReturnsExpectedQualifiedName(int index, string expected)
		{
			var generator = generators[index];

			Assert.Equal(expected, generator.QualifiedName);
		}

		[Theory]
		[InlineData(0, "ConcreteEmptyClass")]
		[InlineData(1, "ConcreteEmptyClass")]
		[InlineData(2, "GivenName")]
		[InlineData(3, "GivenName")]
		[InlineData(4, "ConcreteGenericType")]
		public void Construct_ReturnsExpectedFilename(int index, string expected)
		{
			var generator = generators[index];

			Assert.Equal(expected, generator.Filename);
		}

		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		public void Construct_HasErrorIsFalse(int index)
		{
			var generator = generators[index];

			Assert.False(generator.HasError);
		}

		[Fact]
		public void Construct_NullType_ReturnsExpectedError()
		{
			Type type = null;
			var generator = new ConcreteGenerator(type, "UnitTestingAbstract");

			Assert.True(generator.HasError);
			Assert.Equal("baseType is null.", generator.Error);
		}

		[Fact]
		public void Construct_EmptyNameSpace_ReturnsExpectedError()
		{
			var generator = new ConcreteGenerator(typeof(EmptyClass), "");

			Assert.True(generator.HasError);
			Assert.Equal("Invalid value for name_space: ''", generator.Error);
		}

		[Fact]
		public void Construct_BadTypeName_ReturnsExpectedError()
		{
			var generator = new ConcreteGenerator("DoesNotExist", "UnitTestingAbstract");

			Assert.True(generator.HasError);
			Assert.Equal("Could not locate valid type with: 'DoesNotExist'.", generator.Error);
		}

		[Fact]
		public void Construct_TypeNameWithBadNamespace_ReturnsExpectedError()
		{
			var generator = new ConcreteGenerator("TestReferenceFunctional.BaseClasses.EmptyClass", "");

			Assert.True(generator.HasError);
			Assert.Equal("Invalid value for name_space: ''", generator.Error);
		}

		[Fact]
		public void Build__EmptyNameSpace_ReturnsExpectedString()
		{
			var generator = new ConcreteGenerator(typeof(EmptyClass), "");

			string expected = "\n\t\t// Concrete Generator";

			Assert.True(generator.HasError);
			Assert.Equal(expected, generator.Build());
		}

		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		public void Build_ReturnsExpectedCode(int index)
		{
			var generator = generators[index];

			string expected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;

namespace UnitTestingAbstract
{
	public class ConcreteEmptyClass
	{
		// Concrete Generator
	}
}
";
			string actual = generator.Build();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void AddUsing_ReturnsExpectedCode()
		{
			var generator = new ConcreteGenerator(typeof(EmptyClass), "UnitTestingAbstract")
				.AddUsing("SomeNamespace");

			string expected = @"// Auto generated file
// Any modification to this file may be lost
using SomeNamespace;
using TestReferenceFunctional.BaseClasses;

namespace UnitTestingAbstract
{
	public class ConcreteEmptyClass
	{
		// Concrete Generator
	}
}
";
			string actual = generator.Build();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void AddUsing_MultipleNamespaces_ReturnsExpectedCode()
		{
			var generator = new ConcreteGenerator(typeof(EmptyClass), "UnitTestingAbstract")
				.AddUsing("SomeNamespace")
				.AddUsing("AnotherNamespace")
				.AddUsing("AnotherNamespace.Subnamespace");

			string expected = @"// Auto generated file
// Any modification to this file may be lost
using AnotherNamespace;
using AnotherNamespace.Subnamespace;
using SomeNamespace;
using TestReferenceFunctional.BaseClasses;

namespace UnitTestingAbstract
{
	public class ConcreteEmptyClass
	{
		// Concrete Generator
	}
}
";
			string actual = generator.Build();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void MakePartial_ReturnsExpectedCode()
		{
			var generator = new ConcreteGenerator(typeof(EmptyClass), "UnitTestingAbstract")
				.MakePartial();

			string expected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;

namespace UnitTestingAbstract
{
	public partial class ConcreteEmptyClass
	{
		// Concrete Generator
	}
}
";
			string actual = generator.Build();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void MakeInternal_ReturnsExpectedCode()
		{
			var generator = new ConcreteGenerator(typeof(EmptyClass), "UnitTestingAbstract")
				.MakeInternal();

			string expected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;

namespace UnitTestingAbstract
{
	internal class ConcreteEmptyClass
	{
		// Concrete Generator
	}
}
";
			string actual = generator.Build();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Inherits_ReturnsExpectedCode()
		{
			var generator = new ConcreteGenerator(typeof(EmptyClass), "UnitTestingAbstract", null, true);

			string expected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;

namespace UnitTestingAbstract
{
	public class ConcreteEmptyClass : TestReferenceFunctional.BaseClasses.EmptyClass
	{
		// Concrete Generator
	}
}
";
			string actual = generator.Build();

			Assert.Equal(expected, actual);
		}
	}
}
