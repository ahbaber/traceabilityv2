﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TestReferenceFunctional.BaseClasses;
using TraceCodeGenerator;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class AbstractClassGeneratorNoSealedTests
	{
		[Fact]
		public void Construct_TraceClassGenerator_ByType_ReturnsErrorMessage()
		{
			var generator = new TraceClassGenerator(typeof(SealedClass), "TestNamespace");

			Assert.True(generator.HasError);
			Assert.Equal("Sealed class 'TestReferenceFunctional.BaseClasses.SealedClass,TestReferenceFunctional' may not be traced.", generator.Error);
		}

		[Fact]
		public void Construct_TraceClassGenerator_TypeName_ReturnsErrorMessage()
		{
			var generator = new TraceClassGenerator("TestReferenceFunctional.BaseClasses.SealedClass,TestReferenceFunctional", "TestNamespace");

			Assert.True(generator.HasError);
			Assert.Equal("Sealed class 'TestReferenceFunctional.BaseClasses.SealedClass,TestReferenceFunctional' may not be traced.", generator.Error);
		}

		[Fact]
		public void Construct_TraceFactoryGenerator_ByType_ReturnsErrorMessage()
		{
			var generator = new TraceFactoryGenerator(typeof(SealedClass), typeof(EmptyClass), "TestNamespace");

			Assert.True(generator.HasError);
			Assert.Equal("Sealed class 'TestReferenceFunctional.BaseClasses.SealedClass,TestReferenceFunctional' may not be traced.", generator.Error);
		}

		[Fact]
		public void Construct_TraceFactoryGenerator_TypeName_ReturnsErrorMessage()
		{
			var generator = new TraceFactoryGenerator("TestReferenceFunctional.BaseClasses.SealedClass,TestReferenceFunctional",
				"TestReferenceFunctional.BaseClasses.EmptyClass",
				"TestNamespace");

			Assert.True(generator.HasError);
			Assert.Equal("Sealed class 'TestReferenceFunctional.BaseClasses.SealedClass,TestReferenceFunctional' may not be traced.", generator.Error);
		}
	}
}
