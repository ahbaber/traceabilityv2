﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class IgnoreTests
	{
		private readonly BindingFlags Flags = Visibility.Flags | BindingFlags.Static;

		private class BaseTestClass
		{
			public int NotVirtualProp { get; set; }
			public virtual int VirtualProp { get; set; }
			public virtual int SealedProp { get; set; }
			public static int StaticProp { get; set; }
			private int PrivateProp { get; set; }

			public void NotVirtualMethod() { }
			public virtual void VirtualMethod() { }
			public virtual void SealedMethod() { }
			public static void StaticMethod() { }
			private void PrivateMethod() { }
		}

		private class TestClass : BaseTestClass
		{
			public override sealed int SealedProp { get => base.SealedProp; set => base.SealedProp = value; }
			public override sealed void SealedMethod() { base.SealedMethod(); }
		}

		private MethodInfo GetMethodInfo(Type baseType, string methodName)
		{
			return baseType.GetMethod(methodName, Flags);
		}

		private PropertyInfo GetPropertyInfo(Type baseType, string propertyName)
		{
			return baseType.GetProperty(propertyName, Flags);
		}

		[Theory]
		[InlineData("NotVirtualProp", IgnoreReason.NotVirtual)]
		[InlineData("VirtualProp", IgnoreReason.Unspecified)]
		[InlineData("StaticProp", IgnoreReason.IsStatic)]
		[InlineData("PrivateProp", IgnoreReason.IsPrivate)]
		public void GetIgnoreReason_Properties_ReturnsExpectedValue(string name, IgnoreReason expected)
		{
			var propertyInfo = GetPropertyInfo(typeof(BaseTestClass), name);

			Assert.Equal(expected, propertyInfo.GetIgnoreReason());
		}

		[Fact]
		public void GetIgnoredReason_SealedProperty_ReturnsExpectedValue()
		{
			var propertyInfo = GetPropertyInfo(typeof(TestClass), "SealedProp");

			Assert.Equal(IgnoreReason.IsFinal, propertyInfo.GetIgnoreReason());
		}

		[Theory]
		[InlineData("NotVirtualProp", IgnoreReason.NotVirtual)]
		[InlineData("VirtualProp", IgnoreReason.NotImplemented)]
		[InlineData("StaticProp", IgnoreReason.IsStatic)]
		[InlineData("PrivateProp", IgnoreReason.IsPrivate)]
		public void GetUntracedReason_Properties_ReturnsExpectedValue(string name, IgnoreReason expected)
		{
			var propertyInfo = GetPropertyInfo(typeof(BaseTestClass), name);

			Assert.Equal(expected, propertyInfo.GetUntracedReason());
		}

		[Fact]
		public void GetUntracedReason_SealedProperty_ReturnsExpectedValue()
		{
			var propertyInfo = GetPropertyInfo(typeof(TestClass), "SealedProp");

			Assert.Equal(IgnoreReason.IsFinal, propertyInfo.GetUntracedReason());
		}

		[Theory]
		[InlineData("NotVirtualMethod", IgnoreReason.NotVirtual)]
		[InlineData("VirtualMethod", IgnoreReason.Unspecified)]
		[InlineData("StaticMethod", IgnoreReason.IsStatic)]
		[InlineData("PrivateMethod", IgnoreReason.IsPrivate)]
		public void GetIgnoreReason_Methods_ReturnsExpectedValue(string name, IgnoreReason expected)
		{
			var methodInfo = GetMethodInfo(typeof(BaseTestClass), name);

			Assert.Equal(expected, methodInfo.GetIgnoreReason());
		}

		[Fact]
		public void GetIgnoredReason_SealedMethod_ReturnsExpectedValue()
		{
			var methodInfo = GetMethodInfo(typeof(TestClass), "SealedMethod");

			Assert.Equal(IgnoreReason.IsFinal, methodInfo.GetIgnoreReason());
		}

		[Theory]
		[InlineData("NotVirtualMethod", IgnoreReason.NotVirtual)]
		[InlineData("VirtualMethod", IgnoreReason.NotImplemented)]
		[InlineData("StaticMethod", IgnoreReason.IsStatic)]
		[InlineData("PrivateMethod", IgnoreReason.IsPrivate)]
		public void GetUntracedReason_Methods_ReturnsExpectedValue(string name, IgnoreReason expected)
		{
			var methodInfo = GetMethodInfo(typeof(BaseTestClass), name);

			Assert.Equal(expected, methodInfo.GetUntracedReason());
		}

		[Fact]
		public void GetUntracedReason_SealedMethod_ReturnsExpectedValue()
		{
			var methodInfo = GetMethodInfo(typeof(TestClass), "SealedMethod");

			Assert.Equal(IgnoreReason.IsFinal, methodInfo.GetUntracedReason());
		}
	}
}
