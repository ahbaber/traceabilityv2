﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using System.Text;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class PropertiesTraceClassMultiVisibilityTests
	{
		private class TestClass
		{
			public virtual string PublicGetPublicSet { get; set; }

			public virtual string PublicGetInternalSet { get; internal set; }
			public virtual string PublicGetProtectedSet { get; protected set; }
			public virtual string PublicGetProtectedInternalSet { get; protected internal set; }
			public virtual string PublicGetPrivateSet { get; private set; }

			public virtual string ProtectedGetPublicSet { protected get; set; }
			public virtual string InternalGetPublicSet { internal get; set; }
			public virtual string ProtectedInternalGetPublicSet { protected internal get; set; }
			public virtual string PrivateGetPublicSet { private get; set; }

			internal virtual string InternalGetInternalSet { get; set; }
			internal virtual string InternalGetPrivateSet { get; private set; }
			internal virtual string PrivateGetInternalSet { private get; set; }

			protected virtual string ProtectedGetProtectedSet { get; set; }
			protected virtual string ProtectedGetPrivateSet { get; private set; }
			protected virtual string PrivateGetProtectedSet { private get; set; }

			protected internal virtual string ProIntGetProIntSet { get; set; }
			protected internal virtual string ProIntGetProtectedSet { get; protected set; }
			protected internal virtual string ProtectedGetProIntSet { protected get; set; }
			protected internal virtual string ProIntGetInternalSet { get; internal set; }
			protected internal virtual string InternalGetProIntSet { internal get; set; }
			protected internal virtual string ProIntGetPrivateSet { get; private set; }
			protected internal virtual string PrivateGetProIntSet { private get; set; }
		}

		private PropertyInfo GetInfo(Type baseType, string propertyName)
		{
			return baseType.GetProperty(propertyName, Visibility.Flags);
		}

		[Theory]
		[InlineData("PublicGetPublicSet", "public", "", "")]

		[InlineData("PublicGetInternalSet", "public", "", "internal ")]
		[InlineData("PublicGetProtectedSet", "public", "", "protected ")]
		[InlineData("PublicGetProtectedInternalSet", "public", "", "protected internal ")]
		[InlineData("PublicGetPrivateSet", "public", "", "private ")]

		[InlineData("ProtectedGetPublicSet", "public", "protected ", "")]
		[InlineData("InternalGetPublicSet", "public", "internal ", "")]
		[InlineData("ProtectedInternalGetPublicSet", "public", "protected internal ", "")]
		[InlineData("PrivateGetPublicSet", "public", "private ", "")]

		[InlineData("InternalGetInternalSet", "internal", "", "")]
		[InlineData("InternalGetPrivateSet", "internal", "", "private ")]
		[InlineData("PrivateGetInternalSet", "internal", "private ", "")]

		[InlineData("ProtectedGetProtectedSet", "protected", "", "")]
		[InlineData("ProtectedGetPrivateSet", "protected", "", "private ")]
		[InlineData("PrivateGetProtectedSet", "protected", "private ", "")]

		[InlineData("ProIntGetProIntSet", "protected internal", "", "")]
		[InlineData("ProIntGetProtectedSet", "protected internal", "", "protected ")]
		[InlineData("ProtectedGetProIntSet", "protected internal", "protected ", "")]
		[InlineData("ProIntGetInternalSet", "protected internal", "", "internal ")]
		[InlineData("InternalGetProIntSet", "protected internal", "internal ", "")]
		[InlineData("ProIntGetPrivateSet", "protected internal", "", "private ")]
		[InlineData("PrivateGetProIntSet", "protected internal", "private ", "")]
		public void BuildProperty_ReturnsExpected(string propperty, string visibility, string getVisibility, string setVisibility)
		{
			var property = GetInfo(typeof(TestClass), propperty);

			string expected = @"
		" + visibility + @" override string " + propperty + @"
		{
			" + getVisibility + @"get { return _tracer.GetProperty(""TestClass." + propperty + @""", base." + propperty + @"); }
			" + setVisibility + @"set { base." + propperty + @" = _tracer.SetProperty(""TestClass." + propperty + @""", value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(TestClass)).ToString();

			Assert.Equal(expected, actual);
		}
	}
}
