﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class VisibilityTests
	{
		private MethodInfo GetMethodInfo(Type baseType, string methodName)
		{
			return baseType.GetMethod(methodName, Visibility.Flags);
		}

		private PropertyInfo GetPropertyInfo(Type baseType, string propertyName)
		{
			return baseType.GetProperty(propertyName, Visibility.Flags);
		}

		private class TestClass
		{
			public int PublicProperty { get; set; }
			protected int ProtectedProperty { get; set; }
			internal int InternalProperty { get; set; }
			protected internal int ProtectedInternalProperty { get; set; }
			private int _publicSet;
			public int PublicSet { set { _publicSet = value; } }

			public void PublicMethod() { }
			protected void ProtectedMethod() { }
			private void PrivateMethod() { }
			internal void InternalMethod() { }
			protected internal void ProtectedInternalMethod() { }
			// private protected not supported in dotnetcore 2.1
		}

		[Theory]
		[InlineData("PublicMethod", "public")]
		[InlineData("ProtectedMethod", "protected")]
		[InlineData("PrivateMethod", "private")]
		[InlineData("InternalMethod", "internal")]
		[InlineData("ProtectedInternalMethod", "protected internal")]
		public void GetVisibility_Methods_ReturnsVisibility(string methodName, string expected)
		{
			var method = GetMethodInfo(typeof(TestClass), methodName);

			string actual = Visibility.GetVisibility(method);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("PublicProperty", "public")]
		[InlineData("ProtectedProperty", "protected")]
		[InlineData("InternalProperty", "internal")]
		[InlineData("ProtectedInternalProperty", "protected internal")]
		[InlineData("PublicSet", "public")]
		public void GetVisibility_Properties_ReturnsVisibility(string propertyName, string expected)
		{
			var property = GetPropertyInfo(typeof(TestClass), propertyName);

			string actual = Visibility.GetVisibility(property);

			Assert.Equal(expected, actual);
		}
	}
}
