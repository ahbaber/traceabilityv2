﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Traceability;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class MethodsTraceFactoryTests
	{
		private class EmptyClass
		{
			public EmptyClass() { }
			public EmptyClass(int i) { }
		}
		private class TraceEmptyClass : EmptyClass
		{
			public TraceEmptyClass(ITracer tracer) { }
		}
		private class EmptyClassFactory
		{
			public virtual EmptyClass Construct()
			{ return new EmptyClass(); }
		}

		[Fact]
		public void Build_EmptyClass_NoCode()
		{
			var classInfo = new ClassInfo(typeof(EmptyClass), Visibility.PublicFlags);

			string expected = "";
			string actual = MethodsTraceFactory.Singleton.Build(new StringBuilder(), classInfo, typeof(TraceEmptyClass)).ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_EmptyClassFactory_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(EmptyClassFactory), Visibility.PublicFlags);

			string expected = @"
		public override EmptyClass Construct()
		{
			return new TraceEmptyClass(_tracer);
		}
";
			string actual = MethodsTraceFactory.Singleton.Build(new StringBuilder(), classInfo, typeof(TraceEmptyClass)).ToString();

			Assert.Equal(expected, actual);
		}

		private class EmptyClassFactory2
		{
			public virtual EmptyClass Construct()
			{ return new EmptyClass(); }
			public virtual EmptyClass Construct(int i)
			{ return new EmptyClass(i); }
		}

		[Fact]
		public void Build_EmptyClassFactory2_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(EmptyClassFactory2), Visibility.PublicFlags);

			string expected = @"
		public override EmptyClass Construct()
		{
			return new TraceEmptyClass(_tracer);
		}

		public override EmptyClass Construct(int i)
		{
			return new TraceEmptyClass(_tracer, i);
		}
";
			string actual = MethodsTraceFactory.Singleton.Build(new StringBuilder(), classInfo, typeof(TraceEmptyClass)).ToString();

			Assert.Equal(expected, actual);
		}

		private class NoVirtualFactory
		{
			public EmptyClass Construct()
			{ return new EmptyClass(); }
		}

		[Fact]
		public void Build_NoVirtual_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(NoVirtualFactory), Visibility.PublicFlags);

			string expected = @"
		// skipped NotVirtual Construct_TraceCodeGeneratorTests.SupportTests.MethodsTraceFactoryTests+EmptyClass
";
			string actual = MethodsTraceFactory.Singleton.Build(new StringBuilder(), classInfo, typeof(TraceEmptyClass)).ToString();

			Assert.Equal(expected, actual);
		}

		private class NoCosntructMethodFactory
		{
			public EmptyClass Build()
			{ return new EmptyClass(); }
		}

		[Fact]
		public void Build_NoCosntructMethodFactory_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(NoCosntructMethodFactory), Visibility.PublicFlags);

			string expected = @"
		// skipped not named Construct: Build_TraceCodeGeneratorTests.SupportTests.MethodsTraceFactoryTests+EmptyClass
";
			string actual = MethodsTraceFactory.Singleton.Build(new StringBuilder(), classInfo, typeof(TraceEmptyClass)).ToString();

			Assert.Equal(expected, actual);
		}

		private class GenericType<T>
		{
			public GenericType(T t) { }
		}

		private class TraceGenericType<T> : GenericType<T>
		{
			public TraceGenericType(T t): base(t) { }
		}

		private class GenericFactory<T>
		{
			public virtual GenericType<T> Construct(T t)
			{ return new GenericType<T>(t); }
		}

		[Fact]
		public void Build_GenericFactory_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(GenericFactory<>), Visibility.PublicFlags);

			string expected = @"
		public override GenericType<T> Construct(T t)
		{
			return new TraceGenericType<T>(_tracer, t);
		}
";
			string actual = MethodsTraceFactory.Singleton.Build(new StringBuilder(), classInfo, typeof(TraceGenericType<>)).ToString();

			Assert.Equal(expected, actual);
		}

		private class GenericMethodFactory
		{
			public virtual GenericType<T> Construct<T>(T t)
			{ return new GenericType<T>(t); }
		}

		[Fact]
		public void Build_GenericMethodFactory_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(GenericMethodFactory), Visibility.PublicFlags);

			string expected = @"
		public override GenericType<T> Construct<T>(T t)
		{
			return new TraceGenericType<T>(_tracer, t);
		}
";
			string actual = MethodsTraceFactory.Singleton.Build(new StringBuilder(), classInfo, typeof(TraceGenericType<>)).ToString();

			Assert.Equal(expected, actual);
		}
	}
}
