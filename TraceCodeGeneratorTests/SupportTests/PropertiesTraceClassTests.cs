﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using System.Text;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class PropertiesTraceClassTests
	{
		private PropertyInfo GetInfo(Type baseType, string propertyName)
		{
			return baseType.GetProperty(propertyName, Visibility.Flags);
		}

		private class TestClass
		{
			public virtual bool BoolRW { get; set; }
			public virtual int IntR { get; }
			public virtual long LongW { set { _longW = value; } }
			protected virtual short ProtectedRW { get; set; }
			internal virtual char InternalRW { get; set; }
			private long _longW;
		}

		[Fact]
		public void BuildProperty_BoolRW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(TestClass), "BoolRW");

			string expected = @"
		public override bool BoolRW
		{
			get { return _tracer.GetProperty(""TestClass.BoolRW"", base.BoolRW); }
			set { base.BoolRW = _tracer.SetProperty(""TestClass.BoolRW"", value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(TestClass)).ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void BuildProperty_IntR_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(TestClass), "IntR");

			string expected = @"
		public override int IntR
		{
			get { return _tracer.GetProperty(""TestClass.IntR"", base.IntR); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(TestClass)).ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void BuildProperty_LongW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(TestClass), "LongW");

			string expected = @"
		public override long LongW
		{
			set { base.LongW = _tracer.SetProperty(""TestClass.LongW"", value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(TestClass)).ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void BuildProperty_ProtectedRW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(TestClass), "ProtectedRW");

			string expected = @"
		protected override short ProtectedRW
		{
			get { return _tracer.GetProperty(""TestClass.ProtectedRW"", base.ProtectedRW); }
			set { base.ProtectedRW = _tracer.SetProperty(""TestClass.ProtectedRW"", value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(TestClass)).ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void BuildProperty_InternalRW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(TestClass), "InternalRW");

			string expected = @"
		internal override char InternalRW
		{
			get { return _tracer.GetProperty(""TestClass.InternalRW"", base.InternalRW); }
			set { base.InternalRW = _tracer.SetProperty(""TestClass.InternalRW"", value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(TestClass)).ToString();

			Assert.Equal(expected, actual);
		}

		private class IndexRW
		{
			public virtual int this[int i]
			{
				get { return _list[i]; }
				set { _list[i] = value; }
			}
			private System.Collections.Generic.List<int> _list = new System.Collections.Generic.List<int>();
		}

		[Fact]
		public void BuildProperty_IndexRW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(IndexRW), "Item");

			string expected = @"
		public override int this[int i]
		{
			get { return _tracer.GetIndexer(""IndexRW"", i, base[i]); }
			set { base[i] = _tracer.SetIndexer(""IndexRW"", i, value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(IndexRW)).ToString();

			Assert.Equal(expected, actual);
		}

		private class IndexR
		{
			public virtual int this[int i]
			{
				get { return _list[i]; }
			}
			private System.Collections.Generic.List<int> _list = new System.Collections.Generic.List<int>();
		}

		[Fact]
		public void BuildProperty_IndexR_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(IndexR), "Item");

			string expected = @"
		public override int this[int i]
		{
			get { return _tracer.GetIndexer(""IndexR"", i, base[i]); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(IndexR)).ToString();

			Assert.Equal(expected, actual);
		}

		private class IndexW
		{
			public virtual int this[int i]
			{
				set { _list[i] = value; }
			}
			private System.Collections.Generic.List<int> _list = new System.Collections.Generic.List<int>();
		}

		[Fact]
		public void BuildProperty_IndexW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(IndexW), "Item");

			string expected = @"
		public override int this[int i]
		{
			set { base[i] = _tracer.SetIndexer(""IndexW"", i, value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(IndexW)).ToString();

			Assert.Equal(expected, actual);
		}

		private class ProtectedIndexRW
		{
			protected virtual int this[int i]
			{
				get { return _list[i]; }
				set { _list[i] = value; }
			}
			private System.Collections.Generic.List<int> _list = new System.Collections.Generic.List<int>();
		}

		[Fact]
		public void BuildProperty_ProtectedIndexRW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(ProtectedIndexRW), "Item");

			string expected = @"
		protected override int this[int i]
		{
			get { return _tracer.GetIndexer(""ProtectedIndexRW"", i, base[i]); }
			set { base[i] = _tracer.SetIndexer(""ProtectedIndexRW"", i, value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(ProtectedIndexRW)).ToString();

			Assert.Equal(expected, actual);
		}

		private class InternalIndexRW
		{
			internal virtual int this[int i]
			{
				get { return _list[i]; }
				set { _list[i] = value; }
			}
			private System.Collections.Generic.List<int> _list = new System.Collections.Generic.List<int>();
		}

		[Fact]
		public void BuildProperty_InternalIndexRW_ReturnsExpectedCode()
		{
			var property = GetInfo(typeof(InternalIndexRW), "Item");

			string expected = @"
		internal override int this[int i]
		{
			get { return _tracer.GetIndexer(""InternalIndexRW"", i, base[i]); }
			set { base[i] = _tracer.SetIndexer(""InternalIndexRW"", i, value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), property, typeof(InternalIndexRW)).ToString();

			Assert.Equal(expected, actual);
		}

		private class BaseVirtualClass
		{
			public virtual int Property { get; set; }
		}

		private class ChildClass : BaseVirtualClass
		{
			public sealed override int Property { get => base.Property; set => base.Property = value; }
		}

		[Fact]
		public void BuildProperties_SealedProperty_ReturnsSkipMessage()
		{
			var classinfo = new ClassInfo(typeof(ChildClass), Visibility.Flags);

			string expected = @"
		// skipped IsFinal int Property
";
			var actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), classinfo).ToString();

			Assert.Equal(expected, actual);
		}

		private class NonVirtualPropClass
		{
			public int Property { get; set; }
		}

		[Fact]
		public void BuildProperties_NonVirtualProperty_ReturnsSkipMessage()
		{
			var classinfo = new ClassInfo(typeof(NonVirtualPropClass), Visibility.Flags);

			string expected = @"
		// skipped NotVirtual int Property
";
			var actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), classinfo).ToString();

			Assert.Equal(expected, actual);
		}

		private class PrivateProperty
		{
			private int PriProperty { get; set; }
		}

		[Fact]
		public void BuildProperties_PrivateProperty_ReturnsSkipMessage()
		{
			var classinfo = new ClassInfo(typeof(PrivateProperty), Visibility.Flags);

			string expected = @"
		// skipped IsPrivate int PriProperty
";
			var actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), classinfo).ToString();

			Assert.Equal(expected, actual);
		}

		private class GenericProperty<T>
		{
			public virtual T Property { get; set; }
		}

		[Fact]
		public void BuildProperties_GenericProperty_ReturnsExpectedCode()
		{
			var classinfo = new ClassInfo(typeof(GenericProperty<>), Visibility.Flags);

			string expected = @"
		public override T Property
		{
			get { return _tracer.GetProperty(""GenericProperty.Property"", base.Property); }
			set { base.Property = _tracer.SetProperty(""GenericProperty.Property"", value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), classinfo).ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void BuildProperties_GenericPropertyInt_ReturnsExpectedCode()
		{
			var classinfo = new ClassInfo(typeof(GenericProperty<int>), Visibility.Flags);

			string expected = @"
		public override int Property
		{
			get { return _tracer.GetProperty(""GenericProperty_int.Property"", base.Property); }
			set { base.Property = _tracer.SetProperty(""GenericProperty_int.Property"", value); }
		}
";
			string actual = PropertiesTraceClass.Singleton.Build(new StringBuilder(), classinfo).ToString();

			Assert.Equal(expected, actual);
		}
	}
}
