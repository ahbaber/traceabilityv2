﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class MethodsTraceClassTests
	{
		private MethodInfo GetMethodInfo(Type baseType, string methodName)
		{
			return baseType.GetMethod(methodName, Visibility.Flags);
		}

		private class TestClass
		{
			public virtual void VoidNoParams() { }
			protected virtual void ProtectedNoParams() { }
			internal virtual void InternalNoParams() { }
			public virtual string StringReturn() { return ""; }
			public virtual void IntParam(int i) { }
			public virtual void BoolStringParams(bool b, string str) { }
			public virtual void RefParam(ref int i) { }
			public virtual void OutParam(out string str) { str = ""; }
		}

		[Fact]
		public void Build_VoidNoParams_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "VoidNoParams");

			string expected = @"
		public override void VoidNoParams()
		{
			_tracer.FunctionStart(""TestClass.VoidNoParams"");
			base.VoidNoParams();
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_ProtectedNoParams_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "ProtectedNoParams");

			string expected = @"
		protected override void ProtectedNoParams()
		{
			_tracer.FunctionStart(""TestClass.ProtectedNoParams"");
			base.ProtectedNoParams();
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_InternalNoParams_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "InternalNoParams");

			string expected = @"
		internal override void InternalNoParams()
		{
			_tracer.FunctionStart(""TestClass.InternalNoParams"");
			base.InternalNoParams();
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_StringReturn_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "StringReturn");

			string expected = @"
		public override string StringReturn()
		{
			_tracer.FunctionStart(""TestClass.StringReturn"");
			var result = base.StringReturn();
			return _tracer.FunctionEnd(result);
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_IntParam_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "IntParam");

			string expected = @"
		public override void IntParam(int i)
		{
			_tracer.FunctionStart(""TestClass.IntParam"");
			_tracer.Parameter(""i"", i);
			base.IntParam(i);
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_BoolStringParams_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "BoolStringParams");

			string expected = @"
		public override void BoolStringParams(bool b, string str)
		{
			_tracer.FunctionStart(""TestClass.BoolStringParams"");
			_tracer.Parameter(""b"", b);
			_tracer.Parameter(""str"", str);
			base.BoolStringParams(b, str);
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_RefParam_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "RefParam");

			string expected = @"
		public override void RefParam(ref int i)
		{
			_tracer.FunctionStart(""TestClass.RefParam"");
			_tracer.Parameter(""i"", i);
			base.RefParam(ref i);
			_tracer.Parameter(""i_out"", i);
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_OutParam_ExpdectedCodeReturned()
		{
			var info = GetMethodInfo(typeof(TestClass), "OutParam");

			string expected = @"
		public override void OutParam(out string str)
		{
			_tracer.FunctionStart(""TestClass.OutParam"");
			base.OutParam(out str);
			_tracer.Parameter(""str_out"", str);
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "TestClass").ToString();

			Assert.Equal(expected, actual);
		}

		private class ParameterizedClass
		{
			public void Method1() { }
			public virtual int Method2() { return 0; }
			public virtual int Method2(int i) { return 0; }
			public int Method2(int i, bool b) { return 0; }
			public void Method3(out int i) { i = 0; }
			public void Method4(ref short s) { }
			public short Method5(ParameterizedClass pc) { return 0; }
		}

		[Fact]
		public void GetAllParameterizedNames_ReturnsExpectedNames()
		{
			var expected = new List<string>
			{
				"Equals_System.Boolean_System.Object",
				"GetHashCode_System.Int32",
				"GetType_System.Type",
				"ToString_System.String",
				"Method1_System.Void",
				"Method2_System.Int32",
				"Method2_System.Int32_System.Int32",
				"Method2_System.Int32_System.Int32_System.Boolean",
				"Method3_System.Void_System.Int32&",
				"Method4_System.Void_System.Int16&",
				"Method5_System.Int16_TraceCodeGeneratorTests.SupportTests.MethodsTraceClassTests+ParameterizedClass",
			};
			expected.Sort();
			var actual = typeof(ParameterizedClass).GetAllParameterizedNames();
			actual.Sort();

			Assert.Equal(expected, actual);
		}

		private class BaseClass
		{
			public virtual void Method() { }
		}

		private class ChildClass : BaseClass
		{
			public sealed override void Method()
			{
				base.Method();
			}
		}

		[Fact]
		public void Build_SealedMethod_ReturnsSkippedMessage()
		{
			var classInfo = new ClassInfo(typeof(ChildClass), Visibility.Flags);

			string expected = @"
		// skipped IsFinal Method_System.Void
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), classInfo).ToString();

			Assert.Equal(expected, actual);
		}

		private class NonVirtual
		{
			public void Method() { }
		}

		[Fact]
		public void Build_NonVirtualMethod_ReturnsSkippedMessage()
		{
			var classInfo = new ClassInfo(typeof(NonVirtual), Visibility.Flags);

			string expected = @"
		// skipped NotVirtual Method_System.Void
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), classInfo).ToString();

			Assert.Equal(expected, actual);
		}

		private class PrivateMethods
		{
			private void Method() { }
		}

		[Fact]
		public void Build_PrivateMethod_ReturnsSkippedMessage()
		{
			var classInfo = new ClassInfo(typeof(PrivateMethods), Visibility.Flags);

			string expected = @"
		// skipped IsPrivate Method_System.Void
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), classInfo).ToString();

			Assert.Equal(expected, actual);
		}

		private class StaticMethods
		{
			public static void Method() { }
		}

		[Fact]
		public void Build_StaticMethod_ReturnsEmptyString()
		{
			var classInfo = new ClassInfo(typeof(StaticMethods), Visibility.Flags);

			string expected = "";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), classInfo).ToString();

			Assert.Equal(expected, actual);
		}

		private class GenericReturnMethod<T>
		{
			public virtual T Method() { return _t; }
#pragma warning disable CS0649 // it's not being used!
			private T _t;
#pragma warning disable CS0649 // it's not being used!
		}

		[Fact]
		public void Build_GenericReturnMethod_ReturnsExpectedCode()
		{
			var info = GetMethodInfo(typeof(GenericReturnMethod<>), "Method");

			string expected = @"
		public override T Method()
		{
			_tracer.FunctionStart(""GenericReturnMethod.Method"");
			var result = base.Method();
			return _tracer.FunctionEnd(result);
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "GenericReturnMethod").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_GenericIntReturnMethod_ReturnsExpectedCode()
		{
			var info = GetMethodInfo(typeof(GenericReturnMethod<int>), "Method");

			string expected = @"
		public override int Method()
		{
			_tracer.FunctionStart(""GenericReturnMethod_int.Method"");
			var result = base.Method();
			return _tracer.FunctionEnd(result);
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "GenericReturnMethod_int").ToString();

			Assert.Equal(expected, actual);
		}

		private class GenericParameterMethod<T>
		{
			public virtual void Method(T t) { }
		}

		[Fact]
		public void Build_GenericParameterMethod_ReturnsExpectedCode()
		{
			var info = GetMethodInfo(typeof(GenericParameterMethod<>), "Method");

			string expected = @"
		public override void Method(T t)
		{
			_tracer.FunctionStart(""GenericParameterMethod.Method"");
			_tracer.Parameter(""t"", t);
			base.Method(t);
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "GenericParameterMethod").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_GenericIntParameterMethod_ReturnsExpectedCode()
		{
			var info = GetMethodInfo(typeof(GenericParameterMethod<int>), "Method");

			string expected = @"
		public override void Method(int t)
		{
			_tracer.FunctionStart(""GenericParameterMethod_int.Method"");
			_tracer.Parameter(""t"", t);
			base.Method(t);
			_tracer.FunctionEnd();
		}
";
			string actual = MethodsTraceClass.Singleton.Build(new StringBuilder(), info, "GenericParameterMethod_int").ToString();

			Assert.Equal(expected, actual);
		}
	}
}
