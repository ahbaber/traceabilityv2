﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TestReferenceFunctional.BaseClasses;
using TraceCodeGenerator;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class ClassInfoTests
	{
		[Fact]
		public void Construct_ReturnsExpectedFields()
		{
			var actual = new ClassInfo(typeof(EmptyClass), Visibility.Flags);

			Assert.Equal(typeof(EmptyClass).Name, actual.ClassType.Name);
			Assert.Single(actual.Constructors);
			Assert.Empty(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Empty(actual.Methods);
			Assert.Empty(actual.MethodsSkipped);
		}

		[Fact]
		public void Construct_FromTypeName_ReturnsExpectedFields()
		{
			string typeName = "TestReferenceFunctional.BaseClasses.EmptyClass";
			var actual = new ClassInfo(typeName, Visibility.Flags);

			Assert.Equal(typeof(EmptyClass).Name, actual.ClassType.Name);
			Assert.Single(actual.Constructors);
			Assert.Empty(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Empty(actual.Methods);
			Assert.Empty(actual.MethodsSkipped);
		}

		[Fact]
		public void Construct_NullType_ReturnsExepctedClassType()
		{
			Type type = null;
			var actual = new ClassInfo(type, Visibility.Flags);

			Assert.Null(actual.ClassType);
		}

		[Theory]
		[InlineData(null)]
		[InlineData("")]
		[InlineData("DoesNotExist")]
		public void Construct_InvalidTypeName_ReturnsExepctedClassType(string typeName)
		{
			var actual = new ClassInfo(typeName, Visibility.Flags);

			Assert.Null(actual.ClassType);
		}

		private class ProtectedConstructor
		{
			protected ProtectedConstructor() { }
		}

		private class InternalConstructor
		{
			internal InternalConstructor() { }
		}

		private class ProtectedInternalConstructor
		{
			protected internal ProtectedInternalConstructor() { }
		}

		private class PrivateConstructor
		{
			private PrivateConstructor() { }
		}

		private class Constructor2
		{
			public Constructor2() { }
			public Constructor2(int i) { }
		}

		[Theory]
		[InlineData(typeof(ProtectedConstructor), 1)]
		[InlineData(typeof(InternalConstructor), 1)]
		[InlineData(typeof(ProtectedInternalConstructor), 1)]
		[InlineData(typeof(PrivateConstructor), 1)]
		[InlineData(typeof(Constructor2), 2)]
		public void Construct_ReturnsExpectedConstructorCount(Type type, int expected)
		{
			var actual = new ClassInfo(type, Visibility.Flags);

			Assert.Equal(expected, actual.Constructors.Count);
			Assert.Empty(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Empty(actual.Methods);
			Assert.Empty(actual.MethodsSkipped);
		}

		[Theory]
		[InlineData(typeof(ProtectedConstructor), 1)]
		[InlineData(typeof(InternalConstructor), 1)]
		[InlineData(typeof(ProtectedInternalConstructor), 1)]
		[InlineData(typeof(PrivateConstructor), 1)]
		[InlineData(typeof(Constructor2), 2)]
		public void Construct_FromTypeName_ReturnsExpectedConstructorCount(Type type, int expected)
		{
			string typeName = type.FullName;
			var actual = new ClassInfo(typeName, Visibility.Flags);

			Assert.Equal(expected, actual.Constructors.Count);
			Assert.Empty(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Empty(actual.Methods);
			Assert.Empty(actual.MethodsSkipped);
		}

		private class PublicPropertyClass
		{
			public int PublicProperty { get; set; }
		}

		private class ProtectedPropertyClass
		{
			protected int ProtectedProperty { get; set; }
		}

		private class InternalPropertyClass
		{
			internal int InternalProperty { get; set; }
		}

		private class ProtectedInternalPropertyClass
		{
			protected internal int ProtectedInternalProperty { get; set; }
		}

		private class PrivatePropertyClass
		{
			private string PrivateProperty => "Keep out!";
		}

		[Theory]
		[InlineData(typeof(PublicPropertyClass), "PublicProperty")]
		[InlineData(typeof(ProtectedPropertyClass), "ProtectedProperty")]
		[InlineData(typeof(InternalPropertyClass), "InternalProperty")]
		[InlineData(typeof(ProtectedInternalPropertyClass), "ProtectedInternalProperty")]
		[InlineData(typeof(PrivatePropertyClass), "PrivateProperty")]
		public void Construct_ReturnsExpectedProperty(Type type, string expected)
		{
			var actual = new ClassInfo(type, Visibility.Flags);

			Assert.Single(actual.Constructors);
			Assert.Single(actual.Properties);
			Assert.Equal(expected, actual.Properties[0].Name);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Empty(actual.Methods);
			Assert.Empty(actual.MethodsSkipped);
		}

		[Theory]
		[InlineData(typeof(PublicPropertyClass), "PublicProperty")]
		[InlineData(typeof(ProtectedPropertyClass), "ProtectedProperty")]
		[InlineData(typeof(InternalPropertyClass), "InternalProperty")]
		[InlineData(typeof(ProtectedInternalPropertyClass), "ProtectedInternalProperty")]
		[InlineData(typeof(PrivatePropertyClass), "PrivateProperty")]
		public void Construct_FromTypeName_ReturnsExpectedProperty(Type type, string expected)
		{
			string typeName = type.FullName;
			var actual = new ClassInfo(typeName, Visibility.Flags);

			Assert.Single(actual.Constructors);
			Assert.Single(actual.Properties);
			Assert.Equal(expected, actual.Properties[0].Name);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Empty(actual.Methods);
			Assert.Empty(actual.MethodsSkipped);
		}

		private class PublicMethodClass
		{
			public void PublicMethod() { }
		}

		private class ProtectedMethodClass
		{
			protected void ProtectedMethod() { }
		}

		private class InternalMethodClass
		{
			internal void InternalMethod() { }
		}

		private class ProtectedInternalMethodClass
		{
			protected internal void ProtectedInternalMethod() { }
		}

		private class PrivateMethodClass
		{
			private void PrivateMethod() { }
		}

		[Theory]
		[InlineData(typeof(PublicMethodClass), "PublicMethod")]
		[InlineData(typeof(ProtectedMethodClass), "ProtectedMethod")]
		[InlineData(typeof(InternalMethodClass), "InternalMethod")]
		[InlineData(typeof(ProtectedInternalMethodClass), "ProtectedInternalMethod")]
		[InlineData(typeof(PrivateMethodClass), "PrivateMethod")]
		public void Construct_ReturnsExpectedMethod(Type type, string expected)
		{
			var actual = new ClassInfo(type, Visibility.Flags);

			Assert.Single(actual.Constructors);
			Assert.Empty(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Single(actual.Methods);
			Assert.Equal(expected, actual.Methods[0].Name);
			Assert.Empty(actual.MethodsSkipped);
		}

		[Theory]
		[InlineData(typeof(PublicMethodClass), "PublicMethod")]
		[InlineData(typeof(ProtectedMethodClass), "ProtectedMethod")]
		[InlineData(typeof(InternalMethodClass), "InternalMethod")]
		[InlineData(typeof(ProtectedInternalMethodClass), "ProtectedInternalMethod")]
		[InlineData(typeof(PrivateMethodClass), "PrivateMethod")]
		public void Construct_FromTypeName_ReturnsExpectedMethod(Type type, string expected)
		{
			string typeName = type.FullName;
			var actual = new ClassInfo(typeName, Visibility.Flags);

			Assert.Single(actual.Constructors);
			Assert.Empty(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Single(actual.Methods);
			Assert.Equal(expected, actual.Methods[0].Name);
			Assert.Empty(actual.MethodsSkipped);
		}

		private class PropertyAndMethodClass
		{
			public int Property { get; set; }
			public void Method() { }
		}

		[Fact]
		public void Construct_PropertyAndMethodInAppropriateLists()
		{
			// making sure properties do not end up in the Methods list
			var actual = new ClassInfo(typeof(PropertyAndMethodClass), Visibility.Flags);

			Assert.Single(actual.Constructors);
			Assert.Single(actual.Properties);
			Assert.Equal("Property", actual.Properties[0].Name);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Single(actual.Methods);
			Assert.Equal("Method", actual.Methods[0].Name);
			Assert.Empty(actual.MethodsSkipped);
		}

		private class TestSkipClass
		{
			public virtual int IntProp { get; set; }
			public virtual void Method1() { }
			public virtual void Method1(int i) { }
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void Construct_PropertySkipped(bool isStrict)
		{
			var skipMap = new SkipMap().Add("IntProp", isStrict);
			var actual = new ClassInfo(typeof(TestSkipClass), Visibility.Flags, skipMap);

			Assert.Single(actual.Constructors);
			Assert.Empty(actual.Properties);
			Assert.Single(actual.PropertiesSkipped);
			Assert.Equal("IntProp", actual.PropertiesSkipped[0].Name);
			Assert.Equal(2, actual.Methods.Count);
			Assert.Empty(actual.MethodsSkipped);
		}

		[Fact]
		public void Construct_BothMethodsSkipped()
		{
			var skipMap = new SkipMap().Add("Method1", false);
			var actual = new ClassInfo(typeof(TestSkipClass), Visibility.Flags, skipMap);

			Assert.Single(actual.Constructors);
			Assert.Single(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Empty(actual.Methods);
			Assert.Equal(2, actual.MethodsSkipped.Count);
			Assert.Equal("Method1", actual.MethodsSkipped[0].Name);
			Assert.Equal("Method1", actual.MethodsSkipped[1].Name);
		}

		[Fact]
		public void Construct_OneMethodSkipped()
		{
			var skipMap = new SkipMap().Add("Method1_System.Void_System.Int32", true);
			var actual = new ClassInfo(typeof(TestSkipClass), Visibility.Flags, skipMap);

			Assert.Single(actual.Constructors);
			Assert.Single(actual.Properties);
			Assert.Empty(actual.PropertiesSkipped);
			Assert.Single(actual.Methods);
			Assert.Equal("Method1_System.Void", actual.Methods[0].GetParameterizedName());
			Assert.Single(actual.MethodsSkipped);
			Assert.Equal("Method1_System.Void_System.Int32", actual.MethodsSkipped[0].GetParameterizedName());
		}

		private class SkipClass
		{
			public int IntProp { get; set; }
			public void Method1() { }
			public void OtherMethod() { }
		}

		[Fact]
		public void Construct_OneMethodSkippedOneMethodSkipped()
		{
			var skipMap = new SkipMap().Add(typeof(SkipClass));
			var actual = new ClassInfo(typeof(TestSkipClass), Visibility.Flags, skipMap);

			Assert.Single(actual.Constructors);
			Assert.Empty(actual.Properties);
			Assert.Single(actual.PropertiesSkipped);
			Assert.Equal("IntProp", actual.PropertiesSkipped[0].Name);
			Assert.Single(actual.Methods);
			Assert.Equal("Method1_System.Void_System.Int32", actual.Methods[0].GetParameterizedName());
			Assert.Single(actual.MethodsSkipped);
			Assert.Equal("Method1_System.Void", actual.MethodsSkipped[0].GetParameterizedName());
		}

		[Fact]
		public void AddNamespace_SingleNamespace_ReturnsExpectedNamespace()
		{
			var classInfo = new ClassInfo(typeof(EmptyClass), Visibility.Flags);

			classInfo.AddNamespace("TestNamespace");

			Assert.Equal(2, classInfo.Namespaces.Count);
			Assert.Contains("TestReferenceFunctional.BaseClasses", classInfo.Namespaces);
			Assert.Contains("TestNamespace", classInfo.Namespaces);
		}

		[Fact]
		public void AddNamespace_TwoNamespace_ReturnsExpectedNamespace()
		{
			var classInfo = new ClassInfo(typeof(EmptyClass), Visibility.Flags);

			classInfo.AddNamespace("TestNamespace1");
			classInfo.AddNamespace("TestNamespace2");

			Assert.Equal(3, classInfo.Namespaces.Count);
			Assert.Contains("TestReferenceFunctional.BaseClasses", classInfo.Namespaces);
			Assert.Contains("TestNamespace1", classInfo.Namespaces);
			Assert.Contains("TestNamespace2", classInfo.Namespaces);
		}

		[Fact]
		public void AddNamespace_RepeatedNamespace_ReturnsExpectedNamespace()
		{
			var classInfo = new ClassInfo(typeof(EmptyClass), Visibility.Flags);

			classInfo.AddNamespace("TestNamespace");
			classInfo.AddNamespace("TestNamespace");

			Assert.Equal(2, classInfo.Namespaces.Count);
			Assert.Contains("TestReferenceFunctional.BaseClasses", classInfo.Namespaces);
			Assert.Contains("TestNamespace", classInfo.Namespaces);
		}

		private class TestClassWithParameterInConstructor
		{
			public TestClassWithParameterInConstructor(System.Collections.Generic.List<int> list){ }
		}

		[Fact]
		public void AddNamespace_ConstructorParameter_ExpectedNamespaces()
		{
			var classInfo = new ClassInfo(typeof(TestClassWithParameterInConstructor), Visibility.Flags);

			Assert.Equal(3, classInfo.Namespaces.Count);
			Assert.Contains("TraceCodeGeneratorTests.SupportTests", classInfo.Namespaces);
			Assert.Contains("System.Collections.Generic", classInfo.Namespaces);
			Assert.Contains("System", classInfo.Namespaces);
		}

		private class TestClassWithReturnType
		{
			public System.Collections.Generic.List<int> Method() { return null; }
		}

		[Fact]
		public void AddNamespace_MethodReturn_ReturnsExpectedNamespaces()
		{
			var classInfo = new ClassInfo(typeof(TestClassWithReturnType), Visibility.Flags);

			Assert.Equal(3, classInfo.Namespaces.Count);
			Assert.Contains("TraceCodeGeneratorTests.SupportTests", classInfo.Namespaces);
			Assert.Contains("System.Collections.Generic", classInfo.Namespaces);
			Assert.Contains("System", classInfo.Namespaces);
		}

		private class TestClassWithMethodParameter
		{
			public void Method(System.Collections.Generic.List<int> list) { }
		}

		[Fact]
		public void AddNamespace_MethodParameter_ReturnsExpectedNamespaces()
		{
			var classInfo = new ClassInfo(typeof(TestClassWithMethodParameter), Visibility.Flags);

			Assert.Equal(3, classInfo.Namespaces.Count);
			Assert.Contains("TraceCodeGeneratorTests.SupportTests", classInfo.Namespaces);
			Assert.Contains("System.Collections.Generic", classInfo.Namespaces);
			Assert.Contains("System", classInfo.Namespaces);
		}

		private class TestWithProperty
		{
			public System.Collections.Generic.List<int> Property {  get { return null; } }
		}

		[Fact]
		public void AddNamespace_Property_ReturnsExpectedNamespaces()
		{
			var classInfo = new ClassInfo(typeof(TestWithProperty), Visibility.Flags);

			Assert.Equal(3, classInfo.Namespaces.Count);
			Assert.Contains("TraceCodeGeneratorTests.SupportTests", classInfo.Namespaces);
			Assert.Contains("System.Collections.Generic", classInfo.Namespaces);
			Assert.Contains("System", classInfo.Namespaces);
		}

		private class TestWithNestedProperty
		{
			public System.Collections.Generic.List<SkipMap> Property {  get { return null; } }
		}

		[Fact]
		public void AddNamespace_NestedProperty_ReturnsExpectedNamespaces()
		{
			var classInfo = new ClassInfo(typeof(TestWithNestedProperty), Visibility.Flags);

			Assert.Equal(3, classInfo.Namespaces.Count);
			Assert.Contains("TraceCodeGeneratorTests.SupportTests", classInfo.Namespaces);
			Assert.Contains("System.Collections.Generic", classInfo.Namespaces);
			Assert.Contains("TraceCodeGenerator", classInfo.Namespaces);
		}
	}
}
