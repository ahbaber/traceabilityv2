﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using System.Text;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class ParametersTests
	{
		private static ParameterInfo[] GetParameters(Type baseType, string methodName)
		{
			var methodInfo = baseType.GetMethod(methodName);
			return methodInfo.GetParameters();
		}

		private class TestClass
		{
			public void NoParams() { }
			public void BoolParam(bool b) { }
			public void ByteParam(byte by) { }
			public void SByteParam(sbyte sb) { }
			public void CharParam(char c) { }
			public void DecimalParam(decimal d) { }
			public void DoubleParam(double d) { }
			public void FloatParam(float f) { }
			public void IntParam(int i) { }
			public void UIntParam(uint ui) { }
			public void LongParam(long l) { }
			public void ULongParam(ulong ul) { }
			public void ObjectParam(object obj) { }
			public void ShortParam(short s) { }
			public void UShortParam(ushort us) { }
			public void StringParam(string str) { }
			public void ClassParam(TestClass test) { }
			public void IntInt(int a, int b) { }
			public void IntStr(int i, string s) { }
			public void DefaultBoolParam(bool b = false) { }
			public void DefaultByteParam(byte by = 6) { }
			public void DefaultSByteParam(sbyte sb = -3) { }
			public void DefaultCharParam(char c = 'c') { }
			public void DefaultDecimalParam(decimal d = 12.3m) { }
			public void DefaultDoubleParam(double d = 4.2) { }
			public void DefaultFloatParam(float f = 2.1f) { }
			public void DefaultIntParam(int i = -5) { }
			public void DefaultUIntParam(uint ui = 5) { }
			public void DefaultLongParam(long l = -3) { }
			public void DefaultULongParam(ulong ul = 6) { }
			public void DefaultObjectParam(object obj = null) { }
			public void DefaultShortParam(short s = -3) { }
			public void DefaultUShortParam(ushort us = 7) { }
			public void DefaultStringParam(string str = "hello") { }
			public void DefaultClassParam(TestClass test = null) { }
			public void DefaultIntInt(int a = 5, int b = 6) { }
			public void DefaultIntStr(int i = 2, string s = "world") { }
			public void OutString(out string str) { str = ""; }
			public void RefInt(ref int i) { }
			public void NullableInt(int? ni) { }
			public void Generic<T>(T t) { }
		}

		[Theory]
		[InlineData("NoParams", "()")]
		[InlineData("BoolParam", "(b)")]
		[InlineData("ByteParam", "(by)")]
		[InlineData("SByteParam", "(sb)")]
		[InlineData("CharParam", "(c)")]
		[InlineData("DecimalParam", "(d)")]
		[InlineData("DoubleParam", "(d)")]
		[InlineData("FloatParam", "(f)")]
		[InlineData("IntParam", "(i)")]
		[InlineData("UIntParam", "(ui)")]
		[InlineData("LongParam", "(l)")]
		[InlineData("ULongParam", "(ul)")]
		[InlineData("ObjectParam", "(obj)")]
		[InlineData("ShortParam", "(s)")]
		[InlineData("UShortParam", "(us)")]
		[InlineData("StringParam", "(str)")]
		[InlineData("ClassParam", "(test)")]
		[InlineData("IntInt", "(a, b)")]
		[InlineData("IntStr", "(i, s)")]
		[InlineData("DefaultBoolParam", "(b)")]
		[InlineData("DefaultByteParam", "(by)")]
		[InlineData("DefaultSByteParam", "(sb)")]
		[InlineData("DefaultCharParam", "(c)")]
		[InlineData("DefaultDecimalParam", "(d)")]
		[InlineData("DefaultDoubleParam", "(d)")]
		[InlineData("DefaultFloatParam", "(f)")]
		[InlineData("DefaultIntParam", "(i)")]
		[InlineData("DefaultUIntParam", "(ui)")]
		[InlineData("DefaultLongParam", "(l)")]
		[InlineData("DefaultULongParam", "(ul)")]
		[InlineData("DefaultObjectParam", "(obj)")]
		[InlineData("DefaultShortParam", "(s)")]
		[InlineData("DefaultUShortParam", "(us)")]
		[InlineData("DefaultStringParam", "(str)")]
		[InlineData("DefaultClassParam", "(test)")]
		[InlineData("DefaultIntInt", "(a, b)")]
		[InlineData("DefaultIntStr", "(i, s)")]
		[InlineData("OutString", "(out str)")]
		[InlineData("RefInt", "(ref i)")]
		[InlineData("NullableInt", "(ni)")]
		[InlineData("Generic", "(t)")]
		public void BuildCalledParamterList_ReturnsExpectedCode(string methodName, string expected)
		{
			var parameters = GetParameters(typeof(TestClass), methodName);

			string actual = Parameters.Singleton.BuildCalledParamterList(new StringBuilder(), parameters).ToString();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("NoParams", "(_tracer)")]
		[InlineData("IntParam", "(_tracer, i)")]
		[InlineData("IntStr", "(_tracer, i, s)")]
		[InlineData("OutString", "(_tracer, out str)")]
		[InlineData("RefInt", "(_tracer, ref i)")]
		[InlineData("NullableInt", "(_tracer, ni)")]
		[InlineData("Generic", "(_tracer, t)")]
		public void BuildCalledParamterList_AddTracer_ReturnsExpectedCode(string methodName, string expected)
		{
			var parameters = GetParameters(typeof(TestClass), methodName);

			string actual = Parameters.Singleton.BuildCalledParamterList(new StringBuilder(), parameters, true).ToString();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("NoParams", "(ITracer tracer)")]
		[InlineData("BoolParam", "(ITracer tracer, bool b)")]
		[InlineData("ByteParam", "(ITracer tracer, byte by)")]
		[InlineData("SByteParam", "(ITracer tracer, sbyte sb)")]
		[InlineData("CharParam", "(ITracer tracer, char c)")]
		[InlineData("DecimalParam", "(ITracer tracer, decimal d)")]
		[InlineData("DoubleParam", "(ITracer tracer, double d)")]
		[InlineData("FloatParam", "(ITracer tracer, float f)")]
		[InlineData("IntParam", "(ITracer tracer, int i)")]
		[InlineData("UIntParam", "(ITracer tracer, uint ui)")]
		[InlineData("LongParam", "(ITracer tracer, long l)")]
		[InlineData("ULongParam", "(ITracer tracer, ulong ul)")]
		[InlineData("ObjectParam", "(ITracer tracer, object obj)")]
		[InlineData("ShortParam", "(ITracer tracer, short s)")]
		[InlineData("UShortParam", "(ITracer tracer, ushort us)")]
		[InlineData("StringParam", "(ITracer tracer, string str)")]
		[InlineData("ClassParam", "(ITracer tracer, TestClass test)")]
		[InlineData("IntInt", "(ITracer tracer, int a, int b)")]
		[InlineData("IntStr", "(ITracer tracer, int i, string s)")]
		[InlineData("DefaultBoolParam", "(ITracer tracer, bool b = false)")]
		[InlineData("DefaultByteParam", "(ITracer tracer, byte by = 6)")]
		[InlineData("DefaultSByteParam", "(ITracer tracer, sbyte sb = -3)")]
		[InlineData("DefaultCharParam", "(ITracer tracer, char c = 'c')")]
		[InlineData("DefaultDecimalParam", "(ITracer tracer, decimal d = 12.3m)")]
		[InlineData("DefaultDoubleParam", "(ITracer tracer, double d = 4.2)")]
		[InlineData("DefaultFloatParam", "(ITracer tracer, float f = 2.1f)")]
		[InlineData("DefaultIntParam", "(ITracer tracer, int i = -5)")]
		[InlineData("DefaultUIntParam", "(ITracer tracer, uint ui = 5)")]
		[InlineData("DefaultLongParam", "(ITracer tracer, long l = -3)")]
		[InlineData("DefaultULongParam", "(ITracer tracer, ulong ul = 6)")]
		[InlineData("DefaultObjectParam", "(ITracer tracer, object obj = null)")]
		[InlineData("DefaultShortParam", "(ITracer tracer, short s = -3)")]
		[InlineData("DefaultUShortParam", "(ITracer tracer, ushort us = 7)")]
		[InlineData("DefaultStringParam", "(ITracer tracer, string str = \"hello\")")]
		[InlineData("DefaultClassParam", "(ITracer tracer, TestClass test = null)")]
		[InlineData("DefaultIntInt", "(ITracer tracer, int a = 5, int b = 6)")]
		[InlineData("DefaultIntStr", "(ITracer tracer, int i = 2, string s = \"world\")")]
		[InlineData("OutString", "(ITracer tracer, out string str)")]
		[InlineData("RefInt", "(ITracer tracer, ref int i)")]
		[InlineData("NullableInt", "(ITracer tracer, int? ni)")]
		[InlineData("Generic", "(ITracer tracer, T t)")]
		public void BuildTraceParameterList_ReturnsExpectedCode(string methodName, string expected)
		{
			var formatter = new Parameters();
			var parameters = GetParameters(typeof(TestClass), methodName);

			string actual = formatter.BuildTraceParameterList(new StringBuilder(), parameters).ToString();

			Assert.Equal(expected, actual);
		}
	}
}
