﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Text;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class ConstructorsFactoryTests
	{
		private class DefaultConstructor { }

		[Fact]
		public void Build_DefaultConstuctor_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(DefaultConstructor), Visibility.PublicFlags);

			string expected = @"
		public virtual DefaultConstructor Construct()
		{
			return new DefaultConstructor();
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false).ToString();

			Assert.Equal(expected, actual);
		}

		private class ProtectedConstructor { protected ProtectedConstructor() { } }

		[Fact]
		public void Build_ProtectedConstructor_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(ProtectedConstructor), Visibility.PublicFlags);

			string expected = "At least one public constructor is required to create a factory for ProtectedConstructor";
			var actual = Assert.Throws<Exception>(() => ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false));

			Assert.Equal(expected, actual.Message);
		}

		private class SingleParam { public SingleParam(int param) { } }

		[Fact]
		public void Build_SingleParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(SingleParam), Visibility.PublicFlags);

			string expected = @"
		public virtual SingleParam Construct(int param)
		{
			return new SingleParam(param);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false).ToString();

			Assert.Equal(expected, actual);
		}

		private class DoubleParam { public DoubleParam(string a, long b) { } }

		[Fact]
		public void Build_DoubleParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(DoubleParam), Visibility.PublicFlags);

			string expected = @"
		public virtual DoubleParam Construct(string a, long b)
		{
			return new DoubleParam(a, b);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false).ToString();

			Assert.Equal(expected, actual);
		}

		private class DoubleParamWithDefault { public DoubleParamWithDefault(int c, bool d = true) { } }

		[Fact]
		public void Build_DoubleParamWithDefault_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(DoubleParamWithDefault), Visibility.PublicFlags);

			string expected = @"
		public virtual DoubleParamWithDefault Construct(int c, bool d = true)
		{
			return new DoubleParamWithDefault(c, d);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false).ToString();

			Assert.Equal(expected, actual);
		}

		private class ConstructorWithOutParam { public ConstructorWithOutParam(out int i) { i = 0; } }

		[Fact]
		public void Build_ConstructorWithOutParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(ConstructorWithOutParam), Visibility.PublicFlags);

			string expected = @"
		public virtual ConstructorWithOutParam Construct(out int i)
		{
			return new ConstructorWithOutParam(out i);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false).ToString();

			Assert.Equal(expected, actual);
		}

		private class ConstructorWithRefParam { public ConstructorWithRefParam(ref int i) { } }

		[Fact]
		public void Build_ConstructorWithRefParam_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(ConstructorWithRefParam), Visibility.PublicFlags);

			string expected = @"
		public virtual ConstructorWithRefParam Construct(ref int i)
		{
			return new ConstructorWithRefParam(ref i);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false).ToString();

			Assert.Equal(expected, actual);
		}

		private class Generic<T>
		{
			public Generic() { }
			public Generic(T arg) { }
		}

		[Fact]
		public void Build_GenericClassNoType_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(Generic<>), Visibility.PublicFlags);

			string expected = @"
		public virtual Generic<T> Construct()
		{
			return new Generic<T>();
		}

		public virtual Generic<T> Construct(T arg)
		{
			return new Generic<T>(arg);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, false).ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void BuildFactory_GenericClassNoType_GenericOnConstrutor_ReturnsExpectedCode()
		{
			var classInfo = new ClassInfo(typeof(Generic<>), Visibility.PublicFlags);

			string expected = @"
		public virtual Generic<T> Construct<T>()
		{
			return new Generic<T>();
		}

		public virtual Generic<T> Construct<T>(T arg)
		{
			return new Generic<T>(arg);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, true).ToString();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void Build_GenericClassTypeInt_ReturnsExpectedCode(bool includeGenericParams)
		{
			var classInfo = new ClassInfo(typeof(Generic<int>), Visibility.PublicFlags);

			string expected = @"
		public virtual Generic<int> Construct()
		{
			return new Generic<int>();
		}

		public virtual Generic<int> Construct(int arg)
		{
			return new Generic<int>(arg);
		}";
			string actual = ConstructorsFactory.Singleton.Build(new StringBuilder(), classInfo, includeGenericParams).ToString();

			Assert.Equal(expected, actual);
		}

	}
}
