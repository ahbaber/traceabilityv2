﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests.SupportTests
{
	public class TypeNamesTests
	{
		private struct TestStruct
		{ }

		[Theory]
		[InlineData(typeof(void), "void")]
		[InlineData(typeof(bool), "bool")]
		[InlineData(typeof(byte), "byte")]
		[InlineData(typeof(sbyte), "sbyte")]
		[InlineData(typeof(char), "char")]
		[InlineData(typeof(decimal), "decimal")]
		[InlineData(typeof(double), "double")]
		[InlineData(typeof(float), "float")]
		[InlineData(typeof(int), "int")]
		[InlineData(typeof(uint), "uint")]
		[InlineData(typeof(long), "long")]
		[InlineData(typeof(ulong), "ulong")]
		[InlineData(typeof(object), "object")]
		[InlineData(typeof(short), "short")]
		[InlineData(typeof(ushort), "ushort")]
		[InlineData(typeof(string), "string")]
		public void GetTypeName_BuiltInAndValueTypes_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(new System.Text.StringBuilder(), type).ToString();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(bool?), "bool?")]
		[InlineData(typeof(byte?), "byte?")]
		[InlineData(typeof(sbyte?), "sbyte?")]
		[InlineData(typeof(char?), "char?")]
		[InlineData(typeof(decimal?), "decimal?")]
		[InlineData(typeof(double?), "double?")]
		[InlineData(typeof(float?), "float?")]
		[InlineData(typeof(int?), "int?")]
		[InlineData(typeof(uint?), "uint?")]
		[InlineData(typeof(long?), "long?")]
		[InlineData(typeof(ulong?), "ulong?")]
		[InlineData(typeof(short?), "short?")]
		[InlineData(typeof(ushort?), "ushort?")]
		[InlineData(typeof(TestStruct?), "TestStruct?")]
		public void GetTypeName_BuiltInNullableType_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(new System.Text.StringBuilder(), type).ToString();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(System.Boolean), "bool")]
		[InlineData(typeof(System.Byte), "byte")]
		[InlineData(typeof(System.SByte), "sbyte")]
		[InlineData(typeof(System.Char), "char")]
		[InlineData(typeof(System.Decimal), "decimal")]
		[InlineData(typeof(System.Double), "double")]
		[InlineData(typeof(System.Single), "float")]
		[InlineData(typeof(System.Int32), "int")]
		[InlineData(typeof(System.UInt32), "uint")]
		[InlineData(typeof(System.Int64), "long")]
		[InlineData(typeof(System.UInt64), "ulong")]
		[InlineData(typeof(System.Object), "object")]
		[InlineData(typeof(System.Int16), "short")]
		[InlineData(typeof(System.UInt16), "ushort")]
		[InlineData(typeof(System.String), "string")]
		public void GetTypeName_FullSystemName_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(System.Nullable<bool>), "bool?")]
		[InlineData(typeof(System.Nullable<byte>), "byte?")]
		[InlineData(typeof(System.Nullable<sbyte>), "sbyte?")]
		[InlineData(typeof(System.Nullable<char>), "char?")]
		[InlineData(typeof(System.Nullable<decimal>), "decimal?")]
		[InlineData(typeof(System.Nullable<double>), "double?")]
		[InlineData(typeof(System.Nullable<float>), "float?")]
		[InlineData(typeof(System.Nullable<int>), "int?")]
		[InlineData(typeof(System.Nullable<uint>), "uint?")]
		[InlineData(typeof(System.Nullable<long>), "long?")]
		[InlineData(typeof(System.Nullable<ulong>), "ulong?")]
		[InlineData(typeof(System.Nullable<short>), "short?")]
		[InlineData(typeof(System.Nullable<ushort>), "ushort?")]
		public void GetTypeName_ExplicitNullableType_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(System.Collections.Generic.Comparer<>), "Comparer<T>")]
		[InlineData(typeof(System.Collections.Generic.Dictionary<,>), "Dictionary<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Generic.EqualityComparer<>), "EqualityComparer<T>")]
		[InlineData(typeof(System.Collections.Generic.ICollection<>), "ICollection<T>")]
		[InlineData(typeof(System.Collections.Generic.IComparer<>), "IComparer<T>")]
		[InlineData(typeof(System.Collections.Generic.IDictionary<,>), "IDictionary<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Generic.IEnumerable<>), "IEnumerable<T>")]
		[InlineData(typeof(System.Collections.Generic.IEqualityComparer<>), "IEqualityComparer<T>")]
		[InlineData(typeof(System.Collections.Generic.IList<>), "IList<T>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyCollection<>), "IReadOnlyCollection<T>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyDictionary<,>), "IReadOnlyDictionary<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyList<>), "IReadOnlyList<T>")]
		[InlineData(typeof(System.Collections.Generic.KeyValuePair<,>), "KeyValuePair<TKey, TValue>")]
		public void GetTypeName_GenericsUntyped_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(System.Collections.Generic.Comparer<>), "System.Collections.Generic.Comparer<T>")]
		[InlineData(typeof(System.Collections.Generic.Dictionary<,>), "System.Collections.Generic.Dictionary<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Generic.EqualityComparer<>), "System.Collections.Generic.EqualityComparer<T>")]
		[InlineData(typeof(System.Collections.Generic.ICollection<>), "System.Collections.Generic.ICollection<T>")]
		[InlineData(typeof(System.Collections.Generic.IComparer<>), "System.Collections.Generic.IComparer<T>")]
		[InlineData(typeof(System.Collections.Generic.IDictionary<,>), "System.Collections.Generic.IDictionary<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Generic.IEnumerable<>), "System.Collections.Generic.IEnumerable<T>")]
		[InlineData(typeof(System.Collections.Generic.IEqualityComparer<>), "System.Collections.Generic.IEqualityComparer<T>")]
		[InlineData(typeof(System.Collections.Generic.IList<>), "System.Collections.Generic.IList<T>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyCollection<>), "System.Collections.Generic.IReadOnlyCollection<T>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyDictionary<,>), "System.Collections.Generic.IReadOnlyDictionary<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyList<>), "System.Collections.Generic.IReadOnlyList<T>")]
		[InlineData(typeof(System.Collections.Generic.KeyValuePair<,>), "System.Collections.Generic.KeyValuePair<TKey, TValue>")]
		public void GetTypeName_GenericsUntyped_ReturnsExpectedFullTypeType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type, true);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(System.Collections.Generic.Comparer<int>), "Comparer<int>")]
		[InlineData(typeof(System.Collections.Generic.Dictionary<string, bool>), "Dictionary<string, bool>")]
		[InlineData(typeof(System.Collections.Generic.EqualityComparer<short>), "EqualityComparer<short>")]
		[InlineData(typeof(System.Collections.Generic.ICollection<char>), "ICollection<char>")]
		[InlineData(typeof(System.Collections.Generic.IComparer<int>), "IComparer<int>")]
		[InlineData(typeof(System.Collections.Generic.IDictionary<string, int>), "IDictionary<string, int>")]
		[InlineData(typeof(System.Collections.Generic.IEnumerable<bool>), "IEnumerable<bool>")]
		[InlineData(typeof(System.Collections.Generic.IEqualityComparer<float>), "IEqualityComparer<float>")]
		[InlineData(typeof(System.Collections.Generic.IList<byte>), "IList<byte>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyCollection<short>), "IReadOnlyCollection<short>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyDictionary<int, bool>), "IReadOnlyDictionary<int, bool>")]
		[InlineData(typeof(System.Collections.Generic.IReadOnlyList<string>), "IReadOnlyList<string>")]
		[InlineData(typeof(System.Collections.Generic.KeyValuePair<string, object>), "KeyValuePair<string, object>")]
		public void GetTypeName_Generics_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(System.Collections.Generic.List<System.Collections.Generic.List<int>>), "List<List<int>>")]
		[InlineData(typeof(System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<int>>), "Dictionary<string, List<int>>")]
		public void GetTypeName_GenericNested_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(System.Collections.Concurrent.BlockingCollection<>), "BlockingCollection<T>")]
		[InlineData(typeof(System.Collections.Concurrent.ConcurrentBag<>), "ConcurrentBag<T>")]
		[InlineData(typeof(System.Collections.Concurrent.ConcurrentDictionary<,>), "ConcurrentDictionary<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Concurrent.ConcurrentQueue<>), "ConcurrentQueue<T>")]
		[InlineData(typeof(System.Collections.Concurrent.ConcurrentStack<>), "ConcurrentStack<T>")]
		[InlineData(typeof(System.Collections.Concurrent.IProducerConsumerCollection<>), "IProducerConsumerCollection<T>")]
		[InlineData(typeof(System.Collections.Concurrent.OrderablePartitioner<>), "OrderablePartitioner<TSource>")]
		[InlineData(typeof(System.Collections.Concurrent.Partitioner<>), "Partitioner<TSource>")]
		public void GetTypeName_Concurrent_ReturnsExpectedType(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type);

			Assert.Equal(expected, actual);
		}

		private class CustomClass { }

		[Theory]
		[InlineData(typeof(CustomClass), false, "CustomClass")]
		[InlineData(typeof(CustomClass), true, "TraceCodeGeneratorTests.SupportTests.TypeNamesTests.CustomClass")]
		public void GetTypeName_CustomClass_ReturnsExpectedType(System.Type type, bool useFullName, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type, useFullName);

			Assert.Equal(expected, actual);
		}

		private class CustomGeneric1<T>
		{
			public T Value => _t;
			private readonly T _t = default(T);
		}
		private class CustomGeneric2<T1, T2>
		{
			public T1 V1 => _t1;
			public T2 V2 => _t2;
			private readonly T1 _t1 = default(T1);
			private readonly T2 _t2 = default(T2);
		}
		private class CustomGeneric3<T1, T2, T3>
		{
			public T1 V1 => _t1;
			public T2 V2 => _t2;
			public T3 V3 => _t3;
			private readonly T1 _t1 = default(T1);
			private readonly T2 _t2 = default(T2);
			private readonly T3 _t3 = default(T3);
		}

		[Theory]
		[InlineData(typeof(CustomGeneric1<>), false, "CustomGeneric1<T>")]
		[InlineData(typeof(CustomGeneric1<>), true, "TraceCodeGeneratorTests.SupportTests.TypeNamesTests.CustomGeneric1<T>")]
		[InlineData(typeof(CustomGeneric1<int>), false, "CustomGeneric1<int>")]
		[InlineData(typeof(CustomGeneric1<int>), true, "TraceCodeGeneratorTests.SupportTests.TypeNamesTests.CustomGeneric1<int>")]
		[InlineData(typeof(CustomGeneric2<,>), false, "CustomGeneric2<T1, T2>")]
		[InlineData(typeof(CustomGeneric2<string, int>), false, "CustomGeneric2<string, int>")]
		[InlineData(typeof(CustomGeneric3<,,>), false, "CustomGeneric3<T1, T2, T3>")]
		[InlineData(typeof(CustomGeneric3<string, int, bool>), false, "CustomGeneric3<string, int, bool>")]
		public void GetTypeName_CustomGeneric_ReturnsExpectedType(System.Type type, bool useFullName, string expected)
		{
			string actual = TypeNames.Singleton.GetTypeName(type, useFullName);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void AddTranslation_ReturnsTranslation()
		{
			var typeNameGenerator = new TypeNames()
				.AddTranslation("CustomClass", "AnotherTypeName");

			string expected = "AnotherTypeName";
			string actual = typeNameGenerator.GetTypeName(typeof(CustomClass));

			Assert.Equal(expected, actual);
		}

		private System.Text.StringBuilder CustomHandler(System.Text.StringBuilder builder, System.Type type, bool useFullName)
		{
			return builder.Append("CustomHandlerCalled");
		}

		[Fact]
		public void AddCustomHandler_ReturnsCustomTypeInfo()
		{
			var listType = typeof(System.Collections.Generic.List<int>);
			var typeNameGenerator = new TypeNames()
				.AddCustomHandler(listType, CustomHandler);

			string expected = "CustomHandlerCalled";
			string actual = typeNameGenerator.GetTypeName(listType);

			Assert.Equal(expected, actual);
		}

		private class CustomInt : CustomGeneric1<int> { }

		[Theory]
		[InlineData(typeof(int), "int")]
		[InlineData(typeof(CustomClass), "CustomClass")]
		[InlineData(typeof(CustomGeneric1<>), "CustomGeneric1<T>")]
		[InlineData(typeof(CustomGeneric1<int>), "CustomGeneric1_int")]
		[InlineData(typeof(CustomGeneric2<,>), "CustomGeneric2<T1, T2>")]
		[InlineData(typeof(CustomGeneric2<int, bool>), "CustomGeneric2_int_bool")]
		[InlineData(typeof(CustomInt), "CustomInt")]
		[InlineData(typeof(CustomGeneric1<CustomGeneric2<int, bool>>), "CustomGeneric1_CustomGeneric2_int_bool")]
		[InlineData(typeof(System.Collections.Generic.List<>), "List<T>")]
		[InlineData(typeof(System.Collections.Generic.List<int>), "List_int")]
		public void GetDeclarationName_ReturnsExpectedName(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetDeclarationName(type);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(int), "intFactory")]
		[InlineData(typeof(CustomClass), "CustomClassFactory")]
		[InlineData(typeof(CustomGeneric1<>), "CustomGeneric1Factory<T>")]
		[InlineData(typeof(CustomGeneric1<int>), "CustomGeneric1Factory_int")]
		[InlineData(typeof(CustomGeneric2<,>), "CustomGeneric2Factory<T1, T2>")]
		[InlineData(typeof(CustomGeneric2<int, bool>), "CustomGeneric2Factory_int_bool")]
		[InlineData(typeof(CustomInt), "CustomIntFactory")]
		[InlineData(typeof(CustomGeneric1<CustomGeneric2<int, bool>>), "CustomGeneric1Factory_CustomGeneric2_int_bool")]
		[InlineData(typeof(System.Collections.Generic.List<>), "ListFactory<T>")]
		[InlineData(typeof(System.Collections.Generic.List<int>), "ListFactory_int")]
		public void GetDeclarationName_WithSuffix_ReturnsExpectedName(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetDeclarationName(type, "Factory");

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void GetDeclarationName_WithSuffixAndTranslation_ReturnsExpectedName()
		{
			var typeNames = new TypeNames();
			var type = typeof(int);

			Assert.Equal("intFactory", typeNames.GetDeclarationName(type, "Factory"));

			typeNames.AddTranslation("intFactory", "IntFactory");

			Assert.Equal("IntFactory", typeNames.GetDeclarationName(type, "Factory"));
		}

		[Theory]
		[InlineData(typeof(int), "int")]
		[InlineData(typeof(CustomClass), "CustomClass")]
		[InlineData(typeof(CustomGeneric1<>), "CustomGeneric1")]
		[InlineData(typeof(CustomGeneric1<int>), "CustomGeneric1_int")]
		[InlineData(typeof(CustomGeneric2<,>), "CustomGeneric2")]
		[InlineData(typeof(CustomGeneric2<int, bool>), "CustomGeneric2_int_bool")]
		[InlineData(typeof(CustomInt), "CustomInt")]
		[InlineData(typeof(CustomGeneric1<CustomGeneric2<int, bool>>), "CustomGeneric1_CustomGeneric2_int_bool")]
		[InlineData(typeof(System.Collections.Generic.List<>), "List")]
		[InlineData(typeof(System.Collections.Generic.List<int>), "List_int")]
		public void GetConstructorName_ReturnsExpectedName(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetConstructorName(type);

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(typeof(int), "intFactory")]
		[InlineData(typeof(CustomClass), "CustomClassFactory")]
		[InlineData(typeof(CustomGeneric1<>), "CustomGeneric1Factory")]
		[InlineData(typeof(CustomGeneric1<int>), "CustomGeneric1Factory_int")]
		[InlineData(typeof(CustomGeneric2<,>), "CustomGeneric2Factory")]
		[InlineData(typeof(CustomGeneric2<int, bool>), "CustomGeneric2Factory_int_bool")]
		[InlineData(typeof(CustomInt), "CustomIntFactory")]
		[InlineData(typeof(CustomGeneric1<CustomGeneric2<int, bool>>), "CustomGeneric1Factory_CustomGeneric2_int_bool")]
		[InlineData(typeof(System.Collections.Generic.List<>), "ListFactory")]
		[InlineData(typeof(System.Collections.Generic.List<int>), "ListFactory_int")]
		public void GetConstructorName_WithSuffix_ReturnsExpectedName(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetConstructorName(type, "Factory");

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void GetConstructorName_WithSuffixAndTranslation_ReturnsExpectedName()
		{
			var typeNames = new TypeNames();
			var type = typeof(CustomGeneric1<>);

			Assert.Equal("CustomGeneric1Factory", typeNames.GetConstructorName(type, "Factory"));

			typeNames.AddTranslation("CustomGeneric1Factory", "CGF");

			Assert.Equal("CGF", typeNames.GetConstructorName(type, "Factory"));
		}

		[Theory]
		[InlineData(typeof(int), "")]
		[InlineData(typeof(System.Collections.Generic.List<>), "<T>")]
		[InlineData(typeof(System.Collections.Generic.List<int>), "<int>")]
		[InlineData(typeof(System.Collections.Generic.Dictionary<,>), "<TKey, TValue>")]
		[InlineData(typeof(System.Collections.Generic.Dictionary<string, int>), "<string, int>")]
		public void GetGenericParameters_ReturnsEpxectedCode(System.Type type, string expected)
		{
			string actual = TypeNames.Singleton.GetGenericParameters(new System.Text.StringBuilder(), type).ToString();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("Library.ClassName", "Library.ClassName,Library")]
		[InlineData("Library.ClassName,Library", "Library.ClassName,Library")]
		[InlineData("Library.Sub.ClassName", "Library.Sub.ClassName,Library")]
		[InlineData(null, "")]
		[InlineData("", "")]
		[InlineData("  ", "  ")]
		[InlineData("ClassNameOnly", "ClassNameOnly")]
		public void GetQualifiedName_ReturnsExpectedName(string typeName, string expected)
		{
			Assert.Equal(expected, TypeNames.GetQualifiedName(typeName));
		}
	}
}
