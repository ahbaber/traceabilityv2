﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TraceCodeGenerator;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests
{
	public class VerifyMethodTests
	{
		private class Allowed
		{
			public virtual string PublicMethod() { return null; }
			protected virtual int ProtectedMethod() { return 0; }
			internal virtual char InternalMethod() { return ' '; }
			protected internal virtual short ProtIntMethod() { return 0; }
		}

		private class TraceAllowed : Allowed
		{
			public override string PublicMethod() { return base.PublicMethod(); }
			protected override int ProtectedMethod() { return base.ProtectedMethod(); }
			internal override char InternalMethod() { return base.InternalMethod(); }
			protected internal override short ProtIntMethod() { return base.ProtIntMethod(); }
		}

		[Fact]
		public void Verify_Allowed_NoErrors()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceAllowed));

			Assert.Equal("Allowed", actual.BaseClassName);
			Assert.Equal("TraceAllowed", actual.TraceClassName);
			Assert.Empty(actual.Errors);
		}

		private class StaticMethodClass
		{
			public static int StaticMethod() { return 0; }
			public virtual int OtherMethod() { return 0; }
		}

		private class TraceStaticMethodClass : StaticMethodClass
		{
			public override int OtherMethod() { return base.OtherMethod(); }
		}

		[Fact]
		public void Verify_Static_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(StaticMethodClass), typeof(TraceStaticMethodClass));

			Assert.Equal("StaticMethodClass", actual.BaseClassName);
			Assert.Equal("TraceStaticMethodClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("StaticMethod_System.Int32", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.IsStatic, actual.Errors[0].Reason);
		}

		private class StaticSkipMap
		{
			public static int StaticMethod() { return 0; }
		}

		[Fact]
		public void Verify_StaticSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(StaticSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(StaticMethodClass), typeof(TraceStaticMethodClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class NonVirtualMethodClass
		{
			public virtual string VirtualMethod() { return null; }
			public string NonVirtualMethod() { return null; }
		}

		private class TraceNonVirtualMethodClass : NonVirtualMethodClass
		{
			public override string VirtualMethod() { return base.VirtualMethod(); }
		}

		[Fact]
		public void Verify_NotVirtual_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(NonVirtualMethodClass), typeof(TraceNonVirtualMethodClass));

			Assert.Equal("NonVirtualMethodClass", actual.BaseClassName);
			Assert.Equal("TraceNonVirtualMethodClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("NonVirtualMethod_System.String", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.NotVirtual, actual.Errors[0].Reason);
		}

		private class NonVirtualSkipMap
		{
			public string NonVirtualMethod() { return null; }
		}

		[Fact]
		public void Verify_NonVirtualSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(NonVirtualSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(NonVirtualMethodClass), typeof(TraceNonVirtualMethodClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class PreSealedMethodClass
		{
			public virtual int SealedMethod() { return 0; }
		}

		private class SealedMethodClass : PreSealedMethodClass
		{
			public sealed override int SealedMethod() { return base.SealedMethod(); }
			public virtual long OtherMethod() { return 0; }
		}

		private class TraceSealedMethodClass : SealedMethodClass
		{
			public override long OtherMethod() { return base.OtherMethod(); }
		}

		[Fact]
		public void Verify_SealedProperty_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(SealedMethodClass), typeof(TraceSealedMethodClass));

			Assert.Equal("SealedMethodClass", actual.BaseClassName);
			Assert.Equal("TraceSealedMethodClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("SealedMethod_System.Int32", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.IsFinal, actual.Errors[0].Reason);
		}

		private class SealedSkipMap
		{
			public int SealedMethod() { return 0; }
		}

		[Fact]
		public void Verify_SealedSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(SealedSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(SealedMethodClass), typeof(TraceSealedMethodClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class PrivateMethodClass
		{
			public virtual string PublicMethod() { return null; }
			private string PrivateMethod() { return null; }
		}

		private class TracePrivateMethodClass : PrivateMethodClass
		{
			public override string PublicMethod() { return base.PublicMethod(); }
		}

		[Fact]
		public void Verify_PrivateProperty_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(PrivateMethodClass), typeof(TracePrivateMethodClass));

			Assert.Equal("PrivateMethodClass", actual.BaseClassName);
			Assert.Equal("TracePrivateMethodClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("PrivateMethod_System.String", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.IsPrivate, actual.Errors[0].Reason);
		}

		private class PrivateSkipMap
		{
			private string PrivateMethod() { return null; }
		}

		[Fact]
		public void Verify_PrivateSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(PrivateSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(PrivateMethodClass), typeof(TracePrivateMethodClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class TraceMissed : Allowed
		{
			protected override int ProtectedMethod() { return base.ProtectedMethod(); }
			internal override char InternalMethod() { return base.InternalMethod(); }
			protected internal override short ProtIntMethod() { return base.ProtIntMethod(); }
		}

		[Fact]
		public void Verify_NotImplemented_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceMissed));

			Assert.Equal("Allowed", actual.BaseClassName);
			Assert.Equal("TraceMissed", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("PublicMethod_System.String", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.NotImplemented, actual.Errors[0].Reason);
		}

		private class MissedSkipMap
		{
			public string PublicMethod() { return null; }
		}

		[Fact]
		public void Verify_MissedSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(MissedSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceMissed), skipMap);

			Assert.Empty(actual.Errors);
		}

			private class TraceExtra : Allowed
		{
			public override string PublicMethod() { return base.PublicMethod(); }
			protected override int ProtectedMethod() { return base.ProtectedMethod(); }
			internal override char InternalMethod() { return base.InternalMethod(); }
			protected internal override short ProtIntMethod() { return base.ProtIntMethod(); }
			public byte ExtraMethod() { return 0; }
		}

		[Fact]
		public void Verify_Extra_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceExtra));

			Assert.Equal("Allowed", actual.BaseClassName);
			Assert.Equal("TraceExtra", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("ExtraMethod_System.Byte", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.Unspecified, actual.Errors[0].Reason);
		}

		private class ExtraSkipMap
		{
			public byte ExtraMethod() { return 0; }
		}

		[Fact]
		public void Verify_ExtraSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(ExtraSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceExtra), skipMap);

			Assert.Empty(actual.Errors);
		}
	}
}
