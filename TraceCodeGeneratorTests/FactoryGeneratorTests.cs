﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TraceCodeGenerator;
using TestReferenceFunctional.BaseClasses;
using Xunit;
using TestStrings = TestReferenceFunctional.Factories.TestStrings;

namespace TraceCodeGeneratorTests
{
	public class FactoryGeneratorTests
	{
		[Theory]
		[InlineData(typeof(EmptyClass), TestStrings.EmptyClassFactoryExpected)]
		[InlineData(typeof(GenericType<>), TestStrings.GenericTypeFactoryExpected)]
		[InlineData(typeof(GenericType<int>), TestStrings.GenericTypeFactory_intExpected)]
		[InlineData(typeof(GenericInt), TestStrings.GenericIntFactoryExpected)]
		[InlineData(typeof(ClassWithNonTraceableConstructor), TestStrings.ClassWithNonTraceableConstructorFactoryExpected)]
		public void Build_Type_EmptyClassFactory_ReturnsExpectedCode(Type type, string expected)
		{
			var codeGen = new FactoryGenerator(type, "TestReferenceFunctional.Factories");

			string actual = codeGen.Build();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("TestReferenceFunctional.BaseClasses.EmptyClass", TestStrings.EmptyClassFactoryExpected)]
		[InlineData("TestReferenceFunctional.BaseClasses.GenericInt", TestStrings.GenericIntFactoryExpected)]
		[InlineData("TestReferenceFunctional.BaseClasses.ClassWithNonTraceableConstructor", TestStrings.ClassWithNonTraceableConstructorFactoryExpected)]
		public void Build_ByTypeName_ReturnsExpectedCode(string typeName, string expected)
		{
			var codeGen = new FactoryGenerator(typeName, "TestReferenceFunctional.Factories");

			string actual = codeGen.Build();

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("GenericType<>")]
		[InlineData("GenericType<int>")]
		public void Build_TypeName_GenericTypes_ReturnsExpectedError(string name)
		{
			var codeGen = new FactoryGenerator("TestReferenceFunctional.BaseClasses." + name, "TestReferenceFunctional.Factories");

			string expected = $"Could not locate valid type with: 'TestReferenceFunctional.BaseClasses.{name},TestReferenceFunctional'.";

			Assert.True(codeGen.HasError);
			Assert.Equal(expected, codeGen.Error);
			Assert.Equal("", codeGen.Build());
		}

		[Fact]
		public void Build_GenericNamed_Type_PlaceGenericParametersOnConstruct_ReturnsExpectedCode()
		{
			var codeGen = new FactoryGenerator(typeof(GenericType<>), "TestReferenceFunctional.Factories", "GenericWithParamsOnConstructFactory")
				.PlaceGenericParametersOnConstruct();

			string expected = TestStrings.GenericWithParamsOnConstructFactoryExpected;
			string actual = codeGen.Build();

			Assert.Equal(expected, actual);
		}
	}
}
