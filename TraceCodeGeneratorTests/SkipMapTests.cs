﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Reflection;
using TraceCodeGenerator;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests
{
	public class SkipMapTests
	{

		private class TestClass
		{
			public virtual int IntProp { get; set; }
			public virtual char CharProp { get; set; }
			public virtual char Method1() { return ' '; }
			public virtual void Method1(char c) { }
		}

		private PropertyInfo GetPropertyInfo(Type baseType, string propertyName)
		{
			return baseType.GetProperty(propertyName, Visibility.Flags);
		}

		private MethodInfo GetMethodInfo(Type baseType, string methodName)
		{
			return baseType.GetMethod(methodName, Visibility.Flags);
		}

		[Theory]
		[InlineData("IntProp", true, "IntProp", true)]
		[InlineData("IntProp", false, "IntProp", true)]
		[InlineData("IntProp", true, "CharProp", false)]
		[InlineData("IntProp", false, "CharProp", false)]
		public void ShouldSkip_PropertyName_ReturnsExpectedValue(string skip, bool isParameterized, string propertyName, bool expected)
		{
			var skipMap = new SkipMap()
				.Add(skip, isParameterized);

			bool actual = skipMap.ShouldSkip(GetPropertyInfo(typeof(TestClass), propertyName));

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void ShouldSkip_AllMethodsWithOneName_ReturnsExpectedValue()
		{
			var skipMap = new SkipMap().
				Add("Method1", false);

			var skippedMethodNames = new List<string>
			{
				"Method1",		//public virtual char Method1()
				"Method1",		//public virtual void Method1(char c)
				"Equals",		//public override bool Equals(object obj)
				"GetHashCode",	//public override int GetHashCode()
				"ToString",		//public override string ToString()
				"GetType",		//public Type GetType()
			};
			int actual = 0;

			foreach (var methodInfo in typeof(TestClass).GetMethods())
			{
				if (skipMap.ShouldSkip(methodInfo))
				{
					if (skippedMethodNames.Remove(methodInfo.Name))
					{ ++actual; }
				}
			}

			Assert.Equal(6, actual);
		}

		[Fact]
		public void ShouldSkip_OneMethodSkippedOrNotSkipped_ReturnsExpectedValue()
		{
			var skipMap = new SkipMap().
				Add("Method1_System.Char", true);

			var skippedMethodNames = new List<string>
			{
				"Method1",		//public virtual char Method1()
				"Equals",		//public override bool Equals(object obj)
				"GetHashCode",	//public override int GetHashCode()
				"ToString",		//public override string ToString()
				"GetType",		//public Type GetType()
			};
			int actual = 0;

			foreach (var methodInfo in typeof(TestClass).GetMethods())
			{
				if (skipMap.ShouldSkip(methodInfo))
				{
					if (skippedMethodNames.Remove(methodInfo.Name))
					{ ++actual; }
				}
			}

			Assert.Equal(5, actual);
		}

		private class SkipMethod
		{
			public char Method1() { return ' '; }
			//public override bool Equals(object obj)
			//public override int GetHashCode()
			//public override string ToString()
			//public Type GetType()
		}

		[Fact]
		public void ShouldSkip_MethodDefinedBySkipClass_ReturnsExpectedValue()
		{
			var skipMap = new SkipMap().Add(typeof(SkipMethod));

			var skippedMethodNames = new List<string>
			{
				"Method1",		//public virtual char Method1()
				"Equals",		//public override bool Equals(object obj)
				"GetHashCode",	//public override int GetHashCode()
				"ToString",		//public override string ToString()
				"GetType",		//public Type GetType()
			};
			int actual = 0;

			foreach (var methodInfo in typeof(TestClass).GetMethods())
			{
				if (skipMap.ShouldSkip(methodInfo))
				{
					if (skippedMethodNames.Remove(methodInfo.Name))
					{ ++actual; }
				}
			}

			Assert.Equal(5, actual);
		}

		private class DisposeProperty
		{
			public virtual int Dispose { get; set; }
		}

		[Fact]
		public void ShouldSkip_DisposeProperty_ReturnsTrue()
		{
			var skipMap = new SkipMap();

			bool actual = skipMap.ShouldSkip(GetPropertyInfo(typeof(DisposeProperty), "Dispose"));

			Assert.True(actual);
		}

		private class DisposeMethod
		{
			public virtual void Dispose() { }
		}

		[Fact]
		public void ShouldSkip_DisposeMethod_ReturnsExpectedValue()
		{
			var skipMap = new SkipMap();

			bool actual = skipMap.ShouldSkip(GetMethodInfo(typeof(DisposeMethod), "Dispose"));

			Assert.True(actual);
		}
	}
}
