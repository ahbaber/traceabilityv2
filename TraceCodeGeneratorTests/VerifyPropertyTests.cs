﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TraceCodeGenerator;
using TraceCodeGenerator.Support;
using Xunit;

namespace TraceCodeGeneratorTests
{
	public class VerifyPropertyTests
	{
		private class Allowed
		{
			public virtual string PublicProperty { get; set; }
			protected virtual int ProtectedProperty { get; set; }
			internal virtual char InternalProperty { get; set; }
			protected internal virtual short ProtIntProperty { get; set; }
		}

		private class TraceAllowed : Allowed
		{
			public override string PublicProperty { get => base.PublicProperty; set => base.PublicProperty = value; }
			protected override int ProtectedProperty { get => base.ProtectedProperty; set => base.ProtectedProperty = value; }
			internal override char InternalProperty { get => base.InternalProperty; set => base.InternalProperty = value; }
			protected internal override short ProtIntProperty { get => base.ProtIntProperty; set => base.ProtIntProperty = value; }
		}

		[Fact]
		public void Verify_Allowed_NoErrors()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceAllowed));

			Assert.Equal("Allowed", actual.BaseClassName);
			Assert.Equal("TraceAllowed", actual.TraceClassName);
			Assert.Empty(actual.Errors);
		}

		private class StaticPropertyClass
		{
			public static int StaticProperty { get; set; }
			public virtual int OtherProperty { get; set; }
		}

		private class TraceStaticPropertyClass : StaticPropertyClass
		{
			public override int OtherProperty { get => base.OtherProperty; set => base.OtherProperty = value; }
		}

		[Fact]
		public void Verify_Static_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(StaticPropertyClass), typeof(TraceStaticPropertyClass));

			Assert.Equal("StaticPropertyClass", actual.BaseClassName);
			Assert.Equal("TraceStaticPropertyClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("StaticProperty", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.IsStatic, actual.Errors[0].Reason);
		}

		private class StaticSkipMap
		{
			public static int StaticProperty { get; set; }
		}

		[Fact]
		public void Verify_StaticSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(StaticSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(StaticPropertyClass), typeof(TraceStaticPropertyClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class NonVirtualPropertyClass
		{
			public virtual string VirtualProperty { get; set; }
			public string NonVirtualProperty { get; set; }
		}

		private class TraceNonVirtualPropertyClass : NonVirtualPropertyClass
		{
			public override string VirtualProperty { get => base.VirtualProperty; set => base.VirtualProperty = value; }
		}

		[Fact]
		public void Verify_NotVirtual_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(NonVirtualPropertyClass), typeof(TraceNonVirtualPropertyClass));

			Assert.Equal("NonVirtualPropertyClass", actual.BaseClassName);
			Assert.Equal("TraceNonVirtualPropertyClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("NonVirtualProperty", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.NotVirtual, actual.Errors[0].Reason);
		}

		private class NonVirtualSkipMap
		{
			public string NonVirtualProperty { get; set; }
		}

		[Fact]
		public void Verify_NonVirtualSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(NonVirtualSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(NonVirtualPropertyClass), typeof(TraceNonVirtualPropertyClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class PreSealedPropertyClass
		{
			public virtual int SealedProperty { get; set; }
			public virtual long OtherProperty { get; set; }
		}

		private class SealedPropertyClass : PreSealedPropertyClass
		{
			public sealed override int SealedProperty { get => base.SealedProperty; set => base.SealedProperty = value; }
			public override long OtherProperty { get => base.OtherProperty; set => base.OtherProperty = value; }
		}

		private class TraceSealedPropertyClass : SealedPropertyClass
		{
			public override long OtherProperty { get => base.OtherProperty; set => base.OtherProperty = value; }
		}

		[Fact]
		public void Verify_SealedProperty_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(SealedPropertyClass), typeof(TraceSealedPropertyClass));

			Assert.Equal("SealedPropertyClass", actual.BaseClassName);
			Assert.Equal("TraceSealedPropertyClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("SealedProperty", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.IsFinal, actual.Errors[0].Reason);
		}

		private class SealedSkipMap
		{
			public int SealedProperty { get; set; }
		}

		[Fact]
		public void Verify_SealedSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(SealedSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(SealedPropertyClass), typeof(TraceSealedPropertyClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class PrivatePropertyClass
		{
			public virtual string PublicProperty => "All welcome.";
			private string PrivateProperty => "No Trespassing";
		}

		private class TracePrivatePropertyClass : PrivatePropertyClass
		{
			public override string PublicProperty => base.PublicProperty;
		}

		[Fact]
		public void Verify_PrivateProperty_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(PrivatePropertyClass), typeof(TracePrivatePropertyClass));

			Assert.Equal("PrivatePropertyClass", actual.BaseClassName);
			Assert.Equal("TracePrivatePropertyClass", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("PrivateProperty", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.IsPrivate, actual.Errors[0].Reason);
		}

		private class PrivateSkipMap
		{
			private string PrivateProperty => "Stay out";
		}

		[Fact]
		public void Verify_PrivateSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(PrivateSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(PrivatePropertyClass), typeof(TracePrivatePropertyClass), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class TraceMissed : Allowed
		{
			protected override int ProtectedProperty { get => base.ProtectedProperty; set => base.ProtectedProperty = value; }
			internal override char InternalProperty { get => base.InternalProperty; set => base.InternalProperty = value; }
			protected internal override short ProtIntProperty { get => base.ProtIntProperty; set => base.ProtIntProperty = value; }
		}

		[Fact]
		public void Verify_NotImplemented_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceMissed));

			Assert.Equal("Allowed", actual.BaseClassName);
			Assert.Equal("TraceMissed", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("PublicProperty", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.NotImplemented, actual.Errors[0].Reason);
		}

		private class MissedSkipMap
		{
			public string PublicProperty { get; set; }
		}

		[Fact]
		public void Verify_MissedSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(MissedSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceMissed), skipMap);

			Assert.Empty(actual.Errors);
		}

		private class TraceExtra : Allowed
		{
			public override string PublicProperty { get => base.PublicProperty; set => base.PublicProperty = value; }
			protected override int ProtectedProperty { get => base.ProtectedProperty; set => base.ProtectedProperty = value; }
			internal override char InternalProperty { get => base.InternalProperty; set => base.InternalProperty = value; }
			protected internal override short ProtIntProperty { get => base.ProtIntProperty; set => base.ProtIntProperty = value; }
			public byte ExtraProperty { get; set; }
		}

		[Fact]
		public void Verify_Extra_ReturnsError()
		{
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceExtra));

			Assert.Equal("Allowed", actual.BaseClassName);
			Assert.Equal("TraceExtra", actual.TraceClassName);
			Assert.Single(actual.Errors);
			Assert.Equal("ExtraProperty", actual.Errors[0].MemberName);
			Assert.Equal(IgnoreReason.Unspecified, actual.Errors[0].Reason);
		}

		private class ExtraSkipMap
		{
			public byte ExtraProperty { get; set; }
		}

		[Fact]
		public void Verify_ExtraSkipped_NoErrors()
		{
			var skipMap = new SkipMap().Add(typeof(ExtraSkipMap));
			var actual = Verify.VerifyTraceInheritence(typeof(Allowed), typeof(TraceExtra), skipMap);

			Assert.Empty(actual.Errors);
		}
	}
}
