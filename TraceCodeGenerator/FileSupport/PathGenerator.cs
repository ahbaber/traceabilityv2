﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.IO;

namespace TraceCodeGenerator.FileSupport
{
	/// <summary>Used to generate paths relative to the solution root.  This class
	/// is meant to be used inside of console application projects.  It expects that
	/// the project file is immediately inside the solution root folder, and not
	/// another subfolder.</summary>
	public class PathGenerator : IPathGenerator
	{
		public PathGenerator()
		{
			string dir = Directory.GetCurrentDirectory();
			string projectName = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
			int index = dir.IndexOf(projectName, StringComparison.InvariantCulture);
			_parentDirectory = dir.Remove(index);
		}

		/// <summary>Generates a file path with the solution directory being the
		/// root.  The subfolders are based on the namespace from the <see cref="IClassGenerator"/>,
		/// with each "." in the namespace being treated as folder separator.
		/// No means to go up a directory level is provided.</summary>
		/// <returns>A string containing a file path</returns>
		/// <param name="classGen">A <see cref="IClassGenerator"/> whose namespace field will be used to determine the path.</param>
		/// <param name="subDirectory">Optional override for a different path.
		/// This value must follow the namespace style in using periods as a separator.</param>
		public string GetPath(IClassGenerator classGen, string subDirectory = null)
		{
			string dir = subDirectory ?? classGen.Namespace;
			string[] subpaths = dir.Split('.');

			string path = _parentDirectory;
			foreach (string sub in subpaths)
			{
				path = Path.Combine(path, sub);
			}

			return path;
		}

		/// <summary>Generates the file path where the genrated file will be saved.
		/// The base folder is the parent folder of the project executing this code;
		/// which is expected to be the Solution folder.  The namespace used in
		/// the <see cref="IClassGenerator"/> will be used to specify the
		/// subfolders.  The name of the file will be gathered from <paramref name="classGen"/>,
		/// or overridden by <paramref name="filename"/>.  The file type will be
		/// gathered from <paramref name="classGen"/>, and cannot be overriden.</summary>
		/// <returns>The filepath and filename.</returns>
		/// <param name="classGen">The <see cref="IClassGenerator"/> that will
		/// generate the code.</param>
		/// <param name="filename">Optional override that will change the filename.  The filename will
		/// automatically have the appropriate filetype appended if it is missing.</param>
		/// <param name="subDirectory">Optional override for a different path.
		/// This value must follow the namespace style in using periods as a separator.</param>
		public string GetPathAndFilename(IClassGenerator classGen, string filename = null, string subDirectory = null)
		{
			filename = filename ?? classGen.Filename;
			string path = GetPath(classGen, subDirectory);
			path = Path.Combine(path, filename);

			if (!path.EndsWith(classGen.FileType, StringComparison.InvariantCulture))
			{
				path += classGen.FileType;
			}

			return path;
		}

		private readonly string _parentDirectory;
	}
}
