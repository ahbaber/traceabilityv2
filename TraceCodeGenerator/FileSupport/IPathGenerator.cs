﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace TraceCodeGenerator.FileSupport
{
	/// <summary>Interface for generating file paths.</summary>
	public interface IPathGenerator
	{
		/// <summary>Constructs and returns the path to the folder that will contain
		/// the file.</summary>
		/// <returns>The folder path.</returns>
		/// <param name="classGen">An <see cref="IClassGenerator"/> whose data will be used to construct tha path.</param>
		/// <param name="directory">Optional override that will be used to generate the path.</param>
		string GetPath(IClassGenerator classGen, string directory = null);

		/// <summary>Constructs and returns the full path and file name.</summary>
		/// <returns>The path and filename.</returns>
		/// <param name="classGen">An <see cref="IClassGenerator"/> whose data will be used to construct tha path and file name.</param>
		/// <param name="filename">Optinal override to the file name.</param>
		/// <param name="directory">Optional override that will be used to generate the path.</param>
		string GetPathAndFilename(IClassGenerator classGen, string filename = null, string directory = null);
	}
}
