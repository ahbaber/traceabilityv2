﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TraceCodeGenerator.Support;

namespace TraceCodeGenerator
{
	/// <summary>Generates compiable factory code for a trace class.  The TraceFactory
	/// is based on <see langword="async"/> FunctionalFactory, and the TraceClass
	/// derived from the FunctionalClass the FunctionalFactory constructs.</summary>
	public class TraceFactoryGenerator : AbstractClassGeneratorNoSealed, IClassGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.TraceFactoryGenerator"/> class.</summary>
		/// <param name="functionalFactory">The functional factory the new trace factory will be based on.</param>
		/// <param name="traceClass">The trace class derived from the type the functional factory constructs.</param>
		/// <param name="name_space">The namespace the trace factory class will inhabit.</param>
		/// <param name="factoryName">Optional name for the factory, this will override the automtically generated class name.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/>.</param>
		public TraceFactoryGenerator(Type functionalFactory, Type traceClass, string name_space, string factoryName = null, SkipMap skipMap = null)
		: base(functionalFactory, Visibility.PublicFlags, name_space, skipMap)
		{
			if (HasError) { return; }

			if (traceClass == null)
			{
				Error = $"{nameof(traceClass)} cannot be null.";
				return;
			}

			_traceClass = traceClass;
			_classInfo.AddNamespace(traceClass);
			SetNames(factoryName, "Trace");

			AddUsing("Traceability");
		}

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.TraceFactoryGenerator"/> class.</summary>
		/// <param name="typeName">The string name of the functional factory the new trace factory will be based on.</param>
		/// <param name="traceClass">The string name of the trace class derived from the type the functional factory constructs.</param>
		/// <param name="name_space">The namespace the factory will inhabit.</param>
		/// <param name="factoryName">Optional name for the factory, this will override the automtically generated class name.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/>.</param>
		public TraceFactoryGenerator(string typeName, string traceClass, string name_space, string factoryName = null, SkipMap skipMap = null)
		: base(typeName, Visibility.PublicFlags, name_space, skipMap)
		{
			if (HasError) { return; }

			traceClass = TypeNames.GetQualifiedName(traceClass);
			_traceClass = Type.GetType(traceClass);
			if (_traceClass == null)
			{
				Error = $"Could not locate valid type with '{traceClass}'";
				return;
			}

			_classInfo.AddNamespace(_traceClass);
			SetNames(factoryName, "Trace");
			AddUsing("Traceability");
		}

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.TraceFactoryGenerator"/> class.
		/// This constructor allow us to use previous class generators to chain code generation.</summary>
		/// <param name="factory">The <see cref="FactoryGenerator"/> that created the base factory at an eariler step.</param>
		/// <param name="traceClass">The <see cref="TraceClassGenerator"/> that created the trace class at an eariler step.</param>
		/// <param name="name_space">The namespace the factory will inhabit.</param>
		/// <param name="factoryName">Optional name for the factory, this will override the automtically generated class name.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/>.</param>
		public TraceFactoryGenerator(FactoryGenerator factory, TraceClassGenerator traceClass, string name_space, string factoryName = null, SkipMap skipMap = null)
		: this(factory.QualifiedName, traceClass.QualifiedName, name_space, factoryName, skipMap)
		{ }

		/// <summary>All files should end with ".cs".</summary>
		/// <value>".cs"</value>
		public override string FileType => ".cs";

		/// <summary>Generates a string containing the trace factory code.</summary>
		/// <returns>A <see cref="string"/> containing compilable code.</returns>
		public override string Build()
		{
			if (HasError) { return string.Empty; }

			AddOpeningBoilerplate(true);

			_constructors.Build(_builder, _classInfo, Filename);
			_methods.Build(_builder, _classInfo, _traceClass);

			_builder.Append("\n\t\tprivate readonly ITracer _tracer;");

			AddClosingBoilerPlate();
			return _builder.ToString();
		}

		private readonly Type _traceClass;
		private readonly ConstructorsTraceClass _constructors = ConstructorsTraceClass.Singleton;
		private readonly MethodsTraceFactory _methods = MethodsTraceFactory.Singleton;
	}
}
