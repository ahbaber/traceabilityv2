﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.IO;
using TraceCodeGenerator.FileSupport;

namespace TraceCodeGenerator
{
	public class FileWriter : IFileWriter
	{
		public FileWriter(IPathGenerator pathGenerator = null)
		{
			_pathGenerator = pathGenerator ?? new PathGenerator();
		}

		/// <summary>Writes the code generated by <paramref name="classGen"/> to
		/// disk.  The path and filename will be returned if successful.  If there
		/// is an error in <paramref name="classGen"/>, then that willbe returned instead.</summary>
		/// <returns>The path and file name, or an error message.</returns>
		/// <param name="classGen">An <see cref="IClassGenerator"/> whose data will be used to construct tha path and file name.</param>
		/// <param name="filename">Optinal override to the file name.</param>
		/// <param name="directory">Optional override that will be used to generate the path.</param>
		public string CreateFile(IClassGenerator classGen, string filename = null, string directory = null)
		{
			if (classGen.HasError) { return classGen.Error; }

			filename = _pathGenerator.GetPathAndFilename(classGen, filename, directory);
			if (_diagnostics)
			{ return filename; }

			string path = _pathGenerator.GetPath(classGen, directory);
			Directory.CreateDirectory(path);

			using (var writer = new StreamWriter(filename, false))
			{ writer.Write(classGen.Build()); }

			return filename;
		}

		/// <summary>Sets a diagnostic mode where the the <see cref="IFileWriter"/>
		/// only emits the final message, but does not write the file to disk.</summary>
		/// <returns>The path and file name, or an error message.</returns>
		/// <param name="activate">If set to <c>true</c>, diagnostic mode is activated,
		/// <c>false</c> will turn off diagnostic mode.</param>
		public IFileWriter DiagnosticOnly(bool activate)
		{
			_diagnostics = activate;
			return this;
		}

		private readonly IPathGenerator _pathGenerator;
		private bool _diagnostics = false;
	}
}
