﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TraceCodeGenerator.Support;

namespace TraceCodeGenerator
{
	/// <summary>Generates compiable factory code for a functional class.</summary>
	public class FactoryGenerator : AbstractClassGenerator, IClassGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.FactoryGenerator"/> class.</summary>
		/// <param name="classType">The type the factory will construct.</param>
		/// <param name="name_space">The namespace the factory will inhabit.</param>
		/// <param name="factoryName">Optional name for the factory, this will override the automtically generated class name.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/>.</param>
		public FactoryGenerator(Type classType, string name_space, string factoryName = null, SkipMap skipMap = null)
		: base(classType, Visibility.PublicFlags, name_space, skipMap)
		{
			SetNames(factoryName, "", "Factory");
		}

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.FactoryGenerator"/> class.</summary>
		/// <param name="typeName">The string name of the type that the factory will construct.</param>
		/// <param name="name_space">The namespace the factory will inhabit.</param>
		/// <param name="factoryName">Optional name for the factory, this will override the automtically generated class name.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/>.</param>
		public FactoryGenerator(string typeName, string name_space, string factoryName = null, SkipMap skipMap = null)
			: base(typeName, Visibility.PublicFlags, name_space, skipMap)
		{
			SetNames(factoryName, "", "Factory");
		}

		/// <summary>All files should end with ".cs".</summary>
		/// <value>".cs"</value>
		public override string FileType => ".cs";

		/// <summary>sets the generated code to place generic parameter types on
		/// each of the Construct() methods.  e.g.
		///		public virtul Generic&lt;T>  Construct&lt;T>()
		/// This setting will not remove any generic type paramters from the class name,
		/// thus a custom class name will need to be passed in during construction.</summary>
		/// <returns>The current instance of <see cref="FactoryGenerator"/>.</returns>
		public IClassGenerator PlaceGenericParametersOnConstruct()
		{
			_placeGenericsOnConstruct = true;
			return this;
		}

		/// <summary>Generate a strinf containing the factory code.</summary>
		/// <returns>A <see cref="string"/> containing compilable code.</returns>
		public override string Build()
		{
			if (HasError) { return string.Empty; }

			AddOpeningBoilerplate(false);

			_constructors.Build(_builder, _classInfo, _placeGenericsOnConstruct);

			AddClosingBoilerPlate();
			return _builder.ToString();
		}

		private bool _placeGenericsOnConstruct = false;
		private readonly ConstructorsFactory _constructors = ConstructorsFactory.Singleton;
	}
}
