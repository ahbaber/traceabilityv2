﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TraceCodeGenerator.Support;

namespace TraceCodeGenerator
{
	/// <summary>Generates compilable trace code from a base class.</summary>
	public class TraceClassGenerator : AbstractClassGeneratorNoSealed, IClassGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.TraceClassGenerator"/> class.</summary>
		/// <param name="classType">The class whose code will be traced.</param>
		/// <param name="name_space">The namespace the trace class will inhabit.</param>
		/// <param name="traceName">Optional name for the resulting trace class.
		/// If this is null, then the name will be "Trace" prepended onto the base class's name.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/>.</param>
		public TraceClassGenerator(Type classType, string name_space, string traceName = null, SkipMap skipMap = null)
		: base(classType, Visibility.Flags, name_space, skipMap)
		{
			if (HasError) { return; }

			SetNames(traceName, "Trace");
			AddUsing("Traceability");
		}

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.TraceClassGenerator"/> class.</summary>
		/// <param name="typeName">The string name of the type that the factory will construct.</param>
		/// <param name="name_space">The namespace the factory will inhabit.</param>
		/// <param name="traceName">Optional name for the factory, this will override the automtically generated class name.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/>.</param>
		public TraceClassGenerator(string typeName, string name_space, string traceName = null, SkipMap skipMap = null)
		: base(typeName, Visibility.Flags, name_space, skipMap)
		{
			if (HasError) { return; }

			SetNames(traceName, "Trace");
			AddUsing("Traceability");
		}

		/// <summary>All files should end with ".cs".</summary>
		/// <value>".cs"</value>
		public override string FileType => ".cs";

		/// <summary>Generates a string containing the trace code.</summary>
		/// <returns>A <see cref="string"/> containing compilable code.</returns>
		public override string Build()
		{
			if (HasError) { return string.Empty; }

			AddOpeningBoilerplate(true);

			// Filename has removed <...> from generics
			_constructors.Build(_builder, _classInfo, Filename);
			_properties.Build(_builder, _classInfo);
			_methods.Build(_builder, _classInfo);

			_builder.Append("\n\t\tprivate readonly ITracer _tracer;");

			AddClosingBoilerPlate();

			return _builder.ToString();
		}

		private readonly ConstructorsTraceClass _constructors = ConstructorsTraceClass.Singleton;
		private readonly PropertiesTraceClass _properties = PropertiesTraceClass.Singleton;
		private readonly MethodsTraceClass _methods = MethodsTraceClass.Singleton;
	}
}
