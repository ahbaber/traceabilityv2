﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;

namespace TraceCodeGenerator
{
	/// <summary>Interface for classes that may be used to autogenerate code.</summary>
	public interface IClassGenerator
	{
		/// <summary>Gets the namespace that will be used in the generated code.</summary>
		/// <value>The namespace of the trace class.</value>
		string Namespace { get; }

		/// <summary>The type name of the generated type.  This field will be the
		/// class name in the generated code.</summary>
		/// <value>The declaration name of the generated class.</value>
		string DeclarationName { get; }

		/// <summary>The qualified name of the generated type.  For the qualified
		/// name to exist, the DeclarationName and Filename must be the same.  If
		/// they are not the same, then QualifiedName will be an empty string.</summary>
		/// <value>The qualified name of the generated class.</value>
		string QualifiedName { get; }

		/// <summary>Gets the name of the class of the generated code.
		/// The name needs to be safe to use as a filename.</summary>
		/// <value>The name of the generated class.</value>
		string Filename { get; }

		/// <summary>Gets the required filetype for the generated file.</summary>
		/// <value>The file type of the file.</value>
		string FileType { get; }

		/// <summary>Indicates if any errors occured while generating a class.</summary>
		/// <value><c>true</c> if an error occurred; otherwise, <c>false</c>.</value>
		bool HasError { get; }

		/// <summary>Contains any error messages from code generation.  Will be null
		/// if there are no errors.</summary>
		/// <value>Null, or a string with an error message.</value>
		string Error { get; }

		/// <summary>Sets additional using clauses to appear in the generated code.</summary>
		/// <returns>The current instance of the <see cref="IClassGenerator"/>.</returns>
		/// <param name="name_space">The namespace that will appear in the using clause.</param>
		IClassGenerator AddUsing(string name_space);

		/// <summary>Causes the partial keyword to appear in the generated code.</summary>
		/// <returns>The current instance of the <see cref="IClassGenerator"/>.</returns>
		IClassGenerator MakePartial();

		/// <summary>Causes the genrated class to given a visibility of internal instead of the default public.</summary>
		/// <returns>The current instance of the <see cref="IClassGenerator"/>.</returns>
		IClassGenerator MakeInternal();

		/// <summary>Generates a string containing generated code.</summary>
		/// <returns>A <see cref="string"/> containing compilable code.</returns>
		String Build();
	}
}
