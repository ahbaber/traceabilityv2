﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace TraceCodeGenerator.Support
{
	/// <summary>Extracts the type info from <see cref="Type"/> and converts the
	/// info into a compilable string.
	/// The order of conversion is:
	/// 	Customhandler
	///		GenericTypes
	///		Reference / ValueTypes
	///
	///	Adding a CustomerHandler will override any other logic; so the CustomHandler
	/// must do all the necessary work.  The primary reason for providing the capability
	/// of adding custom handlers is to allow developers to inject new logic for
	/// cases that have yet to be covered.
	/// </summary>
	public class TypeNames
	{
		public delegate StringBuilder TypeNameDelegate(StringBuilder builder, Type type, bool useFullName);

		/// <summary>Provides a singleton instance of <see cref="TypeNames"/>.</summary>
		/// <value>The singleton version of <see cref="TypeNames"/>.</value>
		public static TypeNames Singleton { get; } = new TypeNames();

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.TypeNames"/> class.</summary>
		public TypeNames()
		{
			_overrides = new Dictionary<string, TypeNameDelegate>();
			_typeTranslator = new Dictionary<string, string>
			{
				{ "Void", "void" },
				{ "System.Void", "void" },
				{ "Boolean", "bool" },
				{ "System.Boolean", "bool" },
				{ "Byte", "byte" },
				{ "System.Byte", "byte" },
				{ "SByte", "sbyte" },
				{ "System.SByte", "sbyte" },
				{ "Char", "char" },
				{ "System.Char", "char" },
				{ "Decimal", "decimal" },
				{ "System.Decimal", "decimal" },
				{ "Double", "double" },
				{ "System.Double", "double" },
				{ "Single", "float" },
				{ "System.Single", "float" },
				{ "Int32", "int" },
				{ "System.Int32", "int" },
				{ "UInt32", "uint" },
				{ "System.UInt32", "uint" },
				{ "Int64", "long" },
				{ "System.Int64", "long" },
				{ "UInt64", "ulong" },
				{ "System.UInt64", "ulong" },
				{ "Object", "object" },
				{ "System.Object", "object" },
				{ "Int16", "short" },
				{ "System.Int16", "short" },
				{ "UInt16", "ushort" },
				{ "System.UInt16", "ushort" },
				{ "String", "string" },
				{ "System.String", "string" },
			};
		}

		/// <summary>Adds a custom translation to the internal translator.</summary>
		/// <returns>The translated type name.</returns>
		/// <param name="from">The original type name.</param>
		/// <param name="to">The transalted type name.</param>
		public TypeNames AddTranslation(string from, string to)
		{
			_typeTranslator.Add(from, to);
			return this;
		}

		/// <summary>Adds a handler for translating generic types.</summary>
		/// <returns>The original instance of <see cref="TypeNames"/>.</returns>
		/// <param name="type">The <see cref="Type"/> that needs translating.</param>
		/// <param name="handler">The handler that processes the translating.</param>
		public TypeNames AddCustomHandler(Type type, TypeNameDelegate handler)
		{
			_overrides.Add(type.FullName, handler);
			return this;
		}

		/// <summary>Extracts the type name of a property.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in as <paramref name="builder"/>.</returns>
		/// <param name="propertyInfo">The property whose type name we are extracting.</param>
		/// <param name="useFullName">If set to <c>true</c>, then the full namespace will be prepended to the output.</param>
		public StringBuilder GetTypeName(StringBuilder builder, PropertyInfo propertyInfo, bool useFullName = false)
		{
			return GetTypeName(builder, propertyInfo.PropertyType, useFullName);
		}

		/// <summary>Extracts the type name of a type and returns it as a string.</summary>
		/// <returns>A <see cref="string"/> containing the typename.</returns>
		/// <param name="type">The <see cref="Type"/> whose typename will be returned.</param>
		/// <param name="useFullName">If set to <c>true</c>, then the full namespace will be prepended to the output.</param>
		public string GetTypeName(Type type, bool useFullName = false)
		{
			return GetTypeName(new StringBuilder(), type, useFullName).ToString();
		}

		/// <summary>Extracts the type name of a type and places it into the <see cref="StringBuilder"/>.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in as <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will contain the results.</param>
		/// <param name="type">The <see cref="Type"/> whose typename will be inserted into <paramref name="builder"/>.</param>
		/// <param name="useFullName">If set to <c>true</c>, then the full namespace will be prepended to the output.</param>
		public StringBuilder GetTypeName(StringBuilder builder, Type type, bool useFullName = false)
		{
			if (_overrides.TryGetValue(type.FullName ?? type.Name, out TypeNameDelegate customHandler))
			{
				customHandler(builder, type, useFullName);
			}
			else
			{
				string name = useFullName
					? type.FullName ?? type.Name
					: type.Name;
				name = name.Replace('+', '.');

				if (type.IsGenericType)
				{
					name = name.Remove(name.IndexOf('`'));
					if (!NullableHandler(builder, name, type))
					{ GenericHandler(builder, name, type); }
				}
				else
				{
					int refIndex = name.IndexOf('&');
					if (refIndex > -1)
					{ name = name.Remove(refIndex); }

					if (_typeTranslator.TryGetValue(name, out string translated))
					{
						builder.Append(translated);
					}
					else
					{
						builder.Append(name);
					}
				}
			}
			return builder;
		}

		/// <summary>Extracts the type name of a parameter.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in as <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will contain the results.</param>
		/// <param name="parameter">The parameter whose type name we are extracting.</param>
		/// <param name="useFullName">If set to <c>true</c>, then the full namespace will be prepended to the output.</param>
		public StringBuilder GetTypeName(StringBuilder builder, ParameterInfo parameter, bool useFullName = false)
		{
			if (parameter.ParameterType.IsByRef)
			{
				builder.Append(parameter.IsOut
					? "out "
					: "ref ");
				GetTypeName(builder, parameter.ParameterType.GetElementType(), useFullName);
			}
			else if (parameter.IsDefined(typeof(ParamArrayAttribute), false))
			{
				builder.Append("params ");
				GetTypeName(builder, parameter.ParameterType.GetElementType(), useFullName);
				builder.Append("[]");
			}
			else
			{
				GetTypeName(builder, parameter.ParameterType, useFullName);
			}
			return builder;
		}

		/// <summary>The default handler for processing generic types.  This was
		/// made public so that custom <see cref="TypeNameDelegate"/>s could use it
		/// if desired.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in as <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will contain the results.</param>
		/// <param name="name">The name of the generic type that will be emitted as part of the type name.</param>
		/// <param name="type">The <see cref="Type"/> that needs translating.</param>
		public StringBuilder GenericHandler(StringBuilder builder, string name, Type type)
		{
			var typeArray = type.GetGenericArguments();
			builder.Append(name);
			GetGenericParameters(builder, type);
			return builder;
		}

		/// <summary>Retrieves the generic types, and formats them into a comma separated
		/// list inside of angle brackets.  e.g. &lt;Type1, Type2>.  If <paramref name="type"/>
		/// is not a generic, then no data will be added to the <paramref name="builder"/>.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in as <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will contain the results.</param>
		/// <param name="type">The <see cref="Type"/> whose generic paramters we are extracting.</param>
		public StringBuilder GetGenericParameters(StringBuilder builder, Type type)
		{
			if (type.IsGenericType)
			{
				var typeArray = type.GetGenericArguments();
				if (typeArray.Length > 0)
				{
					builder.Append("<");
					GetTypeName(builder, typeArray[0]);
					for (int i = 1; i < typeArray.Length; ++i)
					{
						builder.Append(", ");
						GetTypeName(builder, typeArray[i]);
					}
					builder.Append(">");
				}
			}
			return builder;
		}

		/// <summary>Generates a class name that may be used in the class declaration line.</summary>
		/// <returns>A translated class name.</returns>
		/// <param name="type">The <see cref="Type"/> whose name is to be translated.</param>
		/// <param name="suffix">Optional suffix to append to the name of the type.</param>
		public string GetDeclarationName(Type type, string suffix = "")
		{
			string name = type.Name;
			string translated;
			if (type.IsGenericType)
			{
				if (!type.IsGenericTypeDefinition)
				{ name = GetConstructorName(type, suffix); }
				else
				{
					var builder = new StringBuilder(name.Remove(name.IndexOf('`')));
					builder.Append(suffix);
					name = GetGenericParameters(builder, type).ToString();
				}
			}
			else
			{
				if (_typeTranslator.TryGetValue(name, out translated))
				{
					name = translated;
				}
				name += suffix;
			}

			if (_typeTranslator.TryGetValue(name, out translated))
			{
				return translated;
			}

			return name;
		}

		/// <summary>Generates a class name that is safe to use as a filename or
		/// inside of the trace data.  If a non-generic type is passed in, then
		/// the name will be given name of the class.  If generic type is passed
		/// in, and none of the types are set, then the name will be the base
		/// generic name. e.g. List&lt;> -> List.  If a generic with types is passed in,
		/// then the name will be the base generic name with the types appended.
		/// e.g. Dictionary&lt;string,int> -> Dictionary_string_int</summary>
		/// <returns>A translated class name.</returns>
		/// <param name="type">The <see cref="Type"/> whose name is to be translated.</param>
		/// <param name="suffix">Optional suffix to append to the name of the type.</param>
		public string GetConstructorName(Type type, string suffix = "")
		{
			string name = type.Name;
			string translated;
			if (type.IsGenericType)
			{
				name = name.Remove(name.IndexOf('`'));
				name += suffix;
				if (!type.IsGenericTypeDefinition)
				{
					var typeArray = type.GetGenericArguments();
					foreach (var subtype in typeArray)
					{
						name += "_" + GetConstructorName(subtype);
					}
				}
			}
			else
			{
				if (_typeTranslator.TryGetValue(name, out translated))
				{
					name = translated;
				}
				name += suffix;
			}

			if (_typeTranslator.TryGetValue(name, out translated))
			{
				name = translated;
			}

			return name;
		}

		/// <summary>Tries to change a type name string into a qualified name.
		/// It works by looking at the name prior to the first period, and assuming
		/// it is the assembly name for the type.  It then makes sure the assembly name is appended
		/// on the end, after a comma.  If an assembly name after a comma already
		/// exists, then it does not change the type name.
		/// e.g:
		///     assemblyName.namespace.className -> assemblyName.namespace.className,assemblyName
		///     assemblyName.className,assemblyName -> assemblyName.className,assemblyName
		///     assemblyName.className -> assemblyName.className,assemblyName</summary>
		/// <returns>A qualified name, if possible.</returns>
		/// <param name="typeName">Type name.</param>
		public static string GetQualifiedName(string typeName)
		{
			typeName = typeName ?? string.Empty; // will have silent failure instead
			if (typeName.IndexOf(',') == -1) // missing library name
			{
				int index = typeName.IndexOf('.'); // assuming name prior to first period is library name
				if (index > -1)
				{
					string assemblyName = typeName.Remove(index);
					typeName += $",{assemblyName}";
				}
			}
			return typeName;
		}

		private bool NullableHandler(StringBuilder builder, string name, Type type)
		{
			if (name.Equals("Nullable")
			|| name.Equals("System.Nullable"))
			{
				var typeArray = type.GetGenericArguments();
				GetTypeName(builder, typeArray[0]);
				builder.Append("?");
				return true;
			}
			return false;
		}

		private readonly Dictionary<string, TypeNameDelegate> _overrides;
		private readonly Dictionary<string, string> _typeTranslator;
	}
}
