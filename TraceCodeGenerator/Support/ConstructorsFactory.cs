﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using System.Text;

namespace TraceCodeGenerator.Support
{
	/// <summary>Support class for generating factories.</summary>
	public class ConstructorsFactory
	{
		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.ConstructorsFactory"/> class.</summary>
		/// <param name="parameters">A <see cref="Parameters"/> which handles formatting all parameters.</param>
		/// <param name="typeNames">Optional <see cref="TypeNames"/>, if one is not provided then the singleton will be used.</param>
		public ConstructorsFactory(Parameters parameters = null, TypeNames typeNames = null)
		{
			_parameters = parameters ?? Parameters.Singleton;
			_typeNames = typeNames ?? TypeNames.Singleton;
		}

		/// <summary>Provides a singleton instance of <see cref="ConstructorsFactory"/>.</summary>
		/// <value>The singleton instance of <see cref="ConstructorsFactory"/>.</value>
		public static ConstructorsFactory Singleton => new ConstructorsFactory();

		/// <summary>Creates a factory constructor for each public constructor of
		/// the <paramref name="classInfo"/>'s constructors.  Each factory constructor
		/// will be public, virtual, and named "Construct".</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="classInfo">The class that that the factory will build.</param>
		/// <param name="includeGenericParameters">If true, then untyped generic parameters will be part of the method name.</param>
		public StringBuilder Build(StringBuilder builder, ClassInfo classInfo, bool includeGenericParameters)
		{
			if (classInfo.Constructors.Count == 0)
			{ throw new Exception($"At least one public constructor is required to create a factory for {classInfo.ClassType.Name}"); }

			string typeName = _typeNames.GetTypeName(classInfo.ClassType);
			Build(builder, classInfo.Constructors[0], classInfo.ClassType, typeName, includeGenericParameters);
			for (int i = 1; i < classInfo.Constructors.Count; ++i)
			{
				builder.Append("\n");
				Build(builder, classInfo.Constructors[i], classInfo.ClassType, typeName, includeGenericParameters);
			}

			return builder;
		}

		private void Build(StringBuilder builder, ConstructorInfo constructorInfo, Type type, string baseName, bool includeGenericParameters)
		{
			var parameters = constructorInfo.GetParameters();

			builder.Append($"\n\t\tpublic virtual {baseName} Construct");
			if (includeGenericParameters && type.ContainsGenericParameters)
			{ _typeNames.GetGenericParameters(builder, type); }
			_parameters.BuildParameterList(builder, parameters);
			builder.Append("\n\t\t{");

			builder.Append($"\n\t\t\treturn new {baseName}");
			_parameters.BuildCalledParamterList(builder, parameters);
			builder.Append(";");

			builder.Append("\n\t\t}");
		}

		private readonly Parameters _parameters;
		private readonly TypeNames _typeNames;
	}
}
