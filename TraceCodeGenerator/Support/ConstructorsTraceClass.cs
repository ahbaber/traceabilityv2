﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Reflection;
using System.Text;

namespace TraceCodeGenerator.Support
{
	/// <summary>Support class for generating compilable trace constructors.</summary>
	public class ConstructorsTraceClass
	{
		/// <summary>Provides a singleton instance of <see cref="ConstructorsTraceClass"/>.</summary>
		/// <value>The singleton instance of <see cref="ConstructorsTraceClass"/>.</value>
		public static ConstructorsTraceClass Singleton => new ConstructorsTraceClass();

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.ConstructorFormatter"/> class.</summary>
		/// <param name="parameters">A <see cref="Parameters"/> which handles formatting all parameters.</param>
		public ConstructorsTraceClass(Parameters parameters = null)
		{
			_parameters = parameters ?? Parameters.Singleton;
		}

		/// <summary>Creates all the constuctors for class and places the code into a <see cref="StringBuilder"/>.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text..</param>
		/// <param name="classInfo">The functional class that is to be traced.</param>
		/// <param name="className">The name that will be used for the trace class.</param>
		public StringBuilder Build(StringBuilder builder, ClassInfo classInfo, string className)
		{
			foreach (var constructorInfo in classInfo.Constructors)
			{
				Build(builder, constructorInfo, className, classInfo.ClassType.Name);
			}
			return builder;
		}

		private void Build(StringBuilder builder, ConstructorInfo constructorInfo, string traceName, string baseName)
		{
			string visibility = Visibility.GetVisibility(constructorInfo);
			if (visibility.Equals("private"))
			{
				builder.Append($"\n\t\t// skipped private {baseName}");
				_parameters.BuildParameterList(builder, constructorInfo.GetParameters());
				builder.Append("\n");
				return;
			}

			builder.Append($"\n\t\t{visibility} {traceName}");
			var parameters = constructorInfo.GetParameters();
			_parameters.BuildTraceParameterList(builder, parameters);

			if (parameters.Length > 0)
			{
				builder.Append("\n\t\t:base");
				_parameters.BuildCalledParamterList(builder, parameters);
			}
			builder.Append("\n\t\t{\n\t\t\t_tracer = tracer;\n\t\t}\n");
		}

		private Parameters _parameters;
	}
}
