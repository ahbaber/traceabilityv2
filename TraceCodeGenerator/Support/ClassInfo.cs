﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Reflection;
using MethodMap = System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo>;

namespace TraceCodeGenerator.Support
{
	/// <summary>Performas as a central repository for all of a class's constructors,
	/// properties, and methods.</summary>
	public class ClassInfo
	{
		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.ClassInfo"/> class.
		/// If <paramref name="classType"/> is null, then the instance will still construct,
		/// but all fields will be empty lists or null.</summary>
		/// <param name="classType">The class whose info is being gathered.</param>
		/// <param name="flags">Visibility flags indicating which members should be gathered.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/> indicating what members should be skipped.</param>
		public ClassInfo(Type classType, BindingFlags flags, SkipMap skipMap = null)
		{
			ClassType = classType;
			if (classType != null)
			{ GatherMemberData(classType, flags, skipMap); }
		}

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.ClassInfo"/> class.
		/// If a type cannot be generated from <paramref name="typeName"/>, then the instance will still construct,
		/// but all fields will be empty lists or null.</summary>
		/// <param name="typeName">The string name of a type</param>
		/// <param name="flags">Visibility flags indicating which members should be gathered.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/> indicating what members should be skipped.</param>
		public ClassInfo(string typeName, BindingFlags flags, SkipMap skipMap = null)
		{
			var classType = Type.GetType(TypeNames.GetQualifiedName(typeName));
			ClassType = classType;
			if (ClassType != null)
			{ GatherMemberData(classType, flags, skipMap); }
		}

		/// <summary>The type info for the class.</summary>
		/// <value>The type of the class.</value>
		public Type ClassType { get; }

		/// <summary>A list of all constructors based on visibility.</summary>
		/// <value>Available constructors.</value>
		public List<ConstructorInfo> Constructors { get; } = new List<ConstructorInfo>();

		/// <summary>A list of all properties based on visibility and not skipped.</summary>
		/// <value>Available properties.</value>
		public List<PropertyInfo> Properties { get; } = new List<PropertyInfo>();

		/// <summary>A list of all properties skipped based on the <see cref="SkipMap"/>.</summary>
		/// <value>Skipped properties.</value>
		public List<PropertyInfo> PropertiesSkipped { get; } = new List<PropertyInfo>();

		/// <summary>A list of all methods based on visibility and not skipped.</summary>
		/// <value>Available methods.</value>
		public List<MethodInfo> Methods { get; } = new List<MethodInfo>();

		/// <summary>A lits of all methods skipped based on the <see cref="SkipMap"/>.</summary>
		/// <value>Skipped methods.</value>
		public List<MethodInfo> MethodsSkipped { get; } = new List<MethodInfo>();

		/// <summary>Contains a list of namespaces encountered while generating code.</summary>
		/// <value>A list of namespaces.</value>
		public List<string> Namespaces { get; } = new List<string>();

		/// <summary>Adds a namespace to the Namespaces list.  If the namespace is
		/// already present, then it will not be added again.</summary>
		/// <returns>The current instance of <see cref="ClassInfo"/>.</returns>
		/// <param name="name_space">A namespace represents as a string.</param>
		public ClassInfo AddNamespace(string name_space)
		{
			if (!Namespaces.Contains(name_space))
			{ Namespaces.Add(name_space); }
			return this;
		}

		/// <summary>Extracts the namespace from the type and adds it the Namespaces
		/// list.  If the namespace is already present, then it will not be added
		/// again.</summary>
		/// <returns>The current instance of <see cref="ClassInfo"/>.</returns>
		/// <param name="type">Any <see cref="Type"/> object.</param>
		public ClassInfo AddNamespace(Type type)
		{
			if (type != null)
			{
				AddNamespace(type.Namespace);//TypeNames.GetNamespace(type));
				if (type.IsGenericType)
				{
					var typeArray = type.GetGenericArguments();
					foreach (var sub in typeArray)
					{
						if (!type.IsGenericTypeDefinition)
						{ AddNamespace(sub); }
					}
				}
			}
			return this;
		}

		/// <summary>Scan a list of parameters, and adds each parameter's namespace
		/// to the Namespaces list.  If the namespace is already present, then it will not be added
		/// again.</summary>
		/// <returns>The current instance of <see cref="ClassInfo"/>.</returns>
		/// <param name="parameters">An array of <see cref="ParameterInfo"/>.</param>
		public ClassInfo ScanParameterNamespaces(ParameterInfo[] parameters)
		{
			foreach (var parameter in parameters)
			{
				if (parameter.ParameterType.IsByRef || parameter.IsDefined(typeof(ParamArrayAttribute), false))
				{
					AddNamespace(parameter.ParameterType.GetElementType());
				}
				else
				{
					AddNamespace(parameter.ParameterType);
				}
			}
			return this;
		}

		private void GatherMemberData(Type classType, BindingFlags flags, SkipMap skipMap)
		{
			skipMap = skipMap ?? _defaultSkipMap;
			AddNamespace(classType);

			foreach (var constructor in classType.GetConstructors(flags))
			{
				Constructors.Add(constructor);
				ScanParameterNamespaces(constructor.GetParameters());
			}

			var propertyMap = new MethodMap();
			foreach (var property in classType.GetProperties(flags))
			{
				// track property methods so we don't gather them below
				if (property.CanRead)
				{ propertyMap.Add(property.GetMethod.Name, property.GetMethod); }
				if (property.CanWrite)
				{ propertyMap.Add(property.SetMethod.Name, property.SetMethod); }

				if (skipMap.ShouldSkip(property))
				{ PropertiesSkipped.Add(property); }
				else
				{
					Properties.Add(property);
					AddNamespace(property.PropertyType);
				}
			}

			foreach (var method in classType.GetMethods(flags))
			{
				if (!propertyMap.ContainsKey(method.Name))
				{
					if (skipMap.ShouldSkip(method))
					{
						// we know these are always skipped, so don't bother tracking them
						if (!_defaultSkipMap.ShouldSkip(method))
						{ MethodsSkipped.Add(method); }
					}
					else
					{
						Methods.Add(method);
						AddNamespace(method.ReturnType);
						ScanParameterNamespaces(method.GetParameters());
					}
				}
			}
		}

		private static SkipMap _defaultSkipMap = new SkipMap();
	}
}
