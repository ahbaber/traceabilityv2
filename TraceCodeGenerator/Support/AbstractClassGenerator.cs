﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using System.Text;
using UsingList = System.Collections.Generic.List<string>;

namespace TraceCodeGenerator.Support
{
	/// <summary>Abstract class containing common code for generating compiable classe code.</summary>
	public abstract class AbstractClassGenerator : IClassGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.AbstractClassGenerator"/> class.</summary>
		/// <param name="baseType">The classType that the generated code will be baesd on.</param>
		/// <param name="flags">Visibility flags indicating which members should be gathered.</param>
		/// <param name="name_space">The new namespace being usede.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/> indicating what members should be skipped.</param>
		protected AbstractClassGenerator(Type baseType, BindingFlags flags, string name_space, SkipMap skipMap)
		{
			_classInfo = new ClassInfo(baseType, flags, skipMap);
			if (_classInfo.ClassType == null)
			{
				Error = $"{nameof(baseType)} is null.";
				return;
			}

			if (string.IsNullOrWhiteSpace(name_space))
			{
				Error = $"Invalid value for {nameof(name_space)}: '{name_space ?? "null"}'";
				return;
			}

			Namespace = name_space;
		}

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.AbstractClassGenerator"/> class.</summary>
		/// <param name="typeName">The string name of the type that the generated code will be baesd on.</param>
		/// <param name="flags">Visibility flags indicating which members should be gathered.</param>
		/// <param name="name_space">The new namespace being usede.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/> indicating what members should be skipped.</param>
		protected AbstractClassGenerator(string typeName, BindingFlags flags, string name_space, SkipMap skipMap)
		{
			_classInfo = new ClassInfo(typeName, flags, skipMap);
			if (_classInfo.ClassType == null)
			{
				typeName = TypeNames.GetQualifiedName(typeName);
				Error = $"Could not locate valid type with: '{typeName}'.";
				return;
			}

			if (string.IsNullOrWhiteSpace(name_space))
			{
				Error = $"Invalid value for {nameof(name_space)}: '{name_space ?? "null"}'";
				return;
			}

			Namespace = name_space;
		}

		/// <summary>Gets the namespace that will be used in the generated code.</summary>
		/// <value>The namespace of the trace class.</value>
		public string Namespace { get; }

		/// <summary>The type name of the generated type.  This field will be the
		/// class name in the generated code.</summary>
		/// <value>The declaration name of the generated class.</value>
		public string DeclarationName { get; private set; }

		/// <summary>The qualified name of the generated type.  For the qualified
		/// name to exist, the DeclarationName and Filename must be the same.  If
		/// they are not the same, then QualifiedName will be an empty string.</summary>
		/// <value>The qualified name of the generated class.</value>
		public string QualifiedName { get; private set; }

		/// <summary>Gets the name of the class of the generated code.
		/// The name needs to be safe to use as a filename.</summary>
		/// <value>The name of the generated class.</value>
		public string Filename { get; protected set; }

		/// <summary>Gets the required filetype for the generated file.</summary>
		/// <value>The file type of the file.</value>
		public abstract string FileType { get; }

		/// <summary>Indicates if any errors occured while generating a class.</summary>
		/// <value><c>true</c> if an error occurred; otherwise, <c>false</c>.</value>
		public bool HasError => (Error != null);

		/// <summary>Contains any error messages from code generation.  Will be null
		/// if there are no errors.</summary>
		/// <value>Null, or a string with an error message.</value>
		public string Error { get; protected set; }

		/// <summary>Generates additonal using clauses in the generated code.</summary>
		/// <returns>The current instance of the <see cref="IClassGenerator"/>.</returns>
		/// <param name="name_space">The namespace that will appear in the using clause.</param>
		public IClassGenerator AddUsing(string name_space)
		{
			_usingList.Add(name_space);
			return this;
		}

		/// <summary>Builds a string containing the generated code.</summary>
		/// <returns>A <see cref="string"/> containing compilable code.</returns>
		public abstract string Build();

		/// <summary>Causes the genrated class to given a visibility of internal instead of the default public.</summary>
		/// <returns>The current instance of the <see cref="IClassGenerator"/>.</returns>
		public IClassGenerator MakeInternal()
		{
			_isInternal = true;
			return this;
		}

		/// <summary>Causes the partial keyword to appear in the generated code.</summary>
		/// <returns>The current instance of the <see cref="IClassGenerator"/>.</returns>
		public IClassGenerator MakePartial()
		{
			_isPartial = true;
			return this;
		}

		/// <summary>Fills in the properties DeclarationName, Filename, and
		/// QualifiedName.  The names will automatically generated from the base class
		/// unless an override name is given.  If the override <paramref name="givenName"/>
		/// is used, then <paramref name="prefix"/> and <paramref name="suffix"/>
		/// will be ignored.</summary>
		/// <param name="givenName">Optional string that will override the generated names.</param>
		/// <param name="prefix">The prefix will be prepended to the generated names.</param>
		/// <param name="suffix">The suffix will be appended to generated names.</param>
		protected void SetNames(string givenName, string prefix = "", string suffix = "")
		{
			if (HasError) { return; }

			if (string.IsNullOrWhiteSpace(givenName))
			{
				var type = _classInfo.ClassType;
				if (type == null) { return; }

				Filename = prefix + _typeNames.GetConstructorName(_classInfo.ClassType, suffix);
				DeclarationName = prefix + _typeNames.GetDeclarationName(_classInfo.ClassType, suffix);
			}
			else
			{
				Filename = givenName;
				DeclarationName = givenName;
			}
			QualifiedName = (DeclarationName.Equals(Filename))
				? TypeNames.GetQualifiedName($"{Namespace}.{DeclarationName}")
				: string.Empty;
		}

		/// <summary>Constructs common boilerplate code for generated classes.
		/// Specifically: a warning that the file is generated and any changes may be lost
		///		the using statements
		///		the namespace and opening curly brace
		///		the class visibility and name
		///		the parent class, if appropriate
		///		class opening curly brace
		/// </summary>
		/// <param name="inheritFromBase">Set to true if the generated class is inheriting from the base class.</param>
		protected void AddOpeningBoilerplate(bool inheritFromBase)
		{
			if (HasError) { return; }

			_builder.Clear();
			_builder.Append("// Auto generated file\n");
			_builder.Append("// Any modification to this file may be lost\n");

			foreach (var ns in _usingList)
			{ _classInfo.AddNamespace(ns); }
			_classInfo.Namespaces.Sort();

			foreach (var ns in _classInfo.Namespaces)
			{
				_builder.Append($"using {ns};\n");
			}

			_builder.Append($"\nnamespace {Namespace}\n{{");
			string visibility = _isInternal ? "internal" : "public";
			string part = _isPartial ? "partial " : "";
			_builder.Append($"\n\t{visibility} {part}class {DeclarationName}");
			if (inheritFromBase)
			{
				string baseName = _typeNames.GetTypeName(_classInfo.ClassType, true);
				_builder.Append($" : {baseName}");
			}
			_builder.Append("\n\t{");
		}

		/// <summary>Add the closing curly braces for the class and namespace to the builder.</summary>
		protected void AddClosingBoilerPlate()
		{
			if (HasError) { return; }

			_builder.Append("\n\t}\n}\n");
		}

		/// <summary>The class the generator is focused on.</summary>
		protected ClassInfo _classInfo;

		/// <summary>True if the generated class has a visibility of internal.</summary>
		protected bool _isInternal = false;

		/// <summary>True if a partial class is being generated.</summary>
		protected bool _isPartial = false;

		/// <summary>A <see cref="StringBuilder"/> used to hold the generated code.</summary>
		protected StringBuilder _builder = new StringBuilder();

		/// <summary>A list of all namespaces that the generated class may depend on.</summary>
		protected readonly UsingList _usingList = new UsingList();

		/// <summary>A <see cref="TypeNames"/> instance used to generate specific type names.</summary>
		protected readonly TypeNames _typeNames = TypeNames.Singleton;
	}
}
