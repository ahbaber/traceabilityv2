﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
namespace TraceCodeGenerator.Support
{
	/// <summary>Represents an error where some member of the trace class does not properly represent
	/// the corresponding member of the base class.</summary>
	public class VerificationError
	{
		/// <summary>The name of the member that generated the error.</summary>
		public string MemberName { get; set; }

		/// <summary>The specific error.</summary>
		public IgnoreReason Reason { get; set; }
	}
}
