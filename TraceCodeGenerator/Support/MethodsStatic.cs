﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using MethodMap = System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo>;

namespace TraceCodeGenerator.Support
{
	public static class MethodsStatic
	{
		/// <summary>Generates a paramatized name of a method.  This allows overloaded
		/// methods to be uniquely identified.</summary>
		/// <returns>A <see cref="string"/> containing the parameterized name.</returns>
		/// <param name="methodInfo">The method whose parameterized name is being generated..</param>
		public static string GetParameterizedName(this MethodInfo methodInfo)
		{
			var builder = new StringBuilder();
			builder.Append(methodInfo.Name).Append("_").Append(methodInfo.ReturnType.UnderlyingSystemType);
			foreach (var param in methodInfo.GetParameters())
			{
				builder.Append("_").Append(param.ParameterType);
			}
			return builder.ToString();
		}

		/// <summary>Generates parameterized names for every method of a class.</summary>
		/// <returns>A list containing the parameterized names.</returns>
		/// <param name="baseType">The class whose methods will be used to generate the parameterized names.</param>
		public static List<string> GetAllParameterizedNames(this Type baseType)
		{
			var list = new List<string>();
			var propertyMap = GetPropertyMap(baseType);

			foreach (var methodInfo in baseType.GetMethods())
			{
				if (!propertyMap.ContainsKey(methodInfo.Name))
				{ list.Add(GetParameterizedName(methodInfo)); }
			}

			return list;
		}

		private static MethodMap GetPropertyMap(Type baseType)
		{
			var propertyMap = new MethodMap();
			foreach (var property in baseType.GetProperties(Visibility.Flags))
			{
				if (property.CanRead)
				{ propertyMap.Add(property.GetMethod.Name, property.GetMethod); }

				if (property.CanWrite)
				{ propertyMap.Add(property.SetMethod.Name, property.SetMethod); }
			}

			return propertyMap;
		}

	}
}
