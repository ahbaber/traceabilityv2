﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Reflection;
using System.Text;

namespace TraceCodeGenerator.Support
{
	/// <summary>Support class for formatting parameter lists.</summary>
	public class Parameters
	{
		/// <summary>Provides a singleton instance of <see cref="Parameters"/>.</summary>
		/// <value>The singleton version of <see cref="Parameters"/>.</value>
		public static Parameters Singleton => new Parameters();

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.ParameterFormatter"/> class.</summary>
		/// <param name="typeNames">Optional <see cref="TypeNames"/>, if one is not provided then the singleton will be used.</param>
		public Parameters(TypeNames typeNames = null)
		{
			_typeNames = typeNames ?? TypeNames.Singleton;
		}

		/// <summary>Creates a parameter list enclosed in parenthesis.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="parameters">An array of <see cref="ParameterInfo"/> containing
		/// all of the parameters to be formatted.</param>
		public StringBuilder BuildParameterList(StringBuilder builder, ParameterInfo[] parameters)
		{
			builder.Append("(");
			if (parameters.Length > 0)
			{
				return BuildParameterListWorker(builder, parameters);
			}
			return builder.Append(")");
		}

		/// <summary>Creates a parameter list enclosed in parenthesis.  An Itracer
		/// argument will be the first parameter.  Ex: (int a, bool b) -> (Itracer tracer, int a, boolo b).</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="parameters">An array of <see cref="ParameterInfo"/> containing
		/// all of the parameters to be formatted.</param>
		public StringBuilder BuildTraceParameterList(StringBuilder builder, ParameterInfo[] parameters)
		{
			builder.Append("(ITracer tracer");
			if (parameters.Length > 0)
			{
				builder.Append(", ");
				return BuildParameterListWorker(builder, parameters);
			}
			return builder.Append(")");
		}

		/// <summary>Formats a list of parameters into into a called sequence.
		/// Ex: (int a, bool b) -> (a, b)</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text..</param>
		/// <param name="parameters">An array of <see cref="ParameterInfo"/> containing
		/// all of the parameters to be formatted.</param>
		/// <param name="addTracer">If set to <c>true</c> add _tracer as the rist parameter.</param>
		public StringBuilder BuildCalledParamterList(StringBuilder builder, ParameterInfo[] parameters, bool addTracer = false)
		{
			builder.Append("(");
			if (addTracer)
			{ builder.Append("_tracer"); }
			if (parameters.Length > 0)
			{
				if (addTracer)
				{ builder.Append(", "); }
				BuildCallName(builder, parameters[0]);
				for (int i = 1; i < parameters.Length; ++i)
				{
					builder.Append(", ");
					BuildCallName(builder, parameters[i]);
				}
			}
			return builder.Append(")");
		}

		private StringBuilder BuildParameterListWorker(StringBuilder builder, ParameterInfo[] parameters)
		{
			if (parameters.Length > 0)
			{
				BuildParameter(builder, parameters[0]);
				for (int i = 1; i < parameters.Length; ++i)
				{
					builder.Append(", ");
					BuildParameter(builder, parameters[i]);
				}
			}
			return builder.Append(")");
		}

		private void BuildParameter(StringBuilder builder, ParameterInfo parameter)
		{
			_typeNames.GetTypeName(builder, parameter);
			builder.Append($" {parameter.Name}");
			if (parameter.HasDefaultValue)
			{
				builder.Append(" = ");
				if (parameter.DefaultValue == null)
				{
					builder.Append("null");
				}
				else if (parameter.ParameterType == typeof(string))
				{
					builder.Append($"\"{parameter.DefaultValue}\"");
				}
				else if (parameter.ParameterType == typeof(char))
				{
					builder.Append($"'{parameter.DefaultValue}'");
				}
				else if (parameter.ParameterType == typeof(bool))
				{
					builder.Append(parameter.DefaultValue.Equals(true)
						? "true"
						: "false");
				}
				else if (parameter.ParameterType == typeof(decimal))
				{
					builder.Append($"{parameter.DefaultValue}m");
				}
				else if (parameter.ParameterType == typeof(float))
				{
					builder.Append($"{parameter.DefaultValue}f");
				}
				else
				{
					builder.Append(parameter.DefaultValue);
				}
			}
		}

		private void BuildCallName(StringBuilder builder, ParameterInfo parameterInfo)
		{
			if (parameterInfo.ParameterType.IsByRef)
			{
				builder.Append(parameterInfo.IsOut
					? "out "
					: "ref ");
			}
			builder.Append(parameterInfo.Name);
		}

		private TypeNames _typeNames;
	}
}
