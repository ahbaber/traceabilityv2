﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Reflection;

namespace TraceCodeGenerator.Support
{
	/// <summary>Helper class to determine the visibility of constructors, properties, and methods.</summary>
	public static class Visibility
	{
		/// <summary>The flags that are used by code generation to determine which members will be traced.</summary>
		public static readonly BindingFlags Flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

		/// <summary>The flags that are used by code generation to determine which members are public.</summary>
		public static readonly BindingFlags PublicFlags = BindingFlags.Instance | BindingFlags.Public;

		/// <summary>Determines the visibility for constructors and methods.</summary>
		/// <returns>public, internal, or protected.</returns>
		/// <param name="methodInfo">The constructor or method whose visibility is being determined.</param>
		public static string GetVisibility(MethodBase methodInfo)
		{
			if (methodInfo.IsPublic)
			{ return "public"; }

			if (methodInfo.IsFamily)
			{ return "protected"; }

			if (methodInfo.IsPrivate)
			{ return "private"; }

			if (methodInfo.IsAssembly)
			{ return "internal"; }

			if (methodInfo.IsFamilyOrAssembly)
			{ return "protected internal"; }

			// private protected not supported in C# 7.0 / DotNetCore2.1
			if (methodInfo.IsFamilyAndAssembly)
			{ return "private protected"; }

			return "unknown";
		}

		/// <summary>Determines the visibility for properties.</summary>
		/// <returns>public, internal, or protected.</returns>
		/// <param name="propertyInfo">The property whose visibility is being determined.</param>
		public static string GetVisibility(PropertyInfo propertyInfo)
		{
			string getVisibility = propertyInfo.CanRead ? GetVisibility(propertyInfo.GetMethod) : "does not exist";
			string setVisibility = propertyInfo.CanWrite ? GetVisibility(propertyInfo.SetMethod) : "does not exist";

			if (getVisibility.Equals("public") || setVisibility.Equals("public"))
			{ return "public"; }

			if (getVisibility.Equals("protected internal") || setVisibility.Equals("protected internal"))
			{ return "protected internal"; }

			if (getVisibility.Equals("internal") || setVisibility.Equals("internal"))
			{ return "internal"; }

			if (getVisibility.Equals("protected") || setVisibility.Equals("protected"))
			{ return "protected"; }

			// private protected not supported in C# 7.0 / DotNetCore2.1

			return "private"; // for completeness sake
		}
	}
}
