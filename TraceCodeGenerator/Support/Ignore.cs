﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Reflection;

namespace TraceCodeGenerator.Support
{
	/// <summary>A set of values indicating why a member is not, or should not be traced.</summary>
	public enum IgnoreReason
	{
		/// <summary>The reason that a member is not traced is unknown, or the member was not specified on the base class.</summary>
		Unspecified,

		/// <summary>Indicates that we have not traced the property or method.</summary>
		NotImplemented,

		/// <summary>Indicates we are ignoring that the member is not traced due to it not being virtual.</summary>
		NotVirtual,

		/// <summary>Indicates we are ignoring that the member is not traced due to it being sealed.</summary>
		IsFinal,

		/// <summary>Indicates we are ignoring that the member is not traced due to it being static.</summary>
		IsStatic,

		/// <summary>Indicates we are ignoring that the member is not traced due to it being private.</summary>
		IsPrivate,
	}

	/// <summary>Extensions methods for working with <see cref="IgnoreReason"/>.</summary>
	public static class IgnoreReasonExtensionMethods
	{
		/// <summary>Retrieves an intrinsic <see cref="IgnoreReason"/> why a method should be ignored.</summary>
		/// <returns>An <see cref="IgnoreReason"/>.</returns>
		/// <param name="methodInfo">The method being examined.</param>
		public static IgnoreReason GetIgnoreReason(this MethodInfo methodInfo)
		{
			return GetIgnorereasonOrDefault(methodInfo, IgnoreReason.Unspecified);
		}

		/// <summary>Retrieves the <see cref="IgnoreReason"/> a method was not traced.</summary>
		/// <returns>An <see cref="IgnoreReason"/>.</returns>
		/// <param name="methodInfo">The method being examined.</param>
		public static IgnoreReason GetUntracedReason(this MethodInfo methodInfo)
		{
			return GetIgnorereasonOrDefault(methodInfo, IgnoreReason.NotImplemented);
		}

		/// <summary>Retrieves an intrinsic <see cref="IgnoreReason"/> why a property should be ignored.</summary>
		/// <returns>An <see cref="IgnoreReason"/>.</returns>
		/// <param name="propertyInfo">The property being examined.</param>
		public static IgnoreReason GetIgnoreReason(this PropertyInfo propertyInfo)
		{
			return GetIgnoreReasonOrDefault(propertyInfo, IgnoreReason.Unspecified);
		}

		/// <summary>Retrieves the <see cref="IgnoreReason"/> a property was not traced.</summary>
		/// <returns>An <see cref="IgnoreReason"/>.</returns>
		/// <param name="propertyInfo">The property being examined.</param>
		public static IgnoreReason GetUntracedReason(this PropertyInfo propertyInfo)
		{
			return GetIgnoreReasonOrDefault(propertyInfo, IgnoreReason.NotImplemented);
		}

		private static IgnoreReason GetIgnorereasonOrDefault(MethodInfo methodInfo, IgnoreReason defaultReason)
		{
			if (methodInfo.IsStatic) return IgnoreReason.IsStatic;
			if (methodInfo.IsPrivate) return IgnoreReason.IsPrivate;
			if (methodInfo.IsFinal) return IgnoreReason.IsFinal;
			if (!methodInfo.IsVirtual) return IgnoreReason.NotVirtual;
			return defaultReason;
		}

		private static IgnoreReason GetIgnoreReasonOrDefault(PropertyInfo propertyInfo, IgnoreReason defaultReason)
		{
			if (Visibility.GetVisibility(propertyInfo).Equals("private")) return IgnoreReason.IsPrivate;
			var methodInfo = propertyInfo.CanRead
				? propertyInfo.GetMethod
				: propertyInfo.SetMethod;
			return GetIgnorereasonOrDefault(methodInfo, defaultReason);
		}
	}
}
