﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;

namespace TraceCodeGenerator.Support
{
	/// <summary>Extends AbstractClassGenerator by checking to see if the base class
	/// is a sealed class or not.  If the base is sealed, then an error will be added.</summary>
	public abstract class AbstractClassGeneratorNoSealed : AbstractClassGenerator
	{
		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.AbstractClassGeneratorNoSealed"/> class.</summary>
		/// <param name="baseType">The classType that the generated code will be baesd on.</param>
		/// <param name="flags">Visibility flags indicating which members should be gathered.</param>
		/// <param name="name_space">The new namespace being usede.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/> indicating what members should be skipped.</param>
		protected AbstractClassGeneratorNoSealed(Type baseType, BindingFlags flags, string name_space, SkipMap skipMap = null)
		: base(baseType, flags, name_space, skipMap)
		{
			if (HasError) { return; }

			if (_classInfo.ClassType.IsSealed)
			{
				string qualifiedName = TypeNames.GetQualifiedName(_classInfo.ClassType.FullName);
				Error = $"Sealed class '{qualifiedName}' may not be traced.";
			}
		}

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.AbstractClassGeneratorNoSealed"/> class.</summary>
		/// <param name="typeName">The string name of the type that the generated code will be baesd on.</param>
		/// <param name="flags">Visibility flags indicating which members should be gathered.</param>
		/// <param name="name_space">The new namespace being usede.</param>
		/// <param name="skipMap">Optional <see cref="SkipMap"/> indicating what members should be skipped.</param>
		protected AbstractClassGeneratorNoSealed(string typeName, BindingFlags flags, string name_space, SkipMap skipMap = null)
		: base(typeName, flags, name_space, skipMap)
		{
			if (HasError) { return; }

			if (_classInfo.ClassType.IsSealed)
			{
				string qualifiedName = TypeNames.GetQualifiedName(_classInfo.ClassType.FullName);
				Error = $"Sealed class '{qualifiedName}' may not be traced.";
				return;
			}
		}
	}
}
