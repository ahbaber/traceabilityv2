﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;

namespace TraceCodeGenerator.Support
{
	/// <summary>Contains any errors that were generated when comparing a base
	/// class and its trace class.</summary>
	public class VerificationErrorList
	{
		/// <summary>The name of the base class that was checked.</summary>
		public string BaseClassName { get; }

		/// <summary>The name of the trace class that was checked.</summary>
		public string TraceClassName { get; }

		/// <summary>A list containing the specific errors, if any.</summary>
		public List<VerificationError> Errors { get; }

		/// <summary>Initializes a new instance of the <see cref="VerificationErrorList"/> class.</summary>
		/// <param name="baseName">The name of the base class being compared.</param>
		/// <param name="traceName">The name of the trace class being compared.</param>
		public VerificationErrorList(string baseName, string traceName)
		{
			BaseClassName = baseName;
			TraceClassName = traceName;
			Errors = new List<VerificationError>();
		}
	}
}
