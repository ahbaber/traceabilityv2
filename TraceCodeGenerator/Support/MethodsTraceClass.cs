﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Reflection;
using System.Text;

namespace TraceCodeGenerator.Support
{
	/// <summary>Support class to help generate trace code for methods.</summary>
	public class MethodsTraceClass
	{
		/// <summary>Provides a singleton instance of <see cref="MethodsTraceClass"/>.</summary>
		/// <value>The singleton instance of <see cref="MethodsTraceClass"/>.</value>
		public static MethodsTraceClass Singleton => new MethodsTraceClass();

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.Methods"/> class.</summary>
		/// <param name="typeNames">Optional <see cref="TypeNames"/>, if one is not
		/// provided then the <see cref="TypeNames.Singleton"/> will be used.</param>
		/// <param name="parameters">Optional <see cref="Parameters"/> instance, if
		/// one is not provided then the <see cref="Parameters.Singleton"/> will be used.</param>
		public MethodsTraceClass(TypeNames typeNames = null, Parameters parameters = null)
		{
			_typeNames = typeNames ?? TypeNames.Singleton;
			_parameters = parameters ?? Parameters.Singleton;
		}

		/// <summary>Generates trace code for all methods of a class.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="classInfo">The functional class that is to be traced.</param>
		public StringBuilder Build(StringBuilder builder, ClassInfo classInfo)
		{
			foreach (var methodInfo in classInfo.Methods)
			{
				string baseName = _typeNames.GetConstructorName(classInfo.ClassType);
				if (ShouldBuild(methodInfo, out IgnoreReason? reason))
				{ Build(builder, methodInfo, baseName); }
				else if (reason.HasValue)
				{ builder.Append($"\n\t\t// skipped {reason} {methodInfo.GetParameterizedName()}\n"); }
			}
			return builder;
		}

		/// <summary>Builds the trace code for a property.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="methodInfo">The method being traced.</param>
		/// <param name="baseName">The name of the class that is to be traced.</param>
		public StringBuilder Build(StringBuilder builder, MethodInfo methodInfo, string baseName)
		{
			string visibility = Visibility.GetVisibility(methodInfo);
			string returnType = _typeNames.GetTypeName(methodInfo.ReturnType);
			string name = methodInfo.Name;

			// public override type methodname
			builder.Append($"\n\t\t{visibility} override {returnType} {name}");

			// (params)
			_parameters.BuildParameterList(builder, methodInfo.GetParameters());

			// \n\t\t{\n\t\t\t_tracer.NewNode(\"className.methodName\");
			builder.Append($"\n\t\t{{\n\t\t\t_tracer.FunctionStart(\"{baseName}.{name}\");");
			TraceParameters(builder, methodInfo);
			BuildBaseCall(builder, methodInfo, returnType);
			TraceOutParameters(builder, methodInfo);
			ReturnResults(builder, returnType);
			builder.Append("\n\t\t}\n");

			return builder;
		}

		private static bool ShouldBuild(MethodInfo methodInfo, out IgnoreReason? reason)
		{
			reason = methodInfo.GetIgnoreReason();
			switch (reason)
			{
				case IgnoreReason.IsPrivate:
				case IgnoreReason.IsFinal:
				case IgnoreReason.IsStatic:
				case IgnoreReason.NotVirtual:
					return false;
			}
			reason = null;
			return true;
		}

		private static void TraceParameters(StringBuilder builder, MethodInfo methodInfo)
		{
			foreach (var parameter in methodInfo.GetParameters())
			{
				if (!parameter.IsOut)
				{ builder.AppendFormat("\n\t\t\t_tracer.Parameter(\"{0}\", {0});", parameter.Name); }
			}
		}

		private static void TraceOutParameters(StringBuilder builder, MethodInfo methodInfo)
		{
			foreach (var parameter in methodInfo.GetParameters())
			{
				if (parameter.ParameterType.IsByRef)
				{ builder.AppendFormat("\n\t\t\t_tracer.Parameter(\"{0}_out\", {0});", parameter.Name); }
			}
		}

		private void BuildBaseCall(StringBuilder builder, MethodInfo methodInfo, string returnType)
		{
			builder.Append("\n\t\t\t");
			if (returnType != "void")
			{ builder.Append("var result = "); }

			builder.AppendFormat("base.{0}", methodInfo.Name);
			_parameters.BuildCalledParamterList(builder, methodInfo.GetParameters());
			builder.Append(";");
		}

		private static void ReturnResults(StringBuilder builder, string returnType)
		{
			builder.Append("\n\t\t\t");
			if (returnType.Equals("void"))
			{ builder.Append("_tracer.FunctionEnd();"); }
			else
			{ builder.Append("return _tracer.FunctionEnd(result);"); }
		}

		private readonly TypeNames _typeNames;
		private readonly Parameters _parameters;
	}
}
