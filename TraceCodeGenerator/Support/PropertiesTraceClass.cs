﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using System.Text;

namespace TraceCodeGenerator.Support
{
	/// <summary>Support class for generating compilable trace properties.</summary>
	public class PropertiesTraceClass
	{
		/// <summary>Provides a singleton instance of <see cref="PropertiesTraceClass"/>.</summary>
		/// <value>The singleton instance of <see cref="PropertiesTraceClass"/>.</value>
		public static PropertiesTraceClass Singleton => new PropertiesTraceClass();

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.Support.Properties"/> class.</summary>
		/// <param name="typeNames">Optional <see cref="TypeNames"/>, if
		/// one is not provided then the singleton will be used.</param>
		public PropertiesTraceClass(TypeNames typeNames = null)
		{
			_typeNames = typeNames ?? TypeNames.Singleton;
		}

		/// <summary>Builds all properties for a class.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="classInfo">The functional class that is to be traced.</param>
		public StringBuilder Build(StringBuilder builder, ClassInfo classInfo)
		{
			foreach (var propertyInfo in  classInfo.Properties)
			{
				if (ShouldBuild(propertyInfo, out IgnoreReason? reason))
				{
					string baseName = _typeNames.GetConstructorName(classInfo.ClassType);
					if (propertyInfo.Name == "Item")
					{ BuildIndexer(builder, propertyInfo, baseName); }
					else
					{ BuildProperty(builder, propertyInfo, baseName); }
				}
				else if (reason.HasValue)
				{
					builder.Append($"\n\t\t// skipped {reason} ");
					_typeNames.GetTypeName(builder, propertyInfo.PropertyType);
					builder.Append($" {propertyInfo.Name}\n");
				}
			}
			return builder;
		}

		/// <summary>Builds a single property.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="propertyInfo">The property being traced.</param>
		/// <param name="baseType">The functional class that is to be traced.</param>
		public StringBuilder Build(StringBuilder builder, PropertyInfo propertyInfo, Type baseType)
		{
			string baseName = _typeNames.GetConstructorName(baseType);
			if (propertyInfo.Name == "Item")
			{ BuildIndexer(builder, propertyInfo, baseName); }
			else
			{ BuildProperty(builder, propertyInfo, baseName); }
			return builder;
		}

		private void BuildProperty(StringBuilder builder, PropertyInfo propertyInfo, string baseName)
		{
			// public override type name\n\t\t{{
			string visibility = Visibility.GetVisibility(propertyInfo);
			builder.Append($"\n\t\t{visibility} override ");
			_typeNames.GetTypeName(builder, propertyInfo.PropertyType);
			builder.Append($" {propertyInfo.Name}\n\t\t{{");

			if (propertyInfo.CanRead)
			{
				string getVisibility = Visibility.GetVisibility(propertyInfo.GetMethod);
				getVisibility = getVisibility.Equals(visibility) ? "" : getVisibility + " ";
				// \n\t\t\tget { return _tracer.GetProperty("className.propName", base.propName); }
				builder.AppendFormat("\n\t\t\t{0}get {{ return _tracer.GetProperty(\"{1}.{2}\", base.{2}); }}",
					getVisibility,
					baseName,
					propertyInfo.Name);
			}
			if (propertyInfo.CanWrite)
			{
				string setVisibility = Visibility.GetVisibility(propertyInfo.SetMethod);
				setVisibility = setVisibility.Equals(visibility) ? "" : setVisibility + " ";
				// \n\t\t\tset { base.propName = _tracer.SetProperty("className.propName", value); }
				builder.AppendFormat("\n\t\t\t{0}set {{ base.{2} = _tracer.SetProperty(\"{1}.{2}\", value); }}",
					setVisibility,
					baseName,
					propertyInfo.Name);
			}
			builder.Append("\n\t\t}\n");
		}

		private void BuildIndexer(StringBuilder builder, PropertyInfo propertyInfo, string baseName)
		{
			// public override type this[int i]\n\t\t{{
			string visibility = Visibility.GetVisibility(propertyInfo);
			builder.Append($"\n\t\t{visibility} override ");
			_typeNames.GetTypeName(builder, propertyInfo.PropertyType);
			builder.Append(" this[int i]\n\t\t{");

			if (propertyInfo.CanRead)
			{
				// \n\t\t\tget { return _tracer.GetIndexer("className", i, base[i]); }
				builder.AppendFormat("\n\t\t\tget {{ return _tracer.GetIndexer(\"{0}\", i, base[i]); }}",
				   baseName);
			}
			if (propertyInfo.CanWrite)
			{
				// \n\t\t\tset { base[i] = _tracer.SetIndexer("className", i, value); }
				builder.AppendFormat("\n\t\t\tset {{ base[i] = _tracer.SetIndexer(\"{0}\", i, value); }}",
					baseName);
			}
			builder.Append("\n\t\t}\n");
		}

		private bool ShouldBuild(PropertyInfo propertyInfo, out IgnoreReason? reason)
		{
			reason = propertyInfo.GetIgnoreReason();
			switch (reason)
			{
				case IgnoreReason.IsPrivate:
				case IgnoreReason.IsFinal:
				case IgnoreReason.IsStatic:
				case IgnoreReason.NotVirtual:
					return false;
			}
			reason = null;
			return true;
		}

		private readonly TypeNames _typeNames;
	}
}
