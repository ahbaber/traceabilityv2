﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Reflection;
using System.Text;

namespace TraceCodeGenerator.Support
{
	public class MethodsTraceFactory
	{
		public MethodsTraceFactory(Parameters parameters = null, TypeNames typeNames = null)
		{
			_parameters = parameters ?? Parameters.Singleton;
			_typeNames = typeNames ?? TypeNames.Singleton;
		}

		public static MethodsTraceFactory Singleton => new MethodsTraceFactory();

		/// <summary>Searches for all public virtual named "Construct" and overrides
		/// them with a new method that returns <paramref name="traceClass"/>
		/// instead.</summary>
		/// <returns>The <see cref="StringBuilder"/> passed in by <paramref name="builder"/>.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will receive the formatted text.</param>
		/// <param name="baseFactory">A factory for constructing a functional layer type.</param>
		/// <param name="traceClass">The class that will be returned.</param>
		public StringBuilder Build(StringBuilder builder, ClassInfo baseFactory, Type traceClass)
		{
			foreach (var methodInfo in baseFactory.Methods)
			{
				if (methodInfo.Name.Equals("Construct"))
				{
					if (methodInfo.IsVirtual)
					{
						Build(builder, baseFactory, methodInfo, traceClass);
					}
					else
					{ builder.Append($"\n\t\t// skipped NotVirtual {methodInfo.GetParameterizedName()}\n"); }
				}
				else
				{ builder.Append($"\n\t\t// skipped not named Construct: {methodInfo.GetParameterizedName()}\n"); }
			}
			return builder;
		}

		/// <summary>
		/// Builds the trace factory.
		/// </summary>
		/// <returns>The trace factory.</returns>
		/// <param name="builder">Builder.</param>
		/// <param name="methodInfo">Method info.</param>
		/// <param name="traceClass">Trace class.</param>
		public StringBuilder Build(StringBuilder builder, ClassInfo baseFactory, MethodInfo methodInfo, Type traceClass)
		{
			string visibility = Visibility.GetVisibility(methodInfo);
			var returnType = methodInfo.ReturnType;
			string returnName= _typeNames.GetTypeName(returnType);

			// public overide returnType Construct
			builder.Append($"\n\t\t{visibility} override {returnName} {methodInfo.Name}");

			if (methodInfo.IsGenericMethod && !baseFactory.ClassType.IsGenericType)
			{ _typeNames.GetGenericParameters(builder, returnType); }

			// (params)
			_parameters.BuildParameterList(builder, methodInfo.GetParameters());
			builder.Append("\n\t\t{");
			// return new TraceType(_tracer, ...)

			builder.Append($"\n\t\t\treturn new {_typeNames.GetTypeName(traceClass)}");
			_parameters.BuildCalledParamterList(builder, methodInfo.GetParameters(), true)
				.Append(";");

			builder.Append("\n\t\t}\n");
			return builder;
		}

		private readonly Parameters _parameters;
		private readonly TypeNames _typeNames;
	}
}
