﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;
using System.Reflection;
using TraceCodeGenerator.Support;
using MethodMap = System.Collections.Generic.Dictionary<string, System.Reflection.MethodInfo>;

namespace TraceCodeGenerator
{
	/// <summary>Used to comapre two classes, and verify that the trace class is a proper representation of the base class.</summary>
	public static class Verify
	{
		private static readonly BindingFlags _baseFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy;
		private static readonly BindingFlags _traceFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.DeclaredOnly;
		private static readonly SkipMap _emptyIgnore = new SkipMap();

		/// <summary>Compares two classes, the base class, and the trace child class.  Properties and methods in each
		/// will be compared.  If any non-private properties or methods appear in the base class, then they are expected
		/// to appear in the trace class as well.  Each missing property or function will be listed in the output, with a
		/// reason why it is not present.
		/// By including the name of the property or method in the ignore dictionary, we may skip the verification.	 The ignore
		/// dictionary should be considered an explicit list of non-traced members.
		/// </summary>
		/// <returns>A list of non-traced members.</returns>
		/// <param name="baseClass">The base class to be traced.</param>
		/// <param name="traceClass">The trace version of the base class.</param>
		/// <param name="ignore">Optional <see cref="SkipMap"/> members to ignore.</param>
		public static VerificationErrorList VerifyTraceInheritence(Type baseClass, Type traceClass, SkipMap ignore = null)
		{
			ignore = ignore ?? _emptyIgnore;

			var errors = new VerificationErrorList(baseClass.Name, traceClass.Name);

			var propertyMap = VerifyProperties(baseClass, traceClass, ignore, errors);
			VerifyMethods(baseClass, traceClass, ignore, errors, propertyMap);

			return errors;
		}

		private static MethodMap VerifyProperties(Type baseClass, Type traceClass, SkipMap ignore, VerificationErrorList errors)
		{
			var propertyMap = new MethodMap();

			var traced = new Dictionary<string, PropertyInfo>();
			foreach (var traceProperty in traceClass.GetProperties(_traceFlags))
			{
				traced.Add(traceProperty.Name, traceProperty);
			}

			// make sure each base class property appears in the trace class unless explictly noted as ignored
			foreach (var baseProperty in baseClass.GetProperties(_baseFlags))
			{
				AddPropertyToMap(propertyMap, baseProperty);

				if (!ignore.ShouldSkip(baseProperty))
				{
					if (!traced.ContainsKey(baseProperty.Name))
					{
						errors.Errors.Add(new VerificationError
						{
							MemberName = baseProperty.Name,
							Reason = baseProperty.GetUntracedReason(),
						});
					}
				}
				traced.Remove(baseProperty.Name);
			}

			foreach (var tracePair in traced)
			{
				AddPropertyToMap(propertyMap, tracePair.Value);
				if (!ignore.ShouldSkip(tracePair.Value))
				{
					errors.Errors.Add(new VerificationError
					{
						MemberName = tracePair.Key,
						Reason = IgnoreReason.Unspecified,
					});
				}
			}

			return propertyMap;
		}

		private static void AddPropertyToMap(MethodMap propertyMap, PropertyInfo propertyInfo)
		{
			if (propertyInfo.CanRead)
			{
				propertyMap.Add(propertyInfo.GetMethod.Name, propertyInfo.GetMethod);
				propertyMap.Add(propertyInfo.GetMethod.GetParameterizedName(), propertyInfo.GetMethod);
			}
			if (propertyInfo.CanWrite)
			{
				propertyMap.Add(propertyInfo.SetMethod.Name, propertyInfo.SetMethod);
				propertyMap.Add(propertyInfo.SetMethod.GetParameterizedName(), propertyInfo.SetMethod);
			}
		}

		private static void VerifyMethods(Type baseClass, Type traceClass, SkipMap ignore, VerificationErrorList errors, MethodMap propertyMap)
		{
			var traceMap = new MethodMap();
			foreach (var traceMethod in traceClass.GetMethods(_traceFlags))
			{
				string parameterizedName = traceMethod.GetParameterizedName();
				if (!propertyMap.ContainsKey(parameterizedName))
				{ traceMap.Add(parameterizedName, traceMethod); }
			}

			foreach (var baseMethod in baseClass.GetMethods(_baseFlags))
			{
				string paramatizedName = baseMethod.GetParameterizedName();
				if (!(ignore.ShouldSkip(baseMethod) || propertyMap.ContainsKey(paramatizedName)))
				{
					if (!traceMap.ContainsKey(paramatizedName))
					{
						errors.Errors.Add(new VerificationError
						{
							MemberName = paramatizedName,
							Reason = baseMethod.GetUntracedReason(),
						});
					}
				}
				traceMap.Remove(paramatizedName);
			}

			//make sure all trace class methods appear in the base class
			foreach (var tracePair in traceMap)
			{
				if (!ignore.ShouldSkip(tracePair.Value))
				{
					errors.Errors.Add(new VerificationError
					{
						MemberName = tracePair.Key,
						Reason = IgnoreReason.Unspecified,
					});
				}
			}
		}
	}
}
