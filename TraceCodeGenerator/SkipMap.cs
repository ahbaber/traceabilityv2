﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TraceCodeGenerator.Support;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace TraceCodeGenerator
{
	/// <summary>Used to track which properties and methods should be skipped during trace code generation.</summary>
	public class SkipMap
	{
		private static readonly BindingFlags _skipFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.FlattenHierarchy;

		/// <summary>Initializes a new instance of the <see cref="T:TraceCodeGenerator.SkipMap"/> class.
		/// During construction, the following default values are added:
		///		Dispose
		///		Finalize
		///		MemberwiseClone
		/// The following methods will be added from <see cref="System.Object"/>
		///		ToString_System.String
		///		Equals_System.Boolean_System.Object
		///		Equals_System.Boolean_System.Object_System.Object
		///		ReferenceEquals_System.Object_System.Object
		///		GetType_System.Type
		///		GetHashCode_System.Int32
		///</summary>
		public SkipMap()
		{
			Add(typeof(object));
			Add("Dispose", false);
			Add("Finalize", false);
			Add("MemberwiseClone", false);
		}

		/// <summary>Adds a property or method name to the <see cref=" SkipMap"/>.</summary>
		/// <returns>The current instance of <see cref="SkipMap"/>.</returns>
		/// <param name="name">Name.</param>
		/// <param name="isParameterized">If this is set to false, then any method matching the name will be skipped, regardless of overloading.
		/// If this is set to true, then only methods with the same paramatization will be skipped. <seealso cref="MethodsStatic.GetAllParameterizedNames(Type)"/>.
		/// This setting has no effect on properties, as they do not have overloads.</param>
		public SkipMap Add(string name, bool isParameterized)
		{
			_skipMap.Add(name, isParameterized);
			return this;
		}

		/// <summary>Adds all properties and methods of a class to the <see cref="SkipMap"/>.</summary>
		/// <returns>The current instance of <see cref="SkipMap"/>.</returns>
		/// <param name="skipType">The <see cref="Type"/> whose members will be added to the <see cref="SkipMap"/>.</param>
		public SkipMap Add(Type skipType)
		{
			foreach (var propertyInfo in skipType.GetProperties(_skipFlags))
			{
				if (!_skipMap.ContainsKey(propertyInfo.Name))
				{ _skipMap.Add(propertyInfo.Name, false); }
			}

			foreach (var methodInfo in skipType.GetMethods(_skipFlags))
			{
				string parameterizedName = methodInfo.GetParameterizedName();
				if (!_skipMap.ContainsKey(parameterizedName))
				{ _skipMap.Add(parameterizedName, true); }
			}
			return this;
		}

		/// <summary>Checks to see if a specific property name is in the <see cref="SkipMap"/>.</summary>
		/// <returns><c>true</c>, if the property should be skipped, <c>false</c> otherwise.</returns>
		/// <param name="propertyInfo">The property being checked.</param>
		public bool ShouldSkip(PropertyInfo propertyInfo)
		{
			return _skipMap.ContainsKey(propertyInfo.Name);
		}

		/// <summary>Checks to see if a specific method name is in the <see cref="SkipMap"/>.</summary>
		/// <returns><c>true</c>, if the method should be skipped, <c>false</c> otherwise.</returns>
		/// <param name="methodInfo">The method being checked.</param>
		public bool ShouldSkip(MethodInfo methodInfo)
		{
			if (_skipMap.TryGetValue(methodInfo.Name, out bool shouldSkip))
			{
				if (!shouldSkip) // does not require paramatized name
				{
					return true;
				}
			}
			if (_skipMap.TryGetValue(methodInfo.GetParameterizedName(), out shouldSkip))
			{
				if (shouldSkip) // requires paramatized name
				{
					return true;
				}
			}
			return false;
		}

		private Dictionary<string, bool> _skipMap = new Dictionary<string, bool>();
	}
}
