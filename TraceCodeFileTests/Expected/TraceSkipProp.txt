﻿// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceSkipProp : TraceCodeFileTests.Functional.SkippingTestClass
	{
		public TraceSkipProp(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override void Method1()
		{
			_tracer.FunctionStart("SkippingTestClass.Method1");
			base.Method1();
			_tracer.FunctionEnd();
		}

		public override void Method1(int i)
		{
			_tracer.FunctionStart("SkippingTestClass.Method1");
			_tracer.Parameter("i", i);
			base.Method1(i);
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
