// Auto generated file
// Any modification to this file may be lost
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceEmptyClass : TraceCodeFileTests.Functional.EmptyClass
	{
		public TraceEmptyClass(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}
}
