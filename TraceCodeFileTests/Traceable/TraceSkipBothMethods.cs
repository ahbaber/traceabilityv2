// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceSkipBothMethods : TraceCodeFileTests.Functional.SkippingTestClass
	{
		public TraceSkipBothMethods(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override int IntProp
		{
			get { return _tracer.GetProperty("SkippingTestClass.IntProp", base.IntProp); }
			set { base.IntProp = _tracer.SetProperty("SkippingTestClass.IntProp", value); }
		}

		private readonly ITracer _tracer;
	}
}
