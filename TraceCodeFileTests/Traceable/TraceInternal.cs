// Auto generated file
// Any modification to this file may be lost
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	internal class TraceInternal : TraceCodeFileTests.Functional.EmptyClass
	{
		public TraceInternal(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}
}
