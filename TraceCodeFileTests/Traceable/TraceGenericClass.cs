// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceGenericClass<T> : TraceCodeFileTests.Functional.GenericClass<T>
	{
		public TraceGenericClass(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override T GenericProperty
		{
			get { return _tracer.GetProperty("GenericClass.GenericProperty", base.GenericProperty); }
			set { base.GenericProperty = _tracer.SetProperty("GenericClass.GenericProperty", value); }
		}

		public override T GenericReturn()
		{
			_tracer.FunctionStart("GenericClass.GenericReturn");
			var result = base.GenericReturn();
			return _tracer.FunctionEnd(result);
		}

		public override void GenericArgument(T arg)
		{
			_tracer.FunctionStart("GenericClass.GenericArgument");
			_tracer.Parameter("arg", arg);
			base.GenericArgument(arg);
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
