// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceSkipSingleMethod : TraceCodeFileTests.Functional.SkippingTestClass
	{
		public TraceSkipSingleMethod(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override int IntProp
		{
			get { return _tracer.GetProperty("SkippingTestClass.IntProp", base.IntProp); }
			set { base.IntProp = _tracer.SetProperty("SkippingTestClass.IntProp", value); }
		}

		public override void Method1()
		{
			_tracer.FunctionStart("SkippingTestClass.Method1");
			base.Method1();
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
