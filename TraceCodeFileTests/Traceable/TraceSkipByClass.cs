// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceSkipByClass : TraceCodeFileTests.Functional.SkippingTestClass
	{
		public TraceSkipByClass(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override void Method1(int i)
		{
			_tracer.FunctionStart("SkippingTestClass.Method1");
			_tracer.Parameter("i", i);
			base.Method1(i);
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
