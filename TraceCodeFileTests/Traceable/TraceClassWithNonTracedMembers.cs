// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceClassWithNonTracedMembers : TraceCodeFileTests.Functional.ClassWithNonTracedMembers
	{
		public TraceClassWithNonTracedMembers(ITracer tracer)
		{
			_tracer = tracer;
		}

		// skipped private ClassWithNonTracedMembers(int i)

		// skipped NotVirtual int PublicProperty

		// skipped IsPrivate string Property

		// skipped NotVirtual PublicMethod_System.Void

		// skipped IsPrivate PrivateMethod_System.Void

		private readonly ITracer _tracer;
	}
}
