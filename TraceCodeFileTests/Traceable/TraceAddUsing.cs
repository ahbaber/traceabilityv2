// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceAddUsing : TraceCodeFileTests.Functional.EmptyClass
	{
		public TraceAddUsing(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}
}
