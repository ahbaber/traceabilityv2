// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Traceable
{
	public class TraceGenericInt : TraceCodeFileTests.Functional.GenericClass<int>
	{
		public TraceGenericInt(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override int GenericProperty
		{
			get { return _tracer.GetProperty("GenericClass_int.GenericProperty", base.GenericProperty); }
			set { base.GenericProperty = _tracer.SetProperty("GenericClass_int.GenericProperty", value); }
		}

		public override int GenericReturn()
		{
			_tracer.FunctionStart("GenericClass_int.GenericReturn");
			var result = base.GenericReturn();
			return _tracer.FunctionEnd(result);
		}

		public override void GenericArgument(int arg)
		{
			_tracer.FunctionStart("GenericClass_int.GenericArgument");
			_tracer.Parameter("arg", arg);
			base.GenericArgument(arg);
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
