// Auto generated file
// Any modification to this file may be lost
using System;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Factories
{
	public class GenericIntFactory
	{
		public virtual GenericClass<int> Construct()
		{
			return new GenericClass<int>();
		}
	}
}
