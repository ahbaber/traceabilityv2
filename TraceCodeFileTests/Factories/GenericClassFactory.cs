// Auto generated file
// Any modification to this file may be lost
using System;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Factories
{
	public class GenericClassFactory<T>
	{
		public virtual GenericClass<T> Construct()
		{
			return new GenericClass<T>();
		}
	}
}
