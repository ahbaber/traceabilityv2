// Auto generated file
// Any modification to this file may be lost
using System;
using TraceCodeFileTests.Functional;

namespace TraceCodeFileTests.Factories
{
	public class GenericNamedFactory
	{
		public virtual GenericClass<T> Construct<T>()
		{
			return new GenericClass<T>();
		}
	}
}
