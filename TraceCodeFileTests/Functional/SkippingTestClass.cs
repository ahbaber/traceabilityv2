﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace TraceCodeFileTests.Functional
{
	public class SkippingTestClass
	{
		public virtual int IntProp { get; set; }
		public virtual void Method1() { }
		public virtual void Method1(int i) { }
	}
}