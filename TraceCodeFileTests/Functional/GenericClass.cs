﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace TraceCodeFileTests.Functional
{
	public class GenericClass<T>
	{
		public GenericClass()
		{ }
		
		public virtual T GenericProperty { get; set; }
		public virtual T GenericReturn() { return GenericProperty; }
		public virtual void GenericArgument(T arg) { GenericProperty = arg; }
	}
}
