﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace TraceCodeFileTests.Functional
{
	public class ClassWithNonTracedMembers
	{
		public ClassWithNonTracedMembers() { }
		private ClassWithNonTracedMembers(int i) { }

		public int PublicProperty { get; set; }
		private string Property { get; set; }
		public static int StaticProperty { get; set; }


		public void PublicMethod() { }
		private void PrivateMethod() { }
		public static int StaticMethod(char c, string s) { return 0; }
	}
}
