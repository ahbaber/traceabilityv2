﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace TraceCodeFileTests.Functional
{
	public class SkipClass
	{
		public int IntProp { get; set; }
		public void Method1() { }
		public void OtherMethod() { }
	}
}
