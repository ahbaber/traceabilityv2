﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using TraceCodeFileTests.Factories;
using TraceCodeFileTests.Functional;
using TraceCodeFileTests.Traceable;
using TraceCodeGenerator;

namespace TraceCodeFileTests
{
	class Program
	{
		static void Main()
		{
			GenerateFunctionalFactories();
			GenerateTraceClasses();
			GenerateTraceFactories();
			Console.WriteLine("Done");
		}

		static void GenerateFunctionalFactories()
		{
			string ns = "TraceCodeFileTests.Factories";
			IClassGenerator codeGen = new FactoryGenerator(typeof(EmptyClass), ns);
			WriteFile(codeGen);

			codeGen = new FactoryGenerator(typeof(GenericClass<>), ns);
			WriteFile(codeGen);

			codeGen = new FactoryGenerator(typeof(GenericClass<>), ns, "GenericNamedFactory")
				.PlaceGenericParametersOnConstruct();
			WriteFile(codeGen);

			codeGen = new FactoryGenerator(typeof(GenericClass<int>), ns, "GenericIntFactory");
			WriteFile(codeGen);
		}

		static void GenerateTraceClasses()
		{
			string ns = "TraceCodeFileTests.Traceable";
			IClassGenerator codeGen = new TraceClassGenerator(typeof(EmptyClass), ns);
			WriteFile(codeGen);

			codeGen = new TraceClassGenerator(typeof(EmptyClass), ns, "TracePartial")
				.MakePartial();
			WriteFile(codeGen);

			codeGen = new TraceClassGenerator(typeof(EmptyClass), ns, "TraceAddUsing")
				.AddUsing("System");
			WriteFile(codeGen);

			codeGen = new TraceClassGenerator(typeof(EmptyClass), ns, "TraceInternal")
				.MakeInternal();
			WriteFile(codeGen);

			var skipMap = new SkipMap().Add("IntProp", false);
			codeGen = new TraceClassGenerator(typeof(SkippingTestClass), ns, "TraceSkipProp", skipMap);
			WriteFile(codeGen);

			skipMap = new SkipMap().Add("Method1", false);
			codeGen = new TraceClassGenerator(typeof(SkippingTestClass), ns, "TraceSkipBothMethods", skipMap);
			WriteFile(codeGen);

			skipMap = new SkipMap().Add("Method1_System.Void_System.Int32", true);
			codeGen = new TraceClassGenerator(typeof(SkippingTestClass), ns, "TraceSkipSingleMethod", skipMap);
			WriteFile(codeGen);

			skipMap = new SkipMap().Add(typeof(SkipClass));
			codeGen = new TraceClassGenerator(typeof(SkippingTestClass), ns, "TraceSkipByClass", skipMap);
			WriteFile(codeGen);

			codeGen = new TraceClassGenerator(typeof(ClassWithNonTracedMembers), ns);
			WriteFile(codeGen);

			codeGen = new TraceClassGenerator(typeof(GenericClass<>), ns);
			WriteFile(codeGen);

			codeGen = new TraceClassGenerator(typeof(GenericClass<int>), ns, "TraceGenericInt");
			WriteFile(codeGen);
		}

		static void GenerateTraceFactories()
		{
			string ns = "TraceCodeFileTests.TraceFactories";
			IClassGenerator codeGen = new TraceFactoryGenerator(typeof(EmptyClassFactory), typeof(TraceEmptyClass), ns)
				.AddUsing("TraceCodeFileTests.Functional");
			WriteFile(codeGen);

			codeGen = new TraceFactoryGenerator(typeof(GenericClassFactory<>), typeof(TraceGenericClass<>), ns)
				.AddUsing("TraceCodeFileTests.Functional");
			WriteFile(codeGen);

			codeGen = new TraceFactoryGenerator(typeof(GenericNamedFactory), typeof(TraceGenericClass<>), ns)
				.AddUsing("TraceCodeFileTests.Functional");
			WriteFile(codeGen);

			codeGen = new TraceFactoryGenerator(typeof(GenericIntFactory), typeof(TraceGenericInt), ns)
				.AddUsing("TraceCodeFileTests.Functional");
			WriteFile(codeGen);
		}

		private static void WriteFile(IClassGenerator classGen)
		{
			Console.WriteLine(_fileWriter.CreateFile(classGen));
		}

		private static FileWriter _fileWriter = new FileWriter();
	}
}
