// Auto generated file
// Any modification to this file may be lost
using Traceability;
using TraceCodeFileTests.Factories;
using TraceCodeFileTests.Functional;
using TraceCodeFileTests.Traceable;

namespace TraceCodeFileTests.TraceFactories
{
	public class TraceGenericNamedFactory : TraceCodeFileTests.Factories.GenericNamedFactory
	{
		public TraceGenericNamedFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override GenericClass<T> Construct<T>()
		{
			return new TraceGenericClass<T>(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
