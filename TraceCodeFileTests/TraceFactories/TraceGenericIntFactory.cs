// Auto generated file
// Any modification to this file may be lost
using System;
using Traceability;
using TraceCodeFileTests.Factories;
using TraceCodeFileTests.Functional;
using TraceCodeFileTests.Traceable;

namespace TraceCodeFileTests.TraceFactories
{
	public class TraceGenericIntFactory : TraceCodeFileTests.Factories.GenericIntFactory
	{
		public TraceGenericIntFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override GenericClass<int> Construct()
		{
			return new TraceGenericInt(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
