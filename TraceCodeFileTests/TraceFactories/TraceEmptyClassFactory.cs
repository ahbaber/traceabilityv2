// Auto generated file
// Any modification to this file may be lost
using Traceability;
using TraceCodeFileTests.Factories;
using TraceCodeFileTests.Functional;
using TraceCodeFileTests.Traceable;

namespace TraceCodeFileTests.TraceFactories
{
	public class TraceEmptyClassFactory : TraceCodeFileTests.Factories.EmptyClassFactory
	{
		public TraceEmptyClassFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override EmptyClass Construct()
		{
			return new TraceEmptyClass(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
