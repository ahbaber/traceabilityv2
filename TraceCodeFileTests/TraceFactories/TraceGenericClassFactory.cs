// Auto generated file
// Any modification to this file may be lost
using Traceability;
using TraceCodeFileTests.Factories;
using TraceCodeFileTests.Functional;
using TraceCodeFileTests.Traceable;

namespace TraceCodeFileTests.TraceFactories
{
	public class TraceGenericClassFactory<T> : TraceCodeFileTests.Factories.GenericClassFactory<T>
	{
		public TraceGenericClassFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override GenericClass<T> Construct()
		{
			return new TraceGenericClass<T>(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
