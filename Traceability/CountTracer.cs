﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using CountMap = System.Collections.Generic.Dictionary<string, int>;

namespace Traceability
{
	/// <summary>This tracer class records the number of times that various objects
	/// are touched; i.e. property reads and writes, as well as function counts.
	/// The purpose of this is to gather general counts to see how changes to the
	/// code impact how much an object is actually interacted with.</summary>
	public class CountTracer : ITracer
	{
		/// <summary>Retrieves a key value map.  The name of the function called
		/// is the key, and the number of times the function was called is the
		/// value.</summary>
		/// <value>A map of function call counts.</value>
		public CountMap Calls => _calls;

		/// <summary>Retreives the current depth of the function hierarchy.</summary>
		/// <value>The current depth.</value>
		public int Depth => _depth;

		/// <summary>Retrieves the maximum depth reached by the function hierarchy.</summary>
		/// <value>The max depth.</value>
		public int MaxDepth => _maxDepth;

		/// <summary>Retrieves a key value map of property reads.  The name of
		/// the property is the key, and the value is the number of times the 
		/// property was read.</summary>
		/// <value>A map of property read counts.</value>
		public CountMap Reads => _reads;

		/// <summary>Retrieves a key value map of property writes.  The name of
		/// the property is the key, and the value is the number of times the 
		/// property was written to.</summary>
		/// <value>A map of property write counts.</value>
		public CountMap Writes => _writes;

		/// <summary>Increases the function's call count,
		/// increases the current depth,
		/// increases the mad dpeth if applicable.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="classAndFunctionName">The name of the function being called.</param>
		public ITracer FunctionStart(string classAndFunctionName)
		{
			_calls.TryGetValue(classAndFunctionName, out int depth);
			_calls[classAndFunctionName] = depth + 1;

			++_depth;
			_maxDepth = Math.Max(_maxDepth, _depth);

			return this;
		}

		/// <summary>Reduces the current depth of the function hierarchy.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		public ITracer FunctionEnd()
		{
			--_depth;
			return this;
		}

		/// <summary>Reduces the current depth of the function hierarchy.</summary>
		/// <returns>The value passed in as <paramref name="result"/>.</returns>
		/// <param name="result">Passed through.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T FunctionEnd<T>(T result)
		{
			--_depth;
			return result;
		}

		/// <summary>Increases a property's read count.</summary>
		/// <returns>The value retrieved from the property.</returns>
		/// <param name="classAndPropertyName">The name of the property.</param>
		/// <param name="value">The value of the property.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T GetProperty<T>(string classAndPropertyName, T value)
		{
			_reads.TryGetValue(classAndPropertyName, out int count);
			_reads[classAndPropertyName] = count + 1;
			return value;
		}

		/// <summary>Increases a property's write count.</summary>
		/// <returns>The value written tp the property.</returns>
		/// <param name="classAndPropertyName">The name of the property.</param>
		/// <param name="value">The value of the property.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T SetProperty<T>(string classAndPropertyName, T value)
		{
			_writes.TryGetValue(classAndPropertyName, out int count);
			_writes[classAndPropertyName] = count + 1;
			return value;
		}

		/// <summary>Increases the indexer's read count.</summary>
		/// <returns>The value retrieved from the indexer.</returns>
		/// <param name="className">The name of the class.</param>
		/// <param name="index">The index of the item retrieved.</param>
		/// <param name="value">The value retrieved.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T GetIndexer<T>(string className, int index, T value)
		{
			className = $"{className}_indexer";
			_reads.TryGetValue(className, out int reads);
			_reads[className] = reads + 1;
			return value;
		}

		/// <summary>Increases the indexer's write count.</summary>
		/// <returns>The value written to the indexer.</returns>
		/// <param name="className">The name of the class.</param>
		/// <param name="index">The index of the item written.</param>
		/// <param name="value">The value written.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T SetIndexer<T>(string className, int index, T value)
		{
			className = $"{className}_indexer";
			_writes.TryGetValue(className, out int reads);
			_writes[className] = reads + 1;
			return value;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">Not used.</param>
		/// <param name="value">Not used.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public ITracer Parameter<T>(string parameterName, T value)
		{
			return this;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">Not used.</param>
		/// <param name="args">Not used.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public ITracer Parameter<T>(string parameterName, T[] args)
		{
			return this;
		}

		private readonly CountMap _calls = new CountMap();
		private readonly CountMap _reads = new CountMap();
		private readonly CountMap _writes = new CountMap();
		private int _depth;
		private int _maxDepth;
	}
}
