﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;

namespace Traceability
{
	/// <summary>Contains all of the trace data associated with a single function call.</summary>
	public class TraceNode
	{
		/// <summary>
		/// Gets or sets the tag.
		/// </summary>
		/// <value>The tag identifying the node.</value>
		public string Tag { get; }

		/// <summary>A strng representation of the value returned
		/// from a function call.</summary>
		/// <value>The returned value, if present.</value>
		public string Value { get; set; }

		/// <summary>Reference to the parent of the current <see cref="TraceNode"/>.</summary>
		/// <value>The parent of the current node.</value>
		public TraceNode Parent { get; }

		/// <summary>Checks to see if the <see cref="TraceNode"/> has had
		/// any parameter data added to it.</summary>
		/// <value><c>true</c> if parameter data is present; otherwise, <c>false</c>.</value>
		public bool HasParameters => _parameters != null && _parameters.Count > 0;

		/// <summary>Returns the List of parameters.</summary>
		/// <value>The List of parameters.</value>
		public Dictionary<string, string> Parameters => _parameters ?? GetParameters();

		/// <summary>Checks to see if the <see cref="TraceNode"/> has
		/// any child nodes.</summary>
		/// <value><c>true</c> if child nodes are present; otherwise, <c>false</c>.</value>
		public bool HasChildren => _children != null && _children.Count > 0;

		/// <summary>Returns the List of child <see cref="TraceNode"/>s.</summary>
		/// <value>The <see cref="List{TraceNode}"/> of children.</value>
		public List<TraceNode> Children => _children ?? GetChildren();

		/// <summary>Initializes a new instance of the <see cref="T:Traceability.TraceNode"/> class.</summary>
		/// <param name="tag">The tag identifying the trace node.  This field is required and may not be empty/<see langword="null"/>.</param>
		/// <param name="parent">Optional filed indiating the parent of the <see cref="TraceNode"/> being constructed.</param>
		public TraceNode(string tag, TraceNode parent = null)
		{
			if (tag == null) throw new ArgumentNullException(nameof(tag));
			if (tag.Length == 0) throw new ArgumentException($"'{nameof(tag)}' may not be an empty string");

			Tag = tag;
			Parent = parent;
		}

		/// <summary>Creates a new child <see cref="TraceNode"/> and returns it.</summary>
		/// <returns>The new child <see cref="TraceNode"/>.</returns>
		/// <param name="tag">The name of the child node.</param>
		public TraceNode CreateChild(string tag)
		{
			var child = new TraceNode(tag, this);
			Children.Add(child);
			return child;
		}

		/// <summary>Adds parameter information to the <see cref="TraceNode"/>.</summary>
		/// <returns>The current instance of the <see cref="TraceNode"/>.</returns>
		/// <param name="tag">The name of the parameter.</param>
		/// <param name="value">The value of the parameter.</param>
		/// <typeparam name="T">Any type that supports .ToString().</typeparam>
		public TraceNode AddParameter<T>(string tag, T value)
		{
			string data = value == null ? "null" : value.ToString();
			Parameters.Add(tag, data);
			return this;
		}

		private Dictionary<string, string> _parameters;
		private List<TraceNode> _children;

		private Dictionary<string, string> GetParameters()
		{
			_parameters = new Dictionary<string, string>();
			return _parameters;
		}

		private List<TraceNode> GetChildren()
		{
			_children = new List<TraceNode>();
			return _children;
		}
	}
}
