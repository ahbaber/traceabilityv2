﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Traceability
{
	/// <summary>Implements <see cref="ITracer"/>, but does nothing otherwise.
	/// The purpose of this class is to assist with trace regression unit tests
	/// to ensure that any tracing does not change any behavior.</summary>
	public class FakeTracer : ITracer
	{
		/// <summary>Does nothing.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="classAndFunctionName">Not used.</param>
		public ITracer FunctionStart(string classAndFunctionName)
		{
			return this;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		public ITracer FunctionEnd()
		{
			return this;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The value passed in as <paramref name="result"/>.</returns>
		/// <param name="result">Passed through.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T FunctionEnd<T>(T result)
		{
			return result;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The value passed in as <paramref name="value"/>.</returns>
		/// <param name="classAndPropertyName">Not used.</param>
		/// <param name="value">Passed through.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T GetProperty<T>(string classAndPropertyName, T value)
		{
			return value;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The value passed in as <paramref name="value"/>.</returns>
		/// <param name="classAndPropertyName">Not used.</param>
		/// <param name="value">Passed through.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T SetProperty<T>(string classAndPropertyName, T value)
		{
			return value;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The value passed in as <paramref name="value"/>.</returns>
		/// <param name="className">Not used.</param>
		/// <param name="index">Not used.</param>
		/// <param name="value">Passed through.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T GetIndexer<T>(string className, int index, T value)
		{
			return value;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The value passed in as <paramref name="value"/>.</returns>
		/// <param name="className">Not used.</param>
		/// <param name="index">Not used.</param>
		/// <param name="value">Passed through.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public T SetIndexer<T>(string className, int index, T value)
		{
			return value;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">Not used.</param>
		/// <param name="value">Not used.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public ITracer Parameter<T>(string parameterName, T value)
		{
			return this;
		}

		/// <summary>Does nothing.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">Not used.</param>
		/// <param name="args">Not used.</param>
		/// <typeparam name="T">Any type.</typeparam>
		public ITracer Parameter<T>(string parameterName, T[] args)
		{
			return this;
		}
	}
}
