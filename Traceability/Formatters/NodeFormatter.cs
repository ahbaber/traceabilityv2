﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Text;

namespace Traceability.Formatters
{
	/// <summary>Used to format the data contained in a <see cref="TraceNode"/>.
	/// The default formatting is well formed XML.  Other formatting styles may
	/// be injected.</summary>
	public class NodeFormatter
	{
		/// <summary>Initializes a new instance of the <see cref="T:Traceability.Formatters.NodeFormatter"/> class.</summary>
		/// <param name="defaultParser">Sets the default <see cref="INodeParser"/> to handle the data parsing.
		/// If no paraser is provided, then it will default to XML.</param>
		public NodeFormatter(INodeParser defaultParser = null)
		{
			_parserMap = new ParserMap(
				defaultParser ?? new XmlSupport.XmlNodeParser());
		}

		/// <summary>Adds an additional <see cref="INodeTaggedParser"/> to the underlying map.</summary>
		/// <returns>The current <see cref="NodeFormatter"/> instance.</returns>
		/// <param name="parser">The additional <see cref="INodeTaggedParser"/> that will be added.</param>
		public NodeFormatter AddParser(INodeTaggedParser parser)
		{
			return AddParser(parser.Tag, parser);
		}

		/// <summary>Adds an additional <see cref="INodeParser"/> to the underlying map.</summary>
		/// <returns>The current <see cref="NodeFormatter"/> instance.</returns>
		/// <param name="tag">The key that the new parser will be associated with.</param>
		/// <param name="parser">The additional <see cref="INodeParser"/> that will be added.</param>
		public NodeFormatter AddParser(string tag, INodeParser parser)
		{
			_parserMap.AddParser(tag, parser);
			return this;
		}

		/// <summary>Adds an additional <see cref="INodeMultiTagParser"/> to the underlying map.
		/// The parser will process each of the nodes that it has a key for.</summary>
		/// <returns>The current <see cref="NodeFormatter"/> instance.</returns>
		/// <param name="parser">The additional <see cref="INodeMultiTagParser"/> that will be added.</param>
		public NodeFormatter AddParser(INodeMultiTagParser parser)
		{
			foreach (var key in parser.Tags)
			{
				AddParser(key, parser);
			}
			return this;
		}

		/// <summary>Sets the initial indent value that wil be used when any data is formatted.</summary>
		/// <returns>The current <see cref="NodeFormatter"/> instance.</returns>
		/// <param name="indent">The <see cref="string"/> that be used for the initial indent.</param>
		public NodeFormatter SetIndent(string indent)
		{
			_indent = indent;
			return this;
		}

		/// <summary>Sets the tab value will be used for subsequent indention of child data.</summary>
		/// <returns>The current <see cref="NodeFormatter"/> instance.</returns>
		/// <param name="tab">The <see cref="string"/> that be used for the subsequent indent.</param>
		public NodeFormatter SetTab(string tab)
		{
			_tab = tab;
			return this;
		}

		/// <summary>Sets the new line value that will be used to terminate each line of the formatted data.</summary>
		/// <returns>The current <see cref="NodeFormatter"/> instance.</returns>
		/// <param name="newLine">The <see cref="string"/> that be used for each new line.</param>
		public NodeFormatter SetNewLine(string newLine)
		{
			_newLine = newLine;
			return this;
		}

		/// <summary>Converts <see cref="TraceNode"/> data into a formatted <see cref="string"/>, and returns the string.</summary>
		/// <returns>A <see cref="string"/> that contains formatted data.</returns>
		/// <param name="node">The <see cref="TraceNode"/> whose data will be formatted.</param>
		public string Format(TraceNode node)
		{
			return Build(new StringBuilder(), node).ToString();
		}

		/// <summary>Converts <see cref="TraceNode"/> data into a formatted <see cref="string"/>, and returns the string.
		/// In the event an exception is thrown while formatting the data, the exception message will be prepended
		/// to any data that had already been successfully formatted.</summary>
		/// <returns>A <see cref="string"/> that contains formatted data.</returns>
		/// <param name="node">The <see cref="TraceNode"/> whose data will be formatted.</param>
		public string FormatSafe(TraceNode node)
		{
			var builder = new StringBuilder();
			try
			{
				return Build(builder, node).ToString();
			}
			catch (Exception ex)
			{
				return ex.Message + "\n\n"
					+ ex.StackTrace + "\n\n"
					+ builder.ToString();
			}
		}

		/// <summary>Converts the <see cref="TraceNode"/> data into a formatted <see cref="StringBuilder"/>.</summary>
		/// <returns>A <see cref="StringBuilder"/> containing formatted data.</returns>
		/// <param name="builder">The <see cref="StringBuilder"/> that will contain the formatted data.</param>
		/// <param name="node">The <see cref="TraceNode"/> whose data will be formatted.</param>
		public virtual StringBuilder Build(StringBuilder builder, TraceNode node)
		{
			_parserMap.GetParser(node.Tag).Build(builder, _parserMap, node, _context, _indent, _tab, _newLine);
			return builder;
		}

		private string _indent = "";
		private string _tab = "";
		private string _newLine = "";
		private readonly ParserMap _parserMap;
		protected readonly Context _context = new Context();
	}
}
