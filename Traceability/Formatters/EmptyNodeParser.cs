﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;

namespace Traceability.Formatters
{
	/// <summary>Does no parsing.  This is useful for situations where a specific
	/// <see cref="TraceNode"/> should be skipped.</summary>
	public class EmptyNodeParser : INodeParser
	{
		/// <summary>Does no formatting at all.</summary>
		/// <returns><paramref name="builder"/> with no changes.</returns>
		/// <param name="builder">Will be oassed tbhrough unchanged.</param>
		/// <param name="map">Not used.</param>
		/// <param name="node">Not used.</param>
		/// <param name="context">Not used.</param>
		/// <param name="indent">Not used.</param>
		/// <param name="tab">Not used.</param>
		/// <param name="newLine">Not used.</param>
		public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			return builder;
		}
	}
}
