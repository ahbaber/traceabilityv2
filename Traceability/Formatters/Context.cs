﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;

namespace Traceability.Formatters
{
	/// <summary>Gives the capability of storing key value pairs so they may be
	/// passed between different parsers.</summary>
	public class Context
	{
		/// <summary>Checks to see if a key is present in the <see cref="Context"/>.</summary>
		/// <returns><c>true</c>, if key was containsed, <c>false</c> otherwise.</returns>
		/// <param name="key">The Key to check.</param>
		public bool ContainsKey(string key)
		{
			return _dataMap.ContainsKey(key);
		}

		/// <summary>Removes a key value pair from the context instance.</summary>
		/// <returns>The current <see cref="Context"/> instance.</returns>
		/// <param name="key">The key of the element to remove.</param>
		public Context Remove(string key)
		{
			_dataMap.Remove(key);
			return this;
		}

		/// <summary>Retrieves a boolean value.  If the key does not exist, then 
		/// <c>false</c> will be returned.</summary>
		/// <returns>The value associated with the key, <c>false</c> otherwise.</returns>
		/// <param name="key">The key of the boolean value.</param>
		public bool GetBool(string key)
		{
			return _dataMap.TryGetValue(key, out object o)
 				&& Convert.ToBoolean(o);
		}

		/// <summary>Retrieves an int value.  If the key does not exist, then 
		/// <c>0</c> will be returned.</summary>
		/// <returns>The value associated with the key, <c>0</c> otherwise.</returns>
		/// <param name="key">The key of the boolean value.</param>
		public int GetInt(string key)
		{
			return _dataMap.TryGetValue(key, out object o)
				? Convert.ToInt32(o)
				: 0;
		}

		/// <summary>Retrieves a atring value.  If the key does not exist, then 
		/// null will be returned.</summary>
		/// <returns>The value associated with the key, <c>null</c> otherwise.</returns>
		/// <param name="key">The key of the boolean value.</param>
		public string GetString(string key)
		{
			return _dataMap.TryGetValue(key, out object o)
				? Convert.ToString(o)
				: null;
		}

		/// <summary>Retrieves a atring value.  If the key does not exist, then 
		/// null will be returned.</summary>
		/// <returns>The value associated with the key, <c>null</c> otherwise.</returns>
		/// <param name="key">The key of the boolean value.</param>
		/// <typeparam name="T">Any class type.</typeparam>
		public T Get<T>(string key) where T : class
		{
			return _dataMap.TryGetValue(key, out object o)
				? o as T
				: default(T);
		}

		/// <summary>Stores a key value in in the context.</summary>
		/// <returns>The current <see cref="Context"/> instance.</returns>
		/// <param name="key">Key.</param>
		/// <param name="value">Value.</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public Context Set<T>(string key, T value)
		{
			if (_dataMap.ContainsKey(key))
			{ _dataMap[key] = value; }
			else
			{ _dataMap.Add(key, value); }
			return this;
		}

		private readonly Dictionary<string, object> _dataMap = new Dictionary<string, object>();
	}
}
