﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;

namespace Traceability.Formatters
{
	/// <summary>Interface for implemeting parsers that handle TraceNode data.</summary>
	public interface INodeParser
	{
		/// <summary>Extracts the data from <paramref name="node"/> and places it
		/// into <paramref name="builder"/>.</summary>
		/// <returns>A <see cref="StringBuilder"/> containing formatted data.</returns>
		/// <param name="builder">A <see cref="StringBuilder"/> to contain 
		/// formatted data.</param>
		/// <param name="map">A <see cref="ParserMap"/> that allows children of
		/// the current <paramref name="node"/> to use other parsers.</param>
		/// <param name="node">The <see cref="TraceNode"/> whose data is being formatted.</param>
		/// <param name="context">A <see cref="Context"/> to pass values between <see cref="INodeParser"/>s.</param>
		/// <param name="indent">A <see cref="string"/> that will precede all other formatted text.</param>
		/// <param name="tab">A <see cref="string"/> that will be used to add further indentation for child data.</param>
		/// <param name="newLine">A <see cref="string"/> that is used to terminate each line of formatted data.</param>
		StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine);
	}
}
