﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;

namespace Traceability.Formatters
{
	/// <summary>
	/// Abstract node child processor.
	/// </summary>
	public class AbstractNodeChildProcessor
	{
		/// <summary>Checks to see if <paramref name="node"/> has any children,
		/// and proccesses them if present.</summary>
		/// <param name="builder">A <see cref="StringBuilder"/> to contain 
		/// formatted data.</param>
		/// <param name="map">A <see cref="ParserMap"/> that allows children of
		/// the current <paramref name="node"/> to use other parsers.</param>
		/// <param name="node">The <see cref="TraceNode"/> whose data is being formatted.</param>
		/// <param name="context">A <see cref="Context"/> to pass values between <see cref="INodeParser"/>s.</param>
		/// <param name="indent">A <see cref="string"/> that will precede all other formatted text.</param>
		/// <param name="tab">A <see cref="string"/> that will be used to add further indentation for child data.</param>
		/// <param name="newLine">A <see cref="string"/> that is used to terminate each line of formatted data.</param>
		protected void ProcessChildren(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			if (node.HasChildren)
			{
				foreach (var child in node.Children)
				{
					map.GetParser(child.Tag).Build(builder, map, child, context, indent, tab, newLine);
				}
			}
		}
	}
}
