﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;

namespace Traceability.Formatters.XmlSupport
{
	/// <summary>Parses a <see cref="TraceNode"/> and converts it into well
	/// formed XML.</summary>
	public class XmlNodeParser : AbstractNodeChildProcessor, INodeParser
	{
		/// <summary>Initializes a new instance of the <see cref="T:Traceability.Formatters.Xml.XmlNode"/> class.</summary>
		public XmlNodeParser()
		: this(new XmlValue(), new XmlParameter())
		{ }

		/// <summary>Initializes a new instance of the <see cref="T:Traceability.Formatters.Xml.XmlNode"/> class.</summary>
		/// <param name="value">Allows an override to handle how the <see cref="TraceNode"/>'s value is formatted.</param>
		/// <param name="parameter">Allows an override to handle how the <see cref="TraceNode"/>'s parameters are formatted.</param>
		public XmlNodeParser(XmlValue value, XmlParameter parameter)
		{
			_value = value;
			_parameter = parameter;
		}

		/// <summary>Extracts the data from <paramref name="node"/> and places it
		/// into <paramref name="builder"/>.</summary>
		/// <returns>A <see cref="StringBuilder"/> containing formatted data.</returns>
		/// <param name="builder">A <see cref="StringBuilder"/> to contain 
		/// formatted data.</param>
		/// <param name="map">A <see cref="ParserMap"/> that allows children of
		/// the current <paramref name="node"/> to use other parsers.</param>
		/// <param name="node">The <see cref="TraceNode"/> whose data is being formatted.</param>
		/// <param name="context">A <see cref="Context"/> to pass values between <see cref="INodeParser"/>s.</param>
		/// <param name="indent">A <see cref="string"/> that will precede all other formatted text.</param>
		/// <param name="tab">A <see cref="string"/> that will be used to add further indentation for child data.</param>
		/// <param name="newLine">A <see cref="string"/> that is used to terminate each line of formatted data.</param>
		public virtual StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			if (node.HasParameters || node.HasChildren)
			{
				string childIndent = indent + tab;
				builder.Append($"{indent}<{node.Tag}>{newLine}");
				_parameter.Build(builder, node, context, childIndent, tab, newLine);
				ProcessChildren(builder, map, node, context, childIndent, tab, newLine);
				_value.Build(builder, node, context, childIndent, tab, newLine);
				builder.Append($"{indent}</{node.Tag}>{newLine}");
			}
			else if (node.Value != null)
			{
				builder.Append($"{indent}<{node.Tag}>");
				_value.Build(builder, node, context, indent + tab, tab, newLine);
				builder.Append($"</{node.Tag}>{newLine}");
			}
			else
			{
				builder.Append($"{indent}<{node.Tag} />{newLine}");
			}
			return builder;
		}

		protected readonly XmlValue _value;
		protected readonly XmlParameter _parameter;
	}
}
