﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;

namespace Traceability.Formatters.XmlSupport
{
	/// <summary>Parses a <see cref="TraceNode"/> value field, and formats the
	/// value into well formed xml.  If the only contents of the node is the value,
	/// then the value will be returned in its current state.  If the node has
	/// other data that will appear as child node, then the value will be placed
	/// inside of a child element with a tag "value".</summary>
	public class XmlValue
	{
		/// <summary>Extracts the value from <paramref name="node"/> and places it
		/// into <paramref name="builder"/>.</summary>
		/// <returns>A <see cref="StringBuilder"/> containing the formatted value.</returns>
		/// <param name="builder">A <see cref="StringBuilder"/> to contain the
		/// formatted value.</param>
		/// <param name="node">The <see cref="TraceNode"/> whose value is being formatted.</param>
		/// <param name="context">A <see cref="Context"/> to pass values between <see cref="INodeParser"/>s.</param>
		/// <param name="indent">A <see cref="string"/> that will precede all other formatted text.</param>
		/// <param name="tab">A <see cref="string"/> that will be used to add further indentation for child data.</param>
		/// <param name="newLine">A <see cref="string"/> that is used to terminate each line of formatted data.</param>
		public virtual StringBuilder Build(StringBuilder builder, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			if (node.Value != null)
			{
				if (node.HasParameters || node.HasChildren)
				{
					builder.Append($"{indent}<value>{node.Value}</value>{newLine}");
				}
				else
				{
					builder.Append(node.Value);
				}
			}
			return builder;
		}
	}
}


