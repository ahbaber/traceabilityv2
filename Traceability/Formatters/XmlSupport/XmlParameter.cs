﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;

namespace Traceability.Formatters.XmlSupport
{
	/// <summary>Formats the parameter data of a <see cref="TraceNode"/>.</summary>
	public class XmlParameter
	{
		/// <summary>Formats the parameter data of the <paramref name="node"/>
		/// and places it into <paramref name="builder"/>.</summary>
		/// <returns>A <see cref="StringBuilder"/> containing formatted data.</returns>
		/// <param name="builder">A <see cref="StringBuilder"/> to collect
		/// formatted data.</param>
		/// <param name="node">The <see cref="TraceNode"/> whose parameter data 
		/// will be formatted.</param>
		/// <param name="context">A <see cref="Context"/> to pass values between <see cref="INodeParser"/>s.</param>
		/// <param name="indent">A <see cref="string"/> that will precede all other formatted text.</param>
		/// <param name="tab">A <see cref="string"/> that will be used to add further indentation for child data.</param>
		/// <param name="newLine">A <see cref="string"/> that is used to terminate each line of formatted data.</param>
		public virtual StringBuilder Build(StringBuilder builder, TraceNode node, Context context, string indent, string tab, string newLine)
		{
			if (node.HasParameters)
			{
				string childIndent = indent + tab;
				builder.Append($"{indent}<parameters>{newLine}");
				foreach (var param in node.Parameters)
				{
					builder.Append($"{childIndent}<{param.Key}>{param.Value}</{param.Key}>{newLine}");
				}
				builder.Append($"{indent}</parameters>{newLine}");
			}
			return builder;
		}
	}
}
