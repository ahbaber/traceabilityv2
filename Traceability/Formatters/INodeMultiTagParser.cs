﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;

namespace Traceability.Formatters
{
	/// <summary>Extends the <see cref="INodeParser"/> to include multiple Key
	/// values for identifying the tag of the node that it will parse.</summary>
	public interface INodeMultiTagParser : INodeParser
	{
		/// <summary>Identifies the tags of the nodes that will be parsed.</summary>
		/// <value>The node tags.</value>
		List<string> Tags { get; }
	}
}
