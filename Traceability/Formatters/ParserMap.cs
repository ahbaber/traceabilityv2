﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Collections.Generic;

namespace Traceability.Formatters
{
	/// <summary>Contains a map that allows different parsers to handle different
	/// <see cref="TraceNode"/>s based on the node's tag.  If a specific parser
	/// does not exist for a tag, then a default parser will be returned.</summary>
	public class ParserMap
	{
		/// <summary>Initializes a new instance of the <see cref="T:Traceability.Formatters.ParserMap"/> class.</summary>
		/// <param name="defaultParser">This parser will be used if a custom one is not found for a specific tag.</param>
		public ParserMap(INodeParser defaultParser)
		{
			_default = defaultParser;
		}

		/// <summary>Gets a parser based on the tag of the node.  If a specific 
		/// parser cannot be found, then the default parser will be returned.</summary>
		/// <returns>A parser for processing a <see cref="TraceNode"/>.</returns>
		/// <param name="tag">The key of the parser being searched for.</param>
		public INodeParser GetParser(string tag)
		{
			if (_parsers.TryGetValue(tag, out INodeParser parser))
			{ return parser; }
			return _default;
		}

		/// <summary>Adds an <see cref="INodeParser"/> to the map.</summary>
		/// <returns>The current instance of the <see cref="ParserMap"/>.</returns>
		/// <param name="tag">The key associated with the parser.</param>
		/// <param name="parser">The <see cref="INodeParser"/> that will be added to the map.</param>
		public ParserMap AddParser(string tag, INodeParser parser)
		{
			_parsers.Add(tag, parser);
			return this;
		}

		/// <summary>Adds an <see cref="INodeParser"/> to the map.  The parser
		/// will be removed automatically when the <see cref="Memento"/> is disposed.
		/// If the new <see cref="INodeParser"/> is replacing one that is already
		/// associated with <paramref name="tag"/>, the the prior one will be returned
		/// to the map when the <see cref="Memento"/> is disposed.</summary>
		/// <returns>A <see cref="Memento"/> instance that controls the life time of the new <see cref="INodeParser"/>.</returns>
		/// <param name="tag">The key that the new <see cref="INodeParser"/> will be associated with.</param>
		/// <param name="parser">The new <see cref="INodeParser"/>.</param>
		public Memento AddTempParser(string tag, INodeParser parser)
		{
			return new Memento(this, tag, parser);
		}

		/// <summary>Add an <see cref="INodeTaggedParser"/> to the map.  The parser
		/// will be removed automatically when the <see cref="Memento"/> is disposed.
		/// If the new <see cref="INodeTaggedParser"/> is replacing one that is already
		/// associated with its key, the the prior one will be returned
		/// to the map when the <see cref="Memento"/> is disposed.</summary>
		/// <returns>A <see cref="Memento"/> instance that controls the life time of the new <see cref="INodeParser"/>.</returns>
		/// <param name="parser">The new <see cref="INodeTaggedParser"/>.</param>
		public Memento AddTempParser(INodeTaggedParser parser)
		{
			return AddTempParser(parser.Tag, parser);
		}

		private Dictionary<string, INodeParser> _parsers = new Dictionary<string, INodeParser>();
		private readonly INodeParser _default;

		/// <summary>Used to track the lifetime of temporary <see cref="INodeParser"/>s.</summary>
		public class Memento : IDisposable
		{
			/// <summary>Initializes a new instance of the <see cref="T:Traceability.Formatters.ParserMap.Memento"/> class.</summary>
			/// <param name="parent">The <see cref="ParserMap"/> that the <see cref="Memento"/> interacts with.</param>
			/// <param name="tag">The key that the <see cref="INodeParser"/> is associated with.</param>
			/// <param name="node">The new <see cref="INodeParser"/>.</param>
			public Memento(ParserMap parent, string tag, INodeParser node)
			{
				_parent = parent;
				_tag = tag;
				if (parent._parsers.TryGetValue(tag, out _previousNode))
				{
					parent._parsers.Remove(tag);
				}
				parent._parsers.Add(tag, node);
			}

			/// <summary>Removes the new <see cref="INodeParser"/>, and restores the old <see cref="INodeParser"/> if it was present.</summary>
			/// <remarks>Call <see cref="Dispose"/> when you are finished using the
			/// <see cref="T:Traceability.Formatters.ParserMap.Memento"/>. The <see cref="Dispose"/> method leaves the
			/// <see cref="T:Traceability.Formatters.ParserMap.Memento"/> in an unusable state. After calling
			/// <see cref="Dispose"/>, you must release all references to the
			/// <see cref="T:Traceability.Formatters.ParserMap.Memento"/> so the garbage collector can reclaim the memory that
			/// the <see cref="T:Traceability.Formatters.ParserMap.Memento"/> was occupying.</remarks>
			public void Dispose()
			{
				_parent._parsers.Remove(_tag);
				if (_previousNode != null)
				{
					_parent._parsers.Add(_tag, _previousNode);
				}
			}

			private readonly ParserMap _parent;
			private readonly string _tag;
			private readonly INodeParser _previousNode;
		}
	}
}
