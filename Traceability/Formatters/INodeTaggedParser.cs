﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
namespace Traceability.Formatters
{
	/// <summary>Extends the <see cref="INodeParser"/> to include a Key value for
	/// identifying the tag of the node that it will parse.</summary>
	public interface INodeTaggedParser : INodeParser
	{
		/// <summary>Identifies the tag of the node that will be parsed.</summary>
		/// <value>The node tag.</value>
		string Tag { get; }
	}
}
