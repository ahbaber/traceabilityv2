﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Potential Code Quality Issues", "RECS0017:Possible compare of value type with 'null'", Justification = "unit testing shows it is safe with value types", Scope = "member", Target = "~M:Traceability.TraceNode.AddParameter``1(System.String,``0)~Traceability.TraceNode")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Potential Code Quality Issues", "RECS0017:Possible compare of value type with 'null'", Justification = "unit testing shows it is safe with value types", Scope = "member", Target = "~M:Traceability.Tracer.ConvertValue``1(``0)~System.String")]

