﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Traceability
{
	/// <summary>used to collect and store trace data.</summary>
	public class Tracer : ITracer
	{
		/// <summary>Gives access to the currently active <see cref="TraceNode"/>.</summary>
		/// <value>The active <see cref="TraceNode"/>.</value>
		public TraceNode Node => _node;

		/// <summary>Gives access to the root <see cref="TraceNode"/>.</summary>
		/// <value>The root <see cref="TraceNode"/>.</value>
		public TraceNode Root => _root;

		/// <summary>Initializes a new instance of the <see cref="T:Traceability.Tracer"/> class.</summary>
		/// <param name="tag">The tag representing the trace data.</param>
		public Tracer(string tag)
		{
			_node = new TraceNode(tag);
			_root = _node;
		}

		/// <summary>Used to start a new node.  All new trace data added will
		/// be contained in this new node.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="classAndFunctionName">The name of the class and function being called.</param>
		public ITracer FunctionStart(string classAndFunctionName)
		{
			_node = _node.CreateChild(classAndFunctionName);
			return this;
		}

		/// <summary>Closes the current node.  Any new trace data will be added
		/// to the parent node.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		public ITracer FunctionEnd()
		{
			_node = _node.Parent ?? _root;
			return this;
		}

		/// <summary>Stores the results as trace data, and then closes the current
		/// node.  Any new trace data will be added to the parent node.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="result">The value to be stored as trace data.</param>
		/// <typeparam name="T">Any type that has .ToString() implemented.</typeparam>
		public T FunctionEnd<T>(T result)
		{
			_node.Value = ConvertValue(result);
			FunctionEnd();
			return result;
		}

		/// <summary>Traces the value of the get Item [] property.  The new node 
		/// will contain the index and value retrieved.  Any new trace data will
		/// be added to the parent node.  Note: the trace node will have a tag of
		/// className_index_get.  This is to denote that the class's indexer was
		/// called to get a value.</summary>
		/// <returns>The value retrieved.</returns>
		/// <param name="className">The name of the class.</param>
		/// <param name="index">The index of the item retrieved.</param>
		/// <param name="value">The value retrieved.</param>
		/// <typeparam name="T">Any type that has .ToString() implemented.</typeparam>
		public T GetIndexer<T>(string className, int index, T value)
		{
			_node.CreateChild($"{className}_index_get")
				.AddParameter("index", index)
				.Value = ConvertValue(value);
			return value;
		}

		/// <summary>Traces the value of the set Item [] property.  The new node 
		/// will contain the index and value set.  Any new trace data will
		/// be added to the parent node.  Note: the trace node will have a tag of
		/// className_index_set.  This is to denote that the class's indexer was
		/// called to write a value.</summary>
		/// <returns>The value set.</returns>
		/// <param name="className">The name of the class.</param>
		/// <param name="index">The index of the item written.</param>
		/// <param name="value">The value written.</param>
		/// <typeparam name="T">Any type that has .ToString() implemented.</typeparam>
		public T SetIndexer<T>(string className, int index, T value)
		{
			_node.CreateChild($"{className}_index_set")
				.AddParameter("index", index)
				.Value = ConvertValue(value);
			return value;
		}

		/// <summary>Traces the value of a get property.  The child note will
		/// indicate which property was gotten, and the value gotten.  Note: the
		/// trace node will have a tag of classAndPropertyName_get.  This is to
		/// denote that the property was read.</summary>
		/// <returns>The value retrieved from the property.</returns>
		/// <param name="classAndPropertyName">The name of the property.</param>
		/// <param name="value">The value of the property.</param>
		/// <typeparam name="T">Any type that has .ToString() implemented.</typeparam>
		public T GetProperty<T>(string classAndPropertyName, T value)
		{
			_node.CreateChild($"{classAndPropertyName}_get")
				.Value = ConvertValue(value);
			return value;
		}

		/// <summary>Traces the value of a set property.  The child note will
		/// indicate which property was written to, and the value written.</summary>
		/// <returns>The value written to the property.  Note: the
		/// trace node will have a tag of classAndPropertyName_set.  This is to
		/// denote that the property was written to.</returns>
		/// <param name="classAndPropertyName">The name of the property.</param>
		/// <param name="value">The value of the property.</param>
		/// <typeparam name="T">Any type that has .ToString() implemented.</typeparam>
		public T SetProperty<T>(string classAndPropertyName, T value)
		{
			_node.CreateChild($"{classAndPropertyName}_set")
				.Value = ConvertValue(value);
			return value;
		}

		/// <summary>Adds a parameter node to the current node.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">The name of the parameter.</param>
		/// <param name="value">The value of the parameter.</param>
		/// <typeparam name="T">Any type that has .ToString() implemented.</typeparam>
		public ITracer Parameter<T>(string parameterName, T value)
		{
			_node.AddParameter(parameterName, value);
			return this;
		}

		/// <summary>Adds a parameter node to the current node.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">The name of the parameter.</param>
		/// <param name="args">The array of arguments.</param>
		/// <typeparam name="T">Any type that has .ToString() implemented.</typeparam>
		public ITracer Parameter<T>(string parameterName, T[] args)
		{
			int size = args == null ? 0 : args.Length;
			_node.AddParameter(parameterName, $"{ConvertValue(args)}({size})");
			return this;
		}

		private readonly TraceNode _root;
		private TraceNode _node;

		private string ConvertValue<T>(T value)
		{
			return value == null ? "null" : value.ToString();
		}
	}
}
