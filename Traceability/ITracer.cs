﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Traceability
{
	/// <summary>Interface for tracers	This allows us to use various tracers that collect
	/// different information.</summary>
	public interface ITracer
	{
		/// <summary>Indicates that a new function call has begun.
		/// </summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="classAndFunctionName">The name of the class and function being called.</param>
		ITracer FunctionStart(string classAndFunctionName);

		/// <summary>Closes the current function.  Any new data will be inserted to
		/// the parent node of the node that has been closed.
		/// </summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		ITracer FunctionEnd();

		/// <summary>Stores the resutls, and closes the current function.
		/// Any subsequent data will be inserted to the parent node of the function
		/// that has been closed.</summary>
		/// <returns>The passed in result value.</returns>
		/// <param name="result">The result to be stored in the trace data.</param>
		/// <typeparam name="T">Any type that has ToString() implemented.</typeparam>
		T FunctionEnd<T>(T result);

		/// <summary>Traces the value of a get property.  The current node will
		/// have a child node and value added to it.  This child note will
		/// indicate which property was gotten, and the value gotten.</summary>
		/// <returns>The property value.</returns>
		/// <param name="classAndPropertyName">The name of the class and property</param>
		/// <param name="value">The value of the property.</param>
		/// <typeparam name="T">Any type that has ToString() implemented.</typeparam>
		T GetProperty<T>(string classAndPropertyName, T value);

		/// <summary>Traces the value of a set property.  The current node will
		/// have a child node and value added to it.  This child note will
		/// indicate which property was set, and the value set.</summary>
		/// <returns>The property value.</returns>
		/// <param name="classAndPropertyName">The name of the class and property</param>
		/// <param name="value">The value of the property.</param>
		/// <typeparam name="T">Any type that has ToString() implemented.</typeparam>
		T SetProperty<T>(string classAndPropertyName, T value);

		/// <summary>Traces the value of a get Item [] property.  The current node will
		/// have a child node and value added to it.  This child note will
		/// indicate which property was gotten, and the value gotten.</summary>
		/// <returns>The value retreived.</returns>
		/// <param name="className">The name of the class.</param>
		/// <param name="index">The index of the item.</param>
		/// <param name="value">The value retreived.</param>
		/// <typeparam name="T">Any type that has ToString() implemented.</typeparam>
		T GetIndexer<T>(string className, int index, T value);

		/// <summary>Traces the value of a set Item [] property.  The current node will
		/// have a child node and value added to it.  This child note will
		/// indicate which property was gotten, and the value gotten.</summary>
		/// <returns>The value set.</returns>
		/// <param name="className">The name of the class.</param>
		/// <param name="index">The index of the item.</param>
		/// <param name="value">The value set..</param>
		/// <typeparam name="T">Any type that has ToString() implemented.</typeparam>
		T SetIndexer<T>(string className, int index, T value);

		/// <summary>Adds a child node to the current node that indicates a
		/// parameter has been passed in, as well as the name of the parameter,
		/// and the value of the parameter. </summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">The name of the parameter.</param>
		/// <param name="value">The value of the parameter.</param>
		/// <typeparam name="T">Any type that has ToString() implemented.</typeparam>
		ITracer Parameter<T>(string parameterName, T value);

		/// <summary>Adda a set of child nodes that indicate that an array has been
		/// passed in as a parameter.  The size of teh array, and up to the first
		/// three values will be included in the trace data.</summary>
		/// <returns>The current <see cref="ITracer"/> instance.</returns>
		/// <param name="parameterName">The name of the parameter.</param>
		/// <param name="args">The array of arguments.</param>
		/// <typeparam name="T">Any type that has ToString() implemented.</typeparam>
		ITracer Parameter<T>(string parameterName, T[] args);
	}
}
