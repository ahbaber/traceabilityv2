﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace TestReferenceFunctional.BaseClasses
{
	public class PropertyAndMethodClass
	{
		public virtual int Property { get; set; }
		public virtual void Method() { }
	}
}
