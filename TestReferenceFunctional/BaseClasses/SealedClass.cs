﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace TestReferenceFunctional.BaseClasses
{
	public class BaseClass
	{
		public virtual void Method() { }
	}

	public sealed class SealedClass : BaseClass
	{
		public override void Method()
		{
			base.Method();
		}
	}
}
