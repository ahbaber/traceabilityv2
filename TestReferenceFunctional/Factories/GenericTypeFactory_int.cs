﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TestReferenceFunctional.BaseClasses;

namespace TestReferenceFunctional.Factories
{
	public class GenericTypeFactory_int
	{
		public virtual GenericType<int> Construct()
		{
			return new GenericType<int>();
		}
	}

	public partial class TestStrings
	{
		public const string GenericTypeFactory_intExpected = @"// Auto generated file
// Any modification to this file may be lost
using System;
using TestReferenceFunctional.BaseClasses;

namespace TestReferenceFunctional.Factories
{
	public class GenericTypeFactory_int
	{
		public virtual GenericType<int> Construct()
		{
			return new GenericType<int>();
		}
	}
}
";
	}
}
