﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TestReferenceFunctional.BaseClasses;

namespace TestReferenceFunctional.Factories
{
	public class EmptyClassFactory
	{
		public virtual EmptyClass Construct()
		{
			return new EmptyClass();
		}
	}

	public partial class TestStrings
	{
		public const string EmptyClassFactoryExpected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;

namespace TestReferenceFunctional.Factories
{
	public class EmptyClassFactory
	{
		public virtual EmptyClass Construct()
		{
			return new EmptyClass();
		}
	}
}
";
	}
}
