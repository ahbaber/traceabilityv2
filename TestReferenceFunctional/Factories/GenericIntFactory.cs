﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TestReferenceFunctional.BaseClasses;

namespace TestReferenceFunctional.Factories
{
	public class GenericIntFactory
	{
		public virtual GenericInt Construct()
		{
			return new GenericInt();
		}
	}

	public partial class TestStrings
	{
		public const string GenericIntFactoryExpected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;

namespace TestReferenceFunctional.Factories
{
	public class GenericIntFactory
	{
		public virtual GenericInt Construct()
		{
			return new GenericInt();
		}
	}
}
";
	}
}
