﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceGenericType_int : TestReferenceFunctional.BaseClasses.GenericType<int>
	{
		public TraceGenericType_int(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}

	public partial class TestStrings
	{
		public const string TraceGenericType_intExpected = @"// Auto generated file
// Any modification to this file may be lost
using System;
using TestReferenceFunctional.BaseClasses;
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceGenericType_int : TestReferenceFunctional.BaseClasses.GenericType<int>
	{
		public TraceGenericType_int(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}
}
";
	}
}
