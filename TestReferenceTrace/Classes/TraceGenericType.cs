﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceGenericType<T> : TestReferenceFunctional.BaseClasses.GenericType<T>
	{
		public TraceGenericType(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}

	public partial class TestStrings
	{
		public const string TraceGenericTypeExpected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceGenericType<T> : TestReferenceFunctional.BaseClasses.GenericType<T>
	{
		public TraceGenericType(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}
}
";
	}
}
