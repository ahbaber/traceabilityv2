﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceClassWithNonTraceableConstructor : TestReferenceFunctional.BaseClasses.ClassWithNonTraceableConstructor
	{
		public TraceClassWithNonTraceableConstructor(ITracer tracer)
		{
			_tracer = tracer;
		}

		// skipped private ClassWithNonTraceableConstructor(int i)

		private readonly ITracer _tracer;
	}

	public partial class TestStrings
	{
		public const string TraceClassWithNonTraceableConstructorExpected = @"// Auto generated file
// Any modification to this file may be lost
using System;
using TestReferenceFunctional.BaseClasses;
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceClassWithNonTraceableConstructor : TestReferenceFunctional.BaseClasses.ClassWithNonTraceableConstructor
	{
		public TraceClassWithNonTraceableConstructor(ITracer tracer)
		{
			_tracer = tracer;
		}

		// skipped private ClassWithNonTraceableConstructor(int i)

		private readonly ITracer _tracer;
	}
}
";
	}
}
