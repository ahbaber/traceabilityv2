﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TracePropertyAndMethodClass : TestReferenceFunctional.BaseClasses.PropertyAndMethodClass
	{
		public TracePropertyAndMethodClass(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override int Property
		{
			get { return _tracer.GetProperty("PropertyAndMethodClass.Property", base.Property); }
			set { base.Property = _tracer.SetProperty("PropertyAndMethodClass.Property", value); }
		}

		public override void Method()
		{
			_tracer.FunctionStart("PropertyAndMethodClass.Method");
			base.Method();
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}

	public partial class TestStrings
	{
		public const string TracePropertyAndMethodClassExpected = @"// Auto generated file
// Any modification to this file may be lost
using System;
using TestReferenceFunctional.BaseClasses;
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TracePropertyAndMethodClass : TestReferenceFunctional.BaseClasses.PropertyAndMethodClass
	{
		public TracePropertyAndMethodClass(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override int Property
		{
			get { return _tracer.GetProperty(""PropertyAndMethodClass.Property"", base.Property); }
			set { base.Property = _tracer.SetProperty(""PropertyAndMethodClass.Property"", value); }
		}

		public override void Method()
		{
			_tracer.FunctionStart(""PropertyAndMethodClass.Method"");
			base.Method();
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
";
	}
}
