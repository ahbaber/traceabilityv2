﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceGenericInt2 : TestReferenceFunctional.BaseClasses.GenericType<int>
	{
		public TraceGenericInt2(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}

	public partial class TestStrings
	{
		public const string TraceGenericInt2Expected = @"// Auto generated file
// Any modification to this file may be lost
using System;
using TestReferenceFunctional.BaseClasses;
using Traceability;

namespace TestReferenceTrace.Classes
{
	public class TraceGenericInt2 : TestReferenceFunctional.BaseClasses.GenericType<int>
	{
		public TraceGenericInt2(ITracer tracer)
		{
			_tracer = tracer;
		}

		private readonly ITracer _tracer;
	}
}
";
	}
}
