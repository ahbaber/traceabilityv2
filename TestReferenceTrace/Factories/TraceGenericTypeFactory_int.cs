﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TestReferenceFunctional.BaseClasses;
using TestReferenceTrace.Classes;
using Traceability;

namespace TestReferenceTrace.Factories
{
	public class TraceGenericTypeFactory_int : TestReferenceFunctional.Factories.GenericTypeFactory_int
	{
		public TraceGenericTypeFactory_int(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override GenericType<int> Construct()
		{
			return new TraceGenericType_int(_tracer);
		}

		private readonly ITracer _tracer;
	}

	public partial class TestStrings
	{
		public const string TraceGenericTypeFactory_intExpected = @"// Auto generated file
// Any modification to this file may be lost
using System;
using TestReferenceFunctional.BaseClasses;
using TestReferenceFunctional.Factories;
using TestReferenceTrace.Classes;
using Traceability;

namespace TestReferenceTrace.Factories
{
	public class TraceGenericTypeFactory_int : TestReferenceFunctional.Factories.GenericTypeFactory_int
	{
		public TraceGenericTypeFactory_int(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override GenericType<int> Construct()
		{
			return new TraceGenericType_int(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
";
	}
}
