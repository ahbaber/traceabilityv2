﻿//Copyright (c) 2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using TestReferenceFunctional.BaseClasses;
using TestReferenceTrace.Classes;
using Traceability;

namespace TestReferenceTrace.Factories
{
	public class TraceGenericWithParamsOnConstructFactory : TestReferenceFunctional.Factories.GenericWithParamsOnConstructFactory
	{
		public TraceGenericWithParamsOnConstructFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override GenericType<T> Construct<T>()
		{
			return new TraceGenericType<T>(_tracer);
		}

		private readonly ITracer _tracer;
	}

	public partial class TestStrings
	{
		public const string TraceGenericWithParamsOnConstructFactoryExpected = @"// Auto generated file
// Any modification to this file may be lost
using TestReferenceFunctional.BaseClasses;
using TestReferenceFunctional.Factories;
using TestReferenceTrace.Classes;
using Traceability;

namespace TestReferenceTrace.Factories
{
	public class TraceGenericWithParamsOnConstructFactory : TestReferenceFunctional.Factories.GenericWithParamsOnConstructFactory
	{
		public TraceGenericWithParamsOnConstructFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override GenericType<T> Construct<T>()
		{
			return new TraceGenericType<T>(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
";
	}
}
