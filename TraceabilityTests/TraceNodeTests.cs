﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using Traceability;
using Xunit;

namespace TraceabilityTests
{
    public class TraceNodeTests
    {
        [Fact]
        public void Constructor_ReturnsNewTraceNode()
        {
            var node = new TraceNode("tag");

            Assert.Equal("tag", node.Tag);
            Assert.Null(node.Value);
            Assert.False(node.HasChildren);
            Assert.False(node.HasParameters);
            Assert.Null(node.Parent);
        }

        [Fact]
        public void Constructor_NullTag_ThrowsException()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => new TraceNode(null));
            Assert.Equal($"Value cannot be null.{Environment.NewLine}Parameter name: tag", ex.Message);
        }

        [Fact]
        public void Constructor_EmptyTag_ThrowsException()
        {
            var ex = Assert.Throws<ArgumentException>(() => new TraceNode(""));
            Assert.Equal("'tag' may not be an empty string", ex.Message);
        }

        [Fact]
        public void CreateChild_ReturnsChildNode()
        {
            var parent = new TraceNode("parent");

            var child = parent.CreateChild("child");

            Assert.Equal("child", child.Tag);
            Assert.Null(child.Value);
            Assert.False(child.HasChildren);
            Assert.False(child.HasParameters);
            Assert.Same(parent, child.Parent);

            Assert.True(parent.HasChildren);
            Assert.Single(parent.Children);
            Assert.Same(child, parent.Children[0]);
        }

        [Theory]
        [InlineData(7, "7")]
        [InlineData('c', "c")]
        [InlineData("str", "str")]
        [InlineData(null, "null")]
        public void AddParameter_ParameterAdded(object value, string expected)
        {
            var node = new TraceNode("tag");

            node.AddParameter("param", value);

            Assert.True(node.HasParameters);
            Assert.Single(node.Parameters);
            Assert.True(node.Parameters.ContainsKey("param"));
            Assert.Equal(expected, node.Parameters["param"]);
        }
    }
}
