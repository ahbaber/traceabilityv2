﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Xunit;

namespace TraceabilityTests
{
	public class TraceNodeComparerTests
	{
		[Fact]
		public void Equals_LtTraceNodeNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var rt = new TraceNode("matches");

			Assert.False(comparer.Equals(null, rt));
		}

		[Fact]
		public void Equals_RtTraceNodeNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("matches");

			Assert.False(comparer.Equals(lt, null));
		}

		[Fact]
		public void Equals_BothTraceNodesNull_ReturnsTrue()
		{
			var comparer = new TraceNodeComparer();

			Assert.True(comparer.Equals(null, null));
		}

		[Fact]
		public void Equals_TagsOnly_ReturnsTrue()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("matches");
			var rt = new TraceNode("matches");

			Assert.True(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_TagsOnly_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("matches?");
			var rt = new TraceNode("doesnotmatch");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_LtValueNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			var rt = new TraceNode("node") { Value = "value" };

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_RtValueNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node") { Value = "value" };
			var rt = new TraceNode("node");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ValuesDontMatch_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node") { Value = "value1" };
			var rt = new TraceNode("node") { Value = "value2" };

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ValuesMatch_ReturnsTrue()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node") { Value = "value" };
			var rt = new TraceNode("node") { Value = "value" };

			Assert.True(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_LtParentNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			var parent = new TraceNode("parent");
			var rt = parent.CreateChild("node");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_RtParentNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var parent = new TraceNode("parent");
			var lt = parent.CreateChild("node");
			var rt = new TraceNode("node");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ParentsEqual_ReturnsTrue()
		{
			var comparer = new TraceNodeComparer();
			var ltParent = new TraceNode("parent");
			var lt = ltParent.CreateChild("node");
			var rtParent = new TraceNode("parent");
			var rt = rtParent.CreateChild("node");

			Assert.True(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ParentsSame_ReturnsTrue()
		{
			var comparer = new TraceNodeComparer();
			var parent = new TraceNode("parent");
			var lt = parent.CreateChild("node");
			var rt = parent.CreateChild("node");

			Assert.True(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ParentsDifferent_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var ltParent = new TraceNode("parent1");
			var lt = ltParent.CreateChild("node");
			var rtParent = new TraceNode("parent2");
			var rt = rtParent.CreateChild("node");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_LtParameterNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			var rt = new TraceNode("node");
			rt.AddParameter("param", "value");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_RtParameterNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			lt.AddParameter("param", "value");
			var rt = new TraceNode("node");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ParameterDifferetCounts_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			lt.AddParameter("param", "value");
			var rt = new TraceNode("node");
			rt.AddParameter("param", "value");
			rt.AddParameter("param1", "value");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ParametesMatch_ReturnsTrue()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			lt.AddParameter("param", "value");
			var rt = new TraceNode("node");
			rt.AddParameter("param", "value");

			Assert.True(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_LtChildrenNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			var rt = new TraceNode("node");
			rt.CreateChild("child");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_RtChildrenNull_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			lt.CreateChild("child");
			var rt = new TraceNode("node");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ChildrenDifferentCounts_ReturnsFalse()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			lt.CreateChild("child");
			var rt = new TraceNode("node");
			rt.CreateChild("child");
			rt.CreateChild("child");

			Assert.False(comparer.Equals(lt, rt));
		}

		[Fact]
		public void Equals_ChildrenMatch_ReturnsTrue()
		{
			var comparer = new TraceNodeComparer();
			var lt = new TraceNode("node");
			lt.CreateChild("child");
			var rt = new TraceNode("node");
			rt.CreateChild("child");

			Assert.True(comparer.Equals(lt, rt));
		}
	}
}
