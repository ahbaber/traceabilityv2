﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;
using Traceability;

namespace TraceabilityTests
{
	public class TraceNodeComparer : IEqualityComparer<TraceNode>
	{
		private Dictionary<int, int> _nodeMap = new Dictionary<int, int>();

		public TraceNodeComparer Reset()
		{
			_nodeMap.Clear();
			return this;
		}

		public bool Equals(TraceNode lt, TraceNode rt)
		{
			if (lt == null)
			{
				return rt == null ? true : false;
			}
			else if (rt == null)
			{ return false; }

			int ltHash = lt.GetHashCode();
			int rtHash = rt.GetHashCode();
			if (ltHash == rtHash)
			{ return true; }

			if (_nodeMap.TryGetValue(ltHash, out int match))
			{
				return match == rtHash;
			}
			_nodeMap.Add(ltHash, rtHash);

			return lt.Tag.Equals(rt.Tag)
				&& CompareValue(lt, rt)
				&& CompareParents(lt, rt)
				&& CompareParameters(lt, rt)
				&& CompareChildren(lt, rt);
		}

		public int GetHashCode(TraceNode obj)
		{
			return obj.GetHashCode();
		}

		private bool CompareValue(TraceNode lt, TraceNode rt)
		{
			if (lt.Value == null)
			{
				if (rt.Value != null)
				{ return false; }
			}
			else if (rt.Value == null)
			{ return false; }
			else if (!lt.Value.Equals(rt.Value))
			{ return false; }

			return true;
		}

		private bool CompareParents(TraceNode lt, TraceNode rt)
		{
			if (lt.Parent == null)
			{
				if (rt.Parent != null)
				{ return false; }
			}
			else if (rt.Parent == null)
			{ return false; }
			else if (!Equals(lt.Parent, rt.Parent))
			{ return false; }

			return true;
		}

		private bool CompareParameters(TraceNode lt, TraceNode rt)
		{
			if (!lt.HasParameters)
			{
				if (rt.HasParameters)
				{ return false; }
			}
			else if (!rt.HasParameters)
			{ return false; }
			else
			{
				if (lt.Parameters.Count != rt.Parameters.Count)
				{ return false; }

				var rtParams = rt.Parameters;
				foreach (var pair in lt.Parameters)
				{
					if (!rtParams.ContainsKey(pair.Key)
					|| !pair.Value.Equals(rtParams[pair.Key]))
					{ return false; }
				}
			}
			return true;
		}

		private bool CompareChildren(TraceNode lt, TraceNode rt)
		{
			if (!lt.HasChildren)
			{
				if (rt.HasChildren)
				{ return false; }
			}
			else if (!rt.HasChildren)
			{ return false; }
			else
			{
				if (lt.Children.Count != rt.Children.Count)
				{ return false; }

				for (int i = 0; i < lt.Children.Count; ++i)
				{
					var ltChild = lt.Children[i];
					var rtChild = rt.Children[i];
					if (!Equals(ltChild, rtChild))
					{ return false; }
				}
			}
			return true;
		}
	}
}
