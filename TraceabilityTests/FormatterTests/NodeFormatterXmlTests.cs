﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Traceability.Formatters;
using Xunit;

namespace TraceabilityTests.FormatterTests
{
	/// <summary>Tests the output of the default XML for <see cref="NodeFormatter"/>.</summary>
	public class NodeFormatterXmlTests
	{
		[Fact]
		public void Build_Empty_RetrunsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag");

			string expected = "...<tag />\n";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_ReturnsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag")
			{ Value = "whoops" };

			string expected = "...<tag>whoops</tag>\n";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Parameters_ReturnsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag")
				.AddParameter("param1", 42);

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...</tag>
";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Children_ReturnsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag");
			node.CreateChild("empty");

			string expected = @"...<tag>
...	<empty />
...</tag>
";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_Parameters_ReturnsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag")
			{ Value = "you're it!" }
				.AddParameter("param1", 42);

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...	<value>you're it!</value>
...</tag>
";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_Children_ReturnsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag")
			{ Value = "world" };
			node.CreateChild("empty");

			string expected = @"...<tag>
...	<empty />
...	<value>world</value>
...</tag>
";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Parameters_Children_ReturnsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag")
				.AddParameter("param1", 42);
			node.CreateChild("Another").Value = "One";
			node.CreateChild("Bites").Value = "the Dust";

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...	<Another>One</Another>
...	<Bites>the Dust</Bites>
...</tag>
";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_Parameters_Children_ReturnsExpectedText()
		{
			var xmlNode = new NodeFormatter()
				.SetIndent("...")
				.SetTab("\t")
				.SetNewLine("\n");
			var node = new TraceNode("tag")
			{ Value = "Wheee" }
				.AddParameter("param1", 42);
			node.CreateChild("Another").Value = "One";
			node.CreateChild("Bites").Value = "the Dust";

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...	<Another>One</Another>
...	<Bites>the Dust</Bites>
...	<value>Wheee</value>
...</tag>
";
			string actual = xmlNode.Format(node);

			Assert.Equal(expected, actual);
		}
	}
}
