﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;
using Traceability.Formatters;
using Xunit;

namespace TraceabilityTests.FormatterTests
{
	public class EmptyNodeParserTests
	{
		[Fact]
		public void Build_ReturnsEmptyBuilder()
		{
			var parser = new EmptyNodeParser();
			var builder = new StringBuilder();

			var actual = parser.Build(builder, null, null, null, "", "", "");

			Assert.Equal(0, actual.Length);
		}
	}
}
