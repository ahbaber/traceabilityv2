﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;
using Traceability;
using Traceability.Formatters;
using Xunit;

namespace TraceabilityTests.FormatterTests
{
	/// <summary>Tests the output of <see cref="NodeFormatter"/> with an injected parser.</summary>
	public class NodeFormatterInjectedParserTests
	{
		private class TestParser : INodeParser
		{
			public TraceNode Node { get; set; }
			public string Indent { get; set; }
			public string Tab { get; set; }
			public string NewLine { get; set; }

			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				Node = node;
				Indent = indent;
				Tab = tab;
				NewLine = newLine;
				return builder;
			}
		}
		private class TestNodeFormatter : NodeFormatter
		{
			public TestNodeFormatter(INodeParser parser)
			: base(parser)
			{ }
		}

		[Fact]
		public void Format_DefaultParserCalled()
		{
			var defaultParser = new TestParser();
			var formatter = new TestNodeFormatter(defaultParser);
			var node = new TraceNode("tag");

			formatter.Format(node);

			Assert.Same(node, defaultParser.Node);
		}

		[Fact]
		public void AddParser_AlternateParserCalled()
		{
			var defaultParser = new TestParser();
			var tagParser = new TestParser();
			var formatter = new TestNodeFormatter(defaultParser)
				.AddParser("tag", tagParser);
			var node = new TraceNode("tag");

			formatter.Format(node);

			Assert.Null(defaultParser.Node);
			Assert.Same(node, tagParser.Node);
		}

		[Fact]
		public void SetIndent_IndentPassedToParser()
		{
			var defaultParser = new TestParser();
			var formatter = new TestNodeFormatter(defaultParser)
				.SetIndent("indent");
			var node = new TraceNode("tag");

			formatter.Format(node);

			Assert.Equal("indent", defaultParser.Indent);
			Assert.Equal(string.Empty, defaultParser.Tab);
			Assert.Equal(string.Empty, defaultParser.NewLine);
		}

		[Fact]
		public void SetTab_TabPassedToParser()
		{
			var defaultParser = new TestParser();
			var formatter = new TestNodeFormatter(defaultParser)
				.SetTab("tab");
			var node = new TraceNode("tag");

			formatter.Format(node);

			Assert.Equal("tab", defaultParser.Tab);
			Assert.Equal(string.Empty, defaultParser.Indent);
			Assert.Equal(string.Empty, defaultParser.NewLine);
		}

		[Fact]
		public void SetNewLine_NewLinePassedToParser()
		{
			var defaultParser = new TestParser();
			var formatter = new TestNodeFormatter(defaultParser)
				.SetNewLine("newLine");
			var node = new TraceNode("tag");

			formatter.Format(node);

			Assert.Equal("newLine", defaultParser.NewLine);
			Assert.Equal(string.Empty, defaultParser.Indent);
			Assert.Equal(string.Empty, defaultParser.Tab);
		}

		private class ThrowingParser : INodeParser
		{
			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				builder.Append($"<{node.Tag}>");
				throw new System.Exception("That wasn't expected");
			}
		}

		[Fact]
		public void FormatSafe_ExceptionThrown_ReturnsExpectedValues()
		{
			var parser = new ThrowingParser();
			var formatter = new TestNodeFormatter(parser);
			var node = new TraceNode("SpanishInquisition");

			string expectedMessage = "That wasn't expected";
			string expectedTag = "<SpanishInquisition>";
			string actual = formatter.FormatSafe(node);

			Assert.Contains(expectedMessage, actual);
			Assert.Contains(expectedTag, actual);
		}
	}
}
