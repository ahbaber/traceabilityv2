﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;
using Traceability;
using Traceability.Formatters.XmlSupport;
using Xunit;

namespace TraceabilityTests.FormatterTests.XmlSupportTests
{
	public class XmlValueTests
	{
		[Fact]
		public void Build_NullValue_ReturnsEmptyString()
		{
			var value = new XmlValue();
			var node = new TraceNode("tag");

			var actual = value.Build(new StringBuilder(), node, null, "", "", "");

			Assert.Equal(0, actual.Length);
		}

		[Fact]
		public void Build_Value_ReturnsExpectedText()
		{
			var value = new XmlValue();
			var node = new TraceNode("tag") { Value = "hello" };

			var actual = value.Build(new StringBuilder(), node, null, "", "", "").ToString();

			Assert.Equal("hello", actual);
		}

		[Fact]
		public void Build_ValueWithParameters_ReturnsExpectedText()
		{
			var value = new XmlValue();
			var node = new TraceNode("tag") { Value = "hello" }.AddParameter("param", 42);

			var actual = value.Build(new StringBuilder(), node, null, "", "", "").ToString();

			Assert.Equal("<value>hello</value>", actual);
		}

		[Fact]
		public void Build_ValueWithChildren_ReturnsExpectedText()
		{
			var value = new XmlValue();
			var node = new TraceNode("tag") { Value = "hello" };
			node.CreateChild("world");

			var actual = value.Build(new StringBuilder(), node, null, "", "", "").ToString();

			Assert.Equal("<value>hello</value>", actual);
		}

		[Fact]
		public void Build_Formatted_Value_ReturnsExpectedText()
		{
			var value = new XmlValue();
			var node = new TraceNode("tag") { Value = "hello" };

			var actual = value.Build(new StringBuilder(), node, null, "\t\t", "\t", "\n").ToString();

			Assert.Equal("hello", actual);
		}

		[Fact]
		public void Build_Formatted_ValueWithParameters_ReturnsExpectedText()
		{
			var value = new XmlValue();
			var node = new TraceNode("tag") { Value = "hello" }.AddParameter("param", 42);

			var actual = value.Build(new StringBuilder(), node, null, "\t\t", "\t", "\n").ToString();

			Assert.Equal("\t\t<value>hello</value>\n", actual);
		}

		[Fact]
		public void Build_Formatted_ValueWithChildren_ReturnsExpectedText()
		{
			var value = new XmlValue();
			var node = new TraceNode("tag") { Value = "hello" };
			node.CreateChild("world");

			var actual = value.Build(new StringBuilder(), node, null, "\t\t", "\t", "\n").ToString();

			Assert.Equal("\t\t<value>hello</value>\n", actual);
		}
	}
}
