﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;
using Traceability;
using Traceability.Formatters;
using Traceability.Formatters.XmlSupport;
using Xunit;

namespace TraceabilityTests.FormatterTests.XmlSupportTests
{
	public class XmlNodeParserTests
	{
		private readonly ParserMap _parserMap = new ParserMap(new XmlNodeParser());

		[Fact]
		public void Build_Empty_RetrunsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag");

			string expected = "...<tag />\n";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_ReturnsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag")
			{ Value = "whoops" };

			string expected = "...<tag>whoops</tag>\n";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Parameters_ReturnsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag")
				.AddParameter("param1", 42);

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...</tag>
";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Children_ReturnsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag");
			node.CreateChild("empty");

			string expected = @"...<tag>
...	<empty />
...</tag>
";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_Parameters_ReturnsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag")
			{ Value = "you're it!" }
				.AddParameter("param1", 42);

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...	<value>you're it!</value>
...</tag>
";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_Children_ReturnsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag")
			{ Value = "world" };
			node.CreateChild("empty");

			string expected = @"...<tag>
...	<empty />
...	<value>world</value>
...</tag>
";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Parameters_Children_ReturnsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag")
				.AddParameter("param1", 42);
			node.CreateChild("Another").Value = "One";
			node.CreateChild("Bites").Value = "the Dust";

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...	<Another>One</Another>
...	<Bites>the Dust</Bites>
...</tag>
";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Value_Parameters_Children_ReturnsExpectedText()
		{
			var xmlNode = new XmlNodeParser();
			var node = new TraceNode("tag")
			{ Value = "Wheee" }
				.AddParameter("param1", 42);
			node.CreateChild("Another").Value = "One";
			node.CreateChild("Bites").Value = "the Dust";

			string expected = @"...<tag>
...	<parameters>
...		<param1>42</param1>
...	</parameters>
...	<Another>One</Another>
...	<Bites>the Dust</Bites>
...	<value>Wheee</value>
...</tag>
";
			var actual = xmlNode.Build(new StringBuilder(), _parserMap, node, null, "...", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}
	}
}
