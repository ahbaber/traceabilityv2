﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Text;
using Traceability;
using Traceability.Formatters.XmlSupport;
using Xunit;

namespace TraceabilityTests.FormatterTests.XmlSupportTests
{
	public class XmlParameterTests
	{
		[Fact]
		public void Build_NoParams_ReturnsEmptyString()
		{
			var param = new XmlParameter();
			var node = new TraceNode("node");

			string actual = param.Build(new StringBuilder(), node, null, "", "", "").ToString();

			Assert.Equal(0, actual.Length);
		}

		[Fact]
		public void Build_OneParam_ReturnsExpectedText()
		{
			var param = new XmlParameter();
			var node = new TraceNode("node")
				.AddParameter("param1", "value1");

			string expected = "<parameters><param1>value1</param1></parameters>";
			string actual = param.Build(new StringBuilder(), node, null, "", "", "").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_TwoParams_ReturnsExpectedText()
		{
			var param = new XmlParameter();
			var node = new TraceNode("node")
				.AddParameter("param1", "value1")
				.AddParameter("param2", "value2");

			string expected = "<parameters><param1>value1</param1><param2>value2</param2></parameters>";
			string actual = param.Build(new StringBuilder(), node, null, "", "", "").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Formatted_OneParam_ReturnsExpectedText()
		{
			var param = new XmlParameter();
			var node = new TraceNode("node")
				.AddParameter("param1", "value1");

			string expected = "<parameters>\n\t<param1>value1</param1>\n</parameters>\n";
			string actual = param.Build(new StringBuilder(), node, null, "", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_Formatted_TwoParams_ReturnsExpectedText()
		{
			var param = new XmlParameter();
			var node = new TraceNode("node")
				.AddParameter("param1", "value1")
				.AddParameter("param2", "value2");

			string expected = "<parameters>\n\t<param1>value1</param1>\n\t<param2>value2</param2>\n</parameters>\n";
			string actual = param.Build(new StringBuilder(), node, null, "", "\t", "\n").ToString();

			Assert.Equal(expected, actual);
		}
	}
}
