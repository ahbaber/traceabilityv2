﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using Traceability.Formatters;
using Xunit;

namespace TraceabilityTests.FormatterTests
{
	public class ContextTests
	{
		private class TestClass { }

		[Fact]
		public void GetSetInt_ReturnsSameValue()
		{
			var context = new Context();
			context.Set("key", 5);

			var actual = context.GetInt("key");

			Assert.Equal(5, actual);
		}

		[Fact]
		public void GetInt_DoesNotExist_Returns0()
		{
			var context = new Context();

			var actual = context.GetInt("key");

			Assert.Equal(0, actual);
		}

		[Theory]
		[InlineData(false, 0)]
		[InlineData(true, 1)]
		public void GetInt_BoolValue_ReturnsExpectedValue(bool b, int expected)
		{
			var context = new Context();
			context.Set("key", b);

			var actual = context.GetInt("key");

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("23", 23)]
		[InlineData("-19", -19)]
		public void GetInt_StringNumbers_ReturnsExpectedValue(string s, int expected)
		{
			var context = new Context();
			context.Set("key", s);

			var actual = context.GetInt("key");

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void GetInt_NontranslatableString_ThrowsException()
		{
			var context = new Context();
			context.Set("key", "not an int");

			var actual = Assert.Throws<FormatException>(() => context.GetInt("key"));

			Assert.Equal("Input string was not in a correct format.", actual.Message);
		}

		[Fact]
		public void GetInt_ReferenceObject_ThrowsException()
		{
			var context = new Context();
			context.Set("key", new TestClass());

			var actual = Assert.Throws<InvalidCastException>(() => context.GetInt("key"));

			Assert.Equal("Unable to cast object of type 'TestClass' to type 'System.IConvertible'.", actual.Message);
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void GetSetBool_ReturnsSameValue(bool b)
		{
			var context = new Context();
			context.Set("key", b);

			var actual = context.GetBool("key");

			Assert.Equal(b, actual);
		}

		[Fact]
		public void GetBool_DoesNotExist_ReturnsFalse()
		{
			var context = new Context();

			var actual = context.GetBool("key");

			Assert.False(actual);
		}

		[Theory]
		[InlineData(-10, true)]
		[InlineData(-1, true)]
		[InlineData(0, false)]
		[InlineData(1, true)]
		[InlineData(2, true)]
		[InlineData(20, true)]
		public void GetBool_ValidInts_ReturnsExpectedValue(int i, bool expected)
		{
			var context = new Context();
			context.Set("key", i);

			var actual = context.GetBool("key");

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData("false", false)]
		[InlineData("False", false)]
		[InlineData("FALSE", false)]
		[InlineData("TRUE", true)]
		[InlineData("True", true)]
		[InlineData("true", true)]
		public void GetBool_ValidStrings_ReturnsExpectedValue(string s, bool expected)
		{
			var context = new Context();
			context.Set("key", s);

			var actual = context.GetBool("key");

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void GetBool_NontranslatableString_ThrowsException()
		{
			var context = new Context();
			context.Set("key", "not a bool");

			var actual = Assert.Throws<FormatException>(() => context.GetInt("key"));

			Assert.Equal("Input string was not in a correct format.", actual.Message);
		}

		[Fact]
		public void GetSetString_ReturnsSameValue()
		{
			var context = new Context();
			context.Set("key", "stringValue");

			var actual = context.GetString("key");

			Assert.Equal("stringValue", actual);
		}

		[Fact]
		public void GetString_DoesNotExist_ReturnsSameValue()
		{
			var context = new Context();

			var actual = context.GetString("key");

			Assert.Null(actual);
		}

		[Theory]
		[InlineData(false, "False")]
		[InlineData(true, "True")]
		public void GetString_BoolValues_ReturnsExpectedValue(bool b, string expected)
		{
			var context = new Context();
			context.Set("key", b);

			var actual = context.GetString("key");

			Assert.Equal(expected, actual);
		}

		[Theory]
		[InlineData(5, "5")]
		[InlineData(-5, "-5")]
		public void GetString_IntValues_ReturnsExpectedValue(int i, string expected)
		{
			var context = new Context();
			context.Set("key", i);

			var actual = context.GetString("key");

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void GetString_ReferenceObject_ReturnsSameValue()
		{
			var context = new Context();
			context.Set("key", new TestClass());

			var actual = context.GetString("key");

			Assert.Equal("TraceabilityTests.FormatterTests.ContextTests+TestClass", actual);
		}

		[Fact]
		public void GetSetStringObject_ReturnsSameValue()
		{
			var context = new Context();
			context.Set("key", "stringValue");

			var actual = context.Get<string>("key");

			Assert.Equal("stringValue", actual);
		}

		[Fact]
		public void GetStringObject_DoesNotExist_ReturnsNull()
		{
			var context = new Context();

			var actual = context.Get<string>("key");

			Assert.Null(actual);
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void GetStringObject_BoolValues_ReturnsNull(bool b)
		{
			var context = new Context();
			context.Set("key", b);

			var actual = context.Get<string>("key");

			Assert.Null(actual);
		}

		[Theory]
		[InlineData(-5)]
		[InlineData(7)]
		public void GetStringObject_intValues_ReturnsNull(int i)
		{
			var context = new Context();
			context.Set("key", i);

			var actual = context.Get<string>("key");

			Assert.Null(actual);
		}

		[Fact]
		public void GetStringObject_ReferenceObject_ReturnsNull()
		{
			var context = new Context();
			context.Set("key", new TestClass());

			var actual = context.Get<string>("key");

			Assert.Null(actual);
		}

		[Fact]
		public void GetSetReference_ReturnsSameValue()
		{
			var context = new Context();
			var testClass = new TestClass();
			context.Set("classKey", testClass);

			var actual = context.Get<TestClass>("classKey");

			Assert.Same(testClass, actual);
		}

		[Fact]
		public void GetReference_DoesNotExist_ReturnsNull()
		{
			var context = new Context();

			var actual = context.Get<TestClass>("key");

			Assert.Null(actual);
		}

		[Theory]
		[InlineData(false)]
		[InlineData(true)]
		public void GetReference_BoolValue_ReturnsNull(bool b)
		{
			var context = new Context();
			context.Set("key", b);

			var actual = context.Get<TestClass>("key");

			Assert.Null(actual);
		}

		[Theory]
		[InlineData(-11)]
		[InlineData(23)]
		public void GetReference_IntValue_ReturnsNull(int i)
		{
			var context = new Context();
			context.Set("key", i);

			var actual = context.Get<TestClass>("key");

			Assert.Null(actual);
		}

		[Fact]
		public void GetReference_String_ReturnsNull()
		{
			var context = new Context();
			context.Set("Key", "string value");

			var actual = context.Get<TestClass>("key");

			Assert.Null(actual);
		}
	}
}
