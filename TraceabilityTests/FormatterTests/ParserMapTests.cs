﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;
using System.Text;
using Traceability;
using Traceability.Formatters;
using Xunit;

namespace TraceabilityTests.FormatterTests
{
	public class ParserMapTests
	{
		private class FakeParser : INodeParser
		{
			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				throw new NotImplementedException();
			}
		}

		private class FakeTaggedParser : INodeTaggedParser
		{
			public string Tag => "tagged";

			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				throw new NotImplementedException();
			}
		}

		[Fact]
		public void GetNode_ReturnsDefault()
		{
			var defaultNode = new FakeParser();
			var nodeMap = new ParserMap(defaultNode);

			var actual = nodeMap.GetParser("whatever");

			Assert.Same(defaultNode, actual);
		}

		[Fact]
		public void AddParser_ReturnsSpecificNode()
		{
			var defaultNode = new FakeParser();
			var specific = new FakeParser();
			var nodeMap = new ParserMap(defaultNode)
				.AddParser("specfic", specific);

			var actual = nodeMap.GetParser("specfic");

			Assert.Same(specific, actual);
		}

		[Fact]
		public void AddTempParser_ReturnsTempThenDefault()
		{
			var defaultNode = new FakeParser();
			var tempNode = new FakeParser();
			var nodeMap = new ParserMap(defaultNode);


			using (var temp = nodeMap.AddTempParser("temp", tempNode))
			{
				Assert.Same(tempNode, nodeMap.GetParser("temp"));
			}
			Assert.Same(defaultNode, nodeMap.GetParser("temp"));
		}

		[Fact]
		public void AddTempParser_KeyAlreadyExists_ReturnsOverrideThenOriginal()
		{
			var defaultNode = new FakeParser();
			var originalNode = new FakeParser();
			var overrideNode = new FakeParser();
			var nodeMap = new ParserMap(defaultNode)
				.AddParser("tag", originalNode);

			using (var temp = nodeMap.AddTempParser("tag", overrideNode))
			{
				Assert.Same(overrideNode, nodeMap.GetParser("tag"));
			}
			Assert.Same(originalNode, nodeMap.GetParser("tag"));
		}

		[Fact]
		public void AddTempParse_Keyed_ReturnsTempThenDefault()
		{
			var defaultNode = new FakeParser();
			var tempNode = new FakeTaggedParser();
			var nodeMap = new ParserMap(defaultNode);

			using (var temp = nodeMap.AddTempParser(tempNode))
			{
				Assert.Same(tempNode, nodeMap.GetParser("tagged"));
			}
			Assert.Same(defaultNode, nodeMap.GetParser("tagged"));
		}
	}
}
