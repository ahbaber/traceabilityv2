﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Traceability.Formatters;
using System.Text;
using Xunit;

namespace TraceabilityTests.FormatterTests
{
	public class AbstractNodeChildProcessorTests
	{
		private class DefaultParser : INodeParser
		{
			public TraceNode Node { get; set; }
			public string Indent { get; set; }
			public string Tab { get; set; }
			public string NewLine { get; set; }

			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				return builder.Append($"<{node.Tag}/>");
			}
		}

		private class ParentProcessor : AbstractNodeChildProcessor, INodeParser
		{
			public StringBuilder Build(StringBuilder builder, ParserMap map, TraceNode node, Context context, string indent, string tab, string newLine)
			{
				builder.Append("<parent>");
				ProcessChildren(builder, map, node, context, indent, tab, newLine);
				return builder.Append("</parent>");
			}
		}

		[Fact]
		public void Build_WithNoChildren_ReturnsExpectedString()
		{
			var map = new ParserMap(new DefaultParser());
			var parent = new ParentProcessor();
			var node = new TraceNode("parent");

			string expected = "<parent></parent>";
			string actual = parent.Build(new StringBuilder(), map, node, null, "", "", "").ToString();

			Assert.Equal(expected, actual);
		}

		[Fact]
		public void Build_WithChildren_ReturnsExpectedString()
		{
			var map = new ParserMap(new DefaultParser());
			var parent = new ParentProcessor();
			var node = new TraceNode("parent");
			node.CreateChild("child1");
			node.CreateChild("child2");

			string expected = "<parent><child1/><child2/></parent>";
			string actual = parent.Build(new StringBuilder(), map, node, null, "", "", "").ToString();

			Assert.Equal(expected, actual);
		}
	}
}
