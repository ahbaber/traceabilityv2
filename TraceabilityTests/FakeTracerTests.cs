﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Xunit;

namespace TraceabilityTests
{
	public class FakeTracerTests
	{
		[Fact]
		public void FunctionStart_ReturnsInstance()
		{
			var tracer = new FakeTracer();

			var actual = tracer.FunctionStart("root");

			Assert.Same(tracer, actual);
		}

		[Fact]
		public void FunctionEnd_ReturnsInstance()
		{
			var tracer = new FakeTracer();

			var actual = tracer.FunctionEnd();

			Assert.Same(tracer, actual);
		}

		[Theory]
		[InlineData(3)]
		[InlineData('c')]
		[InlineData("str")]
		[InlineData(null)]
		public void FunctionEndResult_ReturnsValue(object value)
		{
			var tracer = new FakeTracer();

			var actual = tracer.FunctionEnd(value);

			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(3)]
		[InlineData('c')]
		[InlineData("str")]
		[InlineData(null)]
		public void GetProperty_ReturnsValue(object value)
		{
			var tracer = new FakeTracer();

			var actual = tracer.GetProperty("property", value);

			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(3)]
		[InlineData('c')]
		[InlineData("str")]
		[InlineData(null)]
		public void SetProperty_ReturnsValue(object value)
		{
			var tracer = new FakeTracer();

			var actual = tracer.SetProperty("property", value);

			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(3)]
		[InlineData('c')]
		[InlineData("str")]
		[InlineData(null)]
		public void GetIndexer_ReturnsValue(object value)
		{
			var tracer = new FakeTracer();

			var actual = tracer.GetIndexer("property", 0, value);

			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(3)]
		[InlineData('c')]
		[InlineData("str")]
		[InlineData(null)]
		public void SetIndexer_ReturnsValue(object value)
		{
			var tracer = new FakeTracer();

			var actual = tracer.SetIndexer("property", 0, value);

			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(3)]
		[InlineData('c')]
		[InlineData("str")]
		[InlineData(null)]
		public void Parameter_ReturnsInstance(object value)
		{
			var tracer = new FakeTracer();

			var actual = tracer.Parameter("property", value);

			Assert.Same(tracer, actual);
		}

		[Fact]
		public void ParameterArray_ReturnsInstance()
		{
			var tracer = new FakeTracer();
			int[] array = { 1, 2, 3 };

			var actual = tracer.Parameter("property", array);

			Assert.Same(tracer, actual);
		}
	}
}
