﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Xunit;

namespace TraceabilityTests
{
	public class CountTracerTests
	{
		[Fact]
		public void FunctionStart_FunctionCallRecordedDepthIncreased()
		{
			var tracer = new CountTracer();

			var actual = tracer.FunctionStart("functionName");

			Assert.Equal(1, tracer.Calls["functionName"]);
			Assert.Equal(1, tracer.Depth);
			Assert.Equal(1, tracer.MaxDepth);
			Assert.Same(tracer, actual);
		}

		[Fact]
		public void FunctionEnd_DepthDecreased()
		{
			var tracer = new CountTracer();

			var actual = tracer.FunctionStart("functionName")
				.FunctionEnd();

			Assert.Equal(0, tracer.Depth);
			Assert.Equal(1, tracer.MaxDepth);
			Assert.Same(tracer, actual);
		}

		[Fact]
		public void FunctionEndResult_DepthDecreased()
		{
			var tracer = new CountTracer();

			var actual = tracer.FunctionStart("functionName")
				.FunctionEnd(5);

			Assert.Equal(0, tracer.Depth);
			Assert.Equal(1, tracer.MaxDepth);
			Assert.Equal(5, actual);
		}

		[Fact]
		public void GetProperty_ReadCountIncreased()
		{
			var tracer = new CountTracer();

			var actual = tracer.GetProperty("property", 12);

			Assert.Equal(1, tracer.Reads["property"]);
			Assert.Equal(12, actual);
		}

		[Fact]
		public void SetProperty_WriteCountIncreased()
		{
			var tracer = new CountTracer();

			var actual = tracer.SetProperty("property", 12);

			Assert.Equal(1, tracer.Writes["property"]);
			Assert.Equal(12, actual);
		}

		[Fact]
		public void GetIndexer_ReadCountIncreased()
		{
			var tracer = new CountTracer();

			var actual = tracer.GetIndexer("className", 3, 11);

			Assert.Equal(1, tracer.Reads["className_indexer"]);
			Assert.Equal(11, actual);
		}

		[Fact]
		public void SetIndexer_WriteCountIncreased()
		{
			var tracer = new CountTracer();

			var actual = tracer.SetIndexer("className", 3, 11);

			Assert.Equal(1, tracer.Writes["className_indexer"]);
			Assert.Equal(11, actual);
		}

		[Fact]
		public void Parameter_ReturnsInstance()
		{
			var tracer = new CountTracer();

			var actual = tracer.Parameter("param", 5);

			Assert.Same(tracer, actual);
		}

		[Fact]
		public void ParameterArgs_ReturnsInstance()
		{
			var tracer = new CountTracer();
			int[] array = { 1 };

			var actual = tracer.Parameter("param", array);

			Assert.Same(tracer, actual);
		}
	}
}
