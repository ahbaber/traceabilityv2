﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Traceability;
using Xunit;

namespace TraceabilityTests
{
	public class TracerTests
	{
		[Fact]
		public void Constructor_CreatesTracer()
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");

			Assert.Same(tracer.Root, tracer.Node);
			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
		}

		[Fact]
		public void FunctionStart_ChildNodeAdded()
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.CreateChild("functionCall");
			var actual = tracer.FunctionStart("functionCall");

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root.Children[0], tracer.Node);
			Assert.NotSame(tracer.Root, tracer.Node);
			Assert.Same(tracer, actual);
		}

		[Fact]
		public void FunctionEnd_ChildNodeClosed()
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.CreateChild("functionCall");
			var actual = tracer.FunctionStart("functionCall")
				.FunctionEnd();

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Same(tracer, actual);
		}

		[Fact]
		public void FunctionEnd_CalledOnRoot_NoEffect()
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			var actual = tracer.FunctionEnd();

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Same(tracer, actual);
		}

		[Theory]
		[InlineData(3, "3")]
		[InlineData('d', "d")]
		[InlineData("hello", "hello")]
		[InlineData(null, "null")]
		public void FunctionEnd_StoresResultsAndChildNodeClosed(object value, string expectedValue)
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.CreateChild("functionCall").Value = expectedValue;
			var actual = tracer.FunctionStart("functionCall")
				.FunctionEnd(value);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(5, 13, "5", "13")]
		[InlineData(2, 'e', "2", "e")]
		[InlineData(7, "goodbye", "7", "goodbye")]
		[InlineData(4, null, "4", "null")]
		public void GetIndexer_ExpectedDataTraced(int index, object value, string expectedIndex, string expectedValue)
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.CreateChild("child_index_get")
				.AddParameter("index", expectedIndex)
				.Value = expectedValue;
			var actual = tracer.GetIndexer("child", index, value);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(5, 13, "5", "13")]
		[InlineData(2, 'e', "2", "e")]
		[InlineData(7, "goodbye", "7", "goodbye")]
		[InlineData(4, null, "4", "null")]
		public void SetIndexer_ExpectedDataTraced(int index, object value, string expectedIndex, string expectedValue)
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.CreateChild("child_index_set")
				.AddParameter("index", expectedIndex)
				.Value = expectedValue;
			var actual = tracer.SetIndexer("child", index, value);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(7, "7")]
		[InlineData('f', "f")]
		[InlineData("something", "something")]
		[InlineData(null, "null")]
		public void GetProperty_ExpectedDataTraced(object value, string expectedValue)
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.CreateChild("property_get").Value = expectedValue;
			var actual = tracer.GetProperty("property", value);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(7, "7")]
		[InlineData('f', "f")]
		[InlineData("something", "something")]
		[InlineData(null, "null")]
		public void SetProperty_ExpectedDataTraced(object value, string expectedValue)
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.CreateChild("property_set").Value = expectedValue;
			var actual = tracer.SetProperty("property", value);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Equal(value, actual);
		}

		[Theory]
		[InlineData(7, "7")]
		[InlineData('f', "f")]
		[InlineData("something", "something")]
		[InlineData(null, "null")]
		public void Parameter_ParameterDataTraced(object value, string expectedValue)
		{
			var tracer = new Tracer("root");

			var expected = new TraceNode("root");
			expected.AddParameter("param", expectedValue);
			var actual = tracer.Parameter("param", value);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Same(tracer, actual);
		}

		[Fact]
		public void ParameterArray_NullArray_ParameterDataTraced()
		{
			var tracer = new Tracer("root");
			int[] array = null;

			var expected = new TraceNode("root");
			expected.AddParameter("param", "null(0)");
			var actual = tracer.Parameter("param", array);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Same(tracer, actual);
		}

		[Fact]
		public void ParameterArray_1ElementCharArray_ParameterDataTraced()
		{
			var tracer = new Tracer("root");
			char[] array = { 'c' };

			var expected = new TraceNode("root");
			expected.AddParameter("param", "System.Char[](1)");
			var actual = tracer.Parameter("param", array);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Same(tracer, actual);
		}

		[Fact]
		public void ParameterArray_2ElementIntArray_ParameterDataTraced()
		{
			var tracer = new Tracer("root");
			int[] array = { 1, 2 };

			var expected = new TraceNode("root");
			expected.AddParameter("param", "System.Int32[](2)");
			var actual = tracer.Parameter("param", array);

			Assert.Equal(expected, tracer.Root, new TraceNodeComparer());
			Assert.Same(tracer.Root, tracer.Node);
			Assert.Same(tracer, actual);
		}
	}
}
