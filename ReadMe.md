#Traceability V2

Copyright (c) 2018,2019 Adin Hunter Baber
Under the MIT License
See [License](https://bitbucket.org/ahbaber/traceabilityv2/src/Release/License.txt) for more details

More information is in the [Wiki](https://bitbucket.org/ahbaber/traceabilityv2/wiki/Home)

###Change Log

2019 Feb 19 Release 2.3.0
 * Generators may now use qualified names to create new classes.
 * Verify refactored and improved.
 * Verify now uses SkipMap to indicate members to ignore.
 * FileWriter replaces TraceFileGenerator
 * FileWriter will return error messages if a generator is unable to generate code.


2019 Feb 13 Release 2.2.1

 * namespaces are automatically extracted during code generation, and added to the generated code.

2019 Feb 11 Release 2.2.0

 * Added automatic code generation to build factories.
 * Added automated code generation to build trace factories.

2019 Feb 08 Release 2.1.0

 * ITracer.RemoveFuction() has been removed.  If we want to ignore data, then it should be skipped via writing a formatter parser that skips the specific node.
 * TraceNode.Parameters is now a dictionary.
 * ref and out parameters now add a new entry to the parameter dictionary after the nase method is called.
 * The new parameter entry will have the parameter's name with "_out" appended to it.

2019 Feb 06 Release 2.0.1

 * Improved support for tracing generic classes
 * Example trace file generation added to the unit tests

2019 Jan 28 Release 2.0.0

 * Heavy rewrite
 * Trace code generation no longer requires T4 files
 * Trace code generation may be done in only a few lines of code
 * ITracer interface simplified
 * Formatters improved to use more dependency injection
